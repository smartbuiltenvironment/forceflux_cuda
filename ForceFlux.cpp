#include <stdio.h>
#include "ParticleSystem.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include<GL/glew.h>
#include<glfw3.h>
#include<sstream> 

#include "ErrorCheck.h"

#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"

#include "Shader.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"

#include "tests/TestBatch.h"
#include "tests/TestKeyCamera.h"
#include "tests/TestParticles3D.h"
#include "tests/TestInstancing.h"
#include "tests/TestCircularDisc.h"
#include "tests/TestCylinder.h"
#include "tests/TestLight.h"

#include "Common.h";

using namespace std;

int ScreenWidth = 2400;
int ScreenHeight = 1350;

keyAcion m_KeyAction = keyAcion::non;
float m_xPos = ScreenWidth / 2;
float m_yPos = ScreenHeight / 2;
float m_Fov = 45.0f;
bool m_LeftButtonPressed = false;
bool m_RightButtonPressed = false;

bool m_LeftButtonReleased = false;

template <typename T> T convert_to(const std::string& str)
{
	std::istringstream ss(str);
	T num;
	ss >> num;
	return num;
}


template<typename T>
void ReadTextFile(string& filename, vector<T>& x, vector<T>& y, vector<T>& z, vector<T>& all)
{
	string line;
	ifstream file_(filename);
	int ptCounter = 0;
	int xyzCounter = 0;

	if (file_.is_open())
	{
		while (getline(file_, line))
		{
			T val = convert_to<T>(line);

			if (xyzCounter == 0)
				x.push_back(val);
			else if (xyzCounter == 1)
				y.push_back(val);
			else
				z.push_back(val);

			all.push_back(val);

			xyzCounter++;

			if (xyzCounter == 3)
			{
				xyzCounter = 0;
				ptCounter++;
			}
		}
		file_.close();
	}

}

void processInput(GLFWwindow* window, testNS::Test* currentTest, float scroll)
{
	bool reset = false;

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::leftArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::rightArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::upArrow;
	}
	else if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		m_KeyAction = keyAcion::downArrow;
	}
	
	if(m_KeyAction != keyAcion::non && m_KeyAction != keyAcion::mouse)
		currentTest->OnUpdateCameraKey(m_KeyAction);

	if (m_KeyAction == keyAcion::mouse && m_LeftButtonPressed)
	{
		currentTest->OnUpdateCameraMouseRotate(m_xPos, m_yPos);
		reset = true;
	}
	
	if (m_KeyAction == keyAcion::mouse && m_LeftButtonReleased)
	{
		currentTest->OnUpdateCameraReset();
		m_LeftButtonReleased = false;
		reset = true;
	}


	if(reset)
		m_KeyAction = keyAcion::non;


	if (scroll != 0)
	{	
		m_Fov -= scroll;
		currentTest->OnUpdateCameraScroll(m_Fov);
	}
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	m_KeyAction = keyAcion::mouse;
	m_xPos = xpos;
	m_yPos = ypos;
}

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

void ButtonFeedback(GLFWwindow* window)
{
	int stateLeft = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
	if (stateLeft == GLFW_PRESS)
		m_LeftButtonPressed = true;
	else if (stateLeft == GLFW_RELEASE)
	{
		m_LeftButtonReleased = true;
		m_LeftButtonPressed = false;
	}

	int stateRight = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
	if (stateRight == GLFW_PRESS)
		m_RightButtonPressed = true;
	else if (stateRight == GLFW_RELEASE)
		m_RightButtonPressed = false;
}

void TestingStuff() 
{
	ParticleSystem ps = ParticleSystem();

	vector<float> xCoordinates;
	vector<float> yCoordinates;
	vector<float> zCoordinates;
	vector<float> allCoordinates;

	string coordFile = "import/coordinates.txt";

	vector<int> a;
	vector<int> b;
	vector<int> c;
	vector<int> allTopology;

	string topFile = "import/topology.txt";

	ReadTextFile<float>(coordFile, xCoordinates, yCoordinates, zCoordinates, allCoordinates);
	ReadTextFile<int>(topFile, a, b, c, allTopology);

	const int coordCount = 36;
	const int topCount = 60;

	float arrCoord[coordCount];
	for (int i = 0; i < allCoordinates.size(); i++)
		arrCoord[i] = allCoordinates[i];

	unsigned int arrTop[topCount];
	for (int i = 0; i < allTopology.size(); i++)
		arrTop[i] = allTopology[i];

	ps.InitialiseParticlePositions(xCoordinates, yCoordinates, zCoordinates);
	ps.InitialiseTriangleTopology(a, b, c);

	float** pos = ps.GetPositions();
	int** top = ps.GetTopology();

	cout << top[0][0] << endl;
	cout << top[0][1] << endl;
	cout << top[0][2] << endl;


}


int main()
{
	/*いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい*/
	//Initialise OpenGL following the The Cherno - OpenGL Series on youtube
	/*いいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいいい*/

	m_LeftButtonPressed = true;
	m_RightButtonPressed = false;

	GLFWwindow* window;

	glfwSetErrorCallback(error_callback);

	/* Initialize the library */
	if (!glfwInit())
		return -1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(ScreenWidth, ScreenHeight, "Hello World", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	const char* glfwVersion = glfwGetVersionString();
	cout << glfwVersion << endl;

	glfwSetCursorPosCallback(window, mouse_callback);

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	glfwSwapInterval(1); //

	//Needs to be initialised after we have a valid context
	if (glewInit() != GLEW_OK)
		std::cout << "Error at GLEW initialisation!" << std::endl;

	std::cout << glGetString(GL_VERSION) << std::endl;

	//Scope definition for variable life etc.
	{
		GLCall(glEnable(GL_BLEND));
		GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	
		//Initialisation of ImGUI
		ImGui::CreateContext();
		ImGui_ImplGlfwGL3_Init(window, true);
		ImGui::StyleColorsDark();
		
		testNS::Test* currentTest = nullptr;
		testNS::TestMenu* testMenu = new testNS::TestMenu(currentTest);
		currentTest = testMenu;
		
		//Creates new class instance when button is pressed
		testMenu->RegisterTest<testNS::TestBatch>("Batching");
		testMenu->RegisterTest<testNS::TestKeyCamera>("Camera");
		testMenu->RegisterTest<testNS::TestParticles3D>("Catenoid");
		testMenu->RegisterTest<testNS::TestInstancing>("Instancing");
		testMenu->RegisterTest<testNS::TestCircularDisc>("Disc");
		testMenu->RegisterTest<testNS::TestCylinder>("Cylinder");
		testMenu->RegisterTest<testNS::TestLight>("Light");

		/* Loop until the user closes the window */
		while (!glfwWindowShouldClose(window))
		{
			GLCall(glClearColor(0.0f,0.0f,0.0f,1.0f));
			GLCall(glClear(GL_COLOR_BUFFER_BIT));
			
			ImGui_ImplGlfwGL3_NewFrame();
			if (currentTest) //Testing that the object is not null
			{	
				ImVec2 wPos = ImGui::GetWindowPos();
				processInput(window, currentTest, ImGui::GetIO().MouseWheel);

				currentTest->OnUpdate(); //Run simulation here?
				currentTest->OnRender();
				ImGui::Begin("Test");

				if (currentTest != testMenu && ImGui::Button("<-"))
				{
					delete currentTest;
					currentTest = testMenu;
				}

				currentTest->OnImGuiRenderer();		//Registering a new test based on the button clicked.
				ImGui::End();
			}
			
			ImGui::Render();
			ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());
			GLCall(glfwSwapBuffers(window));
			GLCall(glfwPollEvents());

			ButtonFeedback(window);
		}

		delete currentTest;
		if (currentTest != testMenu)
			delete testMenu;

	}//Scope to avoid some OpenGL error (Video Abstracting OpenGL into Classes)

	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();
	glfwTerminate();


	return 0;
}







