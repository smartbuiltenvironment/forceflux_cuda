#pragma once

#include <stdio.h>
#include <vector>


#define ARRAYLENGTH(arr) (sizeof(arr) / sizeof(arr[0]))


using namespace std;


class ParticleSystem
{
public:

	ParticleSystem();

	~ParticleSystem();

	void InitialiseParticlePositions(vector<float>& x, vector<float>& y, vector<float>& z);

	void InitialiseTriangleTopology(vector<int>& a, vector<int>& b, vector<int>& c);


	float** GetPositions() 
	{
		return mPos;
	}

	int** GetTopology()
	{
		return mTop;
	}

	float* GetArrPositions()
	{
		return mArrPos;
	}

	int* GetArrTopology()
	{
		return mArrTop;
	}


private:

	int N;

	float* mArrPos;
	int* mArrTop;

	int** mTop;
	float** mPos;
	float* mVel;
	float* mAcc;
	float* mFor;


};







