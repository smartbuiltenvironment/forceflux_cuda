#pragma once
#include "Common.h"
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <fstream>

#include "plot/pbPlots.hpp"
#include "plot/supportLib.hpp"


class Structure
{
public:
	Structure();
	Structure(	int nA, 
				int nP, 
				int nZ, 
				float alpha, 
				int ppCount, 
				int paCount, 
				Vertex* vertices, 
				ArmIndices* arms, 
				float* massSize, 
				Vec3* loadVecs, 
				int* ppTop, 
				int* ppStartIndex, 
				int* ppRowLength, 
				int* paTop, 
				int* paStartIndex, 
				int* paRowLength, 
				bool* locked,
				bool* loaded,
				simulationType cType,
				ModelSettings ms,
				Stats stats,
				LoadingCylinder* loadCylinders);

	~Structure();

	void SetupKernel(float alpha);
	void SetupSolver();
	void SetupMaterial();
	void SetupLoad();
	void SetupDisp();
	void SetupSpring();
	void SetupPoissonsRatio();

	void CreateStructure(Vertex* vertices, ArmIndices* arms, float* massSize, Vec3* loadVecs, bool* locked, bool* loaded, LoadingCylinder* loadCylinders);
	void CopyTopology(int* ppTop, int* ppStartIndex, int* ppRowLength, int* pbTop, int* pbStartIndex, int* pbRowLength);
	bool InsideNonFailZone(Vec3 v1, Vec3 v2);

	void RunAnalysis();

	void ZeroParticleForce();
	void CalcArmForce();
	void CalcParticleMeanStrain();
	void CalcMass();
	void CalcDisplacement();
	void CalcDisplacementVertical();
	void CalcDisplacementHorizontal();

	void CalcReactionForce();
	void CalcConvergenceForce();
	void CalcPoissonsRatio();
	void CalcPoissonsRatioVertical();
	void CalcPoissonsRatioHorizontal();
	void CalcSectionStress();
	void CountBrokenArms();

	void CalcFirstPartialVelocity();
	void CalcSecondPartialVelocity();
	
	void CalcAcceleration();
	void ApplyExternalLoad();
	void ApplyExternalDisp();
	void ApplyExternalSpringDisp();
	void ApplyExternalSpringDisp2();

	void TestConsitencyCondition();

	void OutputResults();
	void CollectData();
	void ExportModel();

public:
	std::vector<Arm> m_arms;
	std::vector<Particle> m_particles;
	std::vector<Vec3> m_pInitPos;
	std::vector<bool> m_locked;
	std::vector<bool> m_loaded;

	Material m_m;
	DisplacementSettings m_disp;

	std::vector<LoadingCylinder> m_loadCylinders;

	std::vector<double> m_incDisp;

private:	
	
	KernelSettings m_kernel;
	SolverSettings m_solver;
	LoadSettings m_load;
	SpringSettings m_spring;
	PoissonsRatioSettings m_prs;
	ModelSettings m_ms;
	
	Stats m_stats;

	int m_nParticles; 
	int m_nArms;
	int m_nZones;
	int m_nBrokenArms;

	int m_pClosestToOrigin;

	std::vector<Vec3> m_loadDirVecs;

	std::vector<double> m_avrgDispTotVer;
	std::vector<double> m_avrgDispTop;
	std::vector<double> m_avrgDispBot;
	
	std::vector<double> m_avrgDispTotHor;
	std::vector<double> m_avrgDispLeft;
	std::vector<double> m_avrgDispRight;
	
	std::vector<double> m_incLoad;
	std::vector<double> m_incSpringForceTop;
	std::vector<double> m_incSpringForceBot;
	std::vector<double> m_incSectionStress;
	std::vector<double> m_incSectionStressCom;
	std::vector<double> m_incSectionStressTen;
	std::vector<double> m_reactionForceTop;
	std::vector<double> m_reactionForceBot;
	std::vector<double> m_incObfAvrg;
	std::vector<double> m_incPossionsRatio;

	std::vector<double> m_kernelConcistency;
	

	//Particle-particle topology
	int* m_ppTop;
	int* m_ppStartIndex;
	int* m_ppRowLength;
	int m_ppCount;

	bool m_plasticityOn;
	bool m_CheckFracture;
	bool m_nonFailOn;
	BC m_bc;
	simulationType m_simType;

	std::vector<int> m_ppTopV;
	std::vector<int> m_ppStartIndexV;
	std::vector<int> m_ppRowLengthV;

	//Particle-arm topology
	int* m_paTop;
	int* m_paStartIndex;
	int* m_paRowLength;
	int m_paCount;

	std::vector<int> m_paTopV;
	std::vector<int> m_paStartIndexV;
	std::vector<int> m_paRowLengthV;


};


