#include "ErrorCheck.h"



//Clear errors so we know there are no other errors from other functions
void GLClearError()
{
    while (glGetError() != GL_NO_ERROR);
}


//Check error that may have occured in this function call
bool GLLogCall(const char* function, const char* file, int line)
{
    while (GLenum error = glGetError())
    {
        std::cout << "[OpenGL Error] {" << error << "}:" << function <<
            " " << file << ":" << line << std::endl;
        return false;
    }
    return true;
}