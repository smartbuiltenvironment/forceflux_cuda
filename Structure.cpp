#include "Structure.h"


Structure::Structure()
{
}

Structure::Structure(
	int nParticles,
	int nArms,
	int nZones,
	float alpha,
	int ppCount,
	int paCount,
	Vertex* vertices,
	ArmIndices* arms,
	float* massSize,
	Vec3* loadVecs,
	int* ppTop,
	int* ppStartIndex,
	int* ppRowLength,
	int* paTop,
	int* paStartIndex,
	int* paRowLength,
	bool* locked,
	bool* loaded,
	simulationType cType,
	ModelSettings ms,
	Stats stats,
	LoadingCylinder* loadCylinders)
{
	m_nArms = nArms;
	m_nParticles = nParticles;
	m_nZones = nZones;

	m_plasticityOn = true;
	m_CheckFracture = true;
	m_nonFailOn = false;
	m_simType = cType;
	m_ms = ms;
	m_stats = stats;

	m_arms = std::vector<Arm>(m_nArms);
	m_particles = std::vector<Particle>(m_nParticles);
	m_pInitPos = std::vector<Vec3>(m_nParticles);
	m_loadDirVecs = std::vector<Vec3>(m_nParticles);
	m_locked = std::vector<bool>(m_nParticles);
	m_loaded = std::vector<bool>(m_nParticles);

	m_ppCount = ppCount;

	m_ppTopV = std::vector<int>(m_ppCount);
	m_ppStartIndexV = std::vector<int>(m_nParticles);
	m_ppRowLengthV = std::vector<int>(m_nParticles);

	m_paCount = paCount;

	m_paTopV = std::vector<int>(m_ppCount);
	m_paStartIndexV = std::vector<int>(m_nParticles);
	m_paRowLengthV = std::vector<int>(m_nParticles);

	m_bc = BC::springLoad;

	m_nBrokenArms = 0;

	SetupKernel(alpha);
	SetupSolver();
	SetupMaterial();
	SetupLoad();
	SetupDisp();
	SetupSpring();
	SetupPoissonsRatio();

	CreateStructure(vertices, arms, massSize, loadVecs, locked, loaded, loadCylinders);
	CopyTopology(ppTop, ppStartIndex, ppRowLength, paTop, paStartIndex, paRowLength);

	std::cout << "Structure created" << std::endl;
}

Structure::~Structure()
{

}

void Structure::SetupKernel(float alpha)
{
	int k = 2;
	float pi = 3.1415f;
	float C = (float)Factorial(2 * k + 3) / (4.0f * pi * pow(alpha, 3) * pow(2.0f, (2.0f * k)) * pow((float)Factorial(k), 2.0f));
	float C2 = (8.0f / 105.0f) * pow(alpha, 3);
	C = 2.0f;

	m_kernel.alpha = alpha;
	m_kernel.alphaSquared = alpha * alpha;
	m_kernel.alphaPowThree = pow(alpha, 3);
	m_kernel.alphaPowSeven = pow(alpha, 7);
	m_kernel.k = k;
	m_kernel.C = C;
}

void Structure::SetupSolver()
{
	if (m_simType == simulationType::Cylinder || m_simType == simulationType::SplitCylinder)
	{
		m_solver.dt = 0.0000015f; //0.000002f; 
		m_solver.co = 0.98f; //0.98f;
		m_solver.t = 0.0f;
		m_solver.t2 = m_solver.t + m_solver.dt;
		m_solver.t1 = 0.5 * (m_solver.t + m_solver.t2);
	}
	else if (m_simType == simulationType::ModulusOfRupture) 
	{
		m_solver.dt = 0.0000018f; //0.000002f; 
		m_solver.co = 0.98f; //0.98f;
		m_solver.t = 0.0f;
		m_solver.t2 = m_solver.t + m_solver.dt;
		m_solver.t1 = 0.5 * (m_solver.t + m_solver.t2);
	}
	else if (m_simType == simulationType::DirectTension)
	{
		m_solver.dt = 0.0000018f; //0.000002f; 
		m_solver.co = 0.98f; //0.98f;
		m_solver.t = 0.0f;
		m_solver.t2 = m_solver.t + m_solver.dt;
		m_solver.t1 = 0.5 * (m_solver.t + m_solver.t2);
	}
	else if (m_simType == simulationType::SteelTest)
	{
		m_solver.dt = 0.00000007f; //0.000002f; 
		m_solver.co = 0.98f; //0.98f;
		m_solver.t = 0.0f;
		m_solver.t2 = m_solver.t + m_solver.dt;
		m_solver.t1 = 0.5 * (m_solver.t + m_solver.t2);
	}


}

void Structure::SetupMaterial()
{
	//Data from: https://www.engineeringtoolbox.com/concrete-properties-d_1223.html
	//Compressive strength : 20 - 40 MPa(3000 - 6000 psi)
	//Flexural strength : 3 - 5 MPa(400 - 700 psi)
	//Tensile strength - ? : 2 - 5 MPa(300 - 700 psi)

	m_m.rho = 2400.0f;		//2240 - 2400 Kg/m3
	m_m.E = 40e9f;			//14 - 41 Gpa
	m_m.v = 0.2f;			//0.2 - 0.21
	m_m.G = m_m.E / (2.0f * (1.0f + m_m.v));				//21Gpa
	m_m.K = m_m.E / (3.0f * (1.0f - 2.0f * m_m.v));
	m_m.yieldStrain = 1.0f / 800.0f;		// 1/800
	m_m.elongationLimit = 1.0f / 100000.0f; //100000.0f;
	m_m.nonFailElongationLimit = 1.0f;

	if (m_simType == simulationType::SteelTest)
	{
		m_m.rho = 7800.0f;		
		m_m.E = 210e9f;			
		m_m.v = 0.28f;			
		m_m.G = m_m.E / (2.0f * (1.0f + m_m.v));				
		m_m.K = m_m.E / (3.0f * (1.0f - 2.0f * m_m.v));
		m_m.yieldStrain = 1.0f / 1500.0f;		// 1/800
		m_m.elongationLimit = 1.0f / 6000;		//10000.0f; //100000.0f;
		m_m.nonFailElongationLimit = 1.0f;
	}
}

void Structure::SetupLoad()
{
	m_load.magnitude = 1e9;		//N/m3
	m_load.nLoadSteps = 100;
	m_load.stepCounter = 1;
}

void Structure::SetupDisp()
{
	if (m_simType == simulationType::Cylinder)
	{
		m_disp.magnitude = 0.004;
		m_disp.nDispSteps = 4000;
		m_disp.stepCounter = 1;
	}
	else if(m_simType == simulationType::SplitCylinder)
	{
		m_disp.magnitude = 0.003;
		m_disp.nDispSteps = 2000;
		m_disp.stepCounter = 1;
	}
	else if (m_simType == simulationType::ModulusOfRupture)
	{
		m_disp.magnitude = 0.002;
		m_disp.nDispSteps = 5000;
		m_disp.stepCounter = 1;
	}
	else if (m_simType == simulationType::DirectTension)
	{
		m_disp.magnitude = 0.001;
		m_disp.nDispSteps = 5500;
		m_disp.stepCounter = 1;
	}
	else if (m_simType == simulationType::SteelTest)
	{
		m_disp.magnitude = 0.0025;  //0.003
		m_disp.nDispSteps = 100000;
		m_disp.stepCounter = 1;
		m_disp.startMag = 1.0000e-07;
		m_disp.endMag = 10.0000e-07;
		m_disp.loadIncreaseSteps = 1000;
		m_disp.loadIncreaseStepSize = (m_disp.endMag - m_disp.startMag) / m_disp.loadIncreaseSteps;
	}
}

void Structure::SetupSpring()
{
	m_spring.k = 1.0e6; //3.0e6;
	m_spring.iLength = 0.01;
}

void Structure::SetupPoissonsRatio()
{
	m_prs.hLimit = 0.95 * m_ms.height / 2.0f;
	m_prs.waistHeight = 0.5f * m_ms.height;
	m_prs.rLimit = 0.95f * m_ms.radius / 2.0f;
}

void Structure::CreateStructure(Vertex* vertices, ArmIndices* arms, float* massSize, Vec3* loadVecs, bool* locked, bool* loaded, LoadingCylinder* loadCylinders)
{
	float pi = 3.1415;
	double dOriginMin = 1e30;

	for (int i = 0; i < m_nParticles; i++)
	{
		float mass = ((4.0f / 3.0f) * pi * massSize[i] * massSize[i] * massSize[i]) * m_m.rho;

		Particle p;
		p.iPosition = vertices[i].Position;
		p.cPosition = vertices[i].Position;
		p.Color = vertices[i].Color;
		p.rMass = mass;
		p.h = massSize[i]; //m_kernel.alpha * massSize[i];
		p.Veloc = { 0, 0, 0 };
		p.Accel = { 0, 0, 0 };
		p.ExtForce = { 0, 0, 0 };
		p.TotForce = { 0, 0, 0 };
		p.fMass = mass;
		p.meanStrain = 0;
		p.rSize = massSize[i];
		p.damage = 0.0f;
		p.isBC = false;

		Vec3 v;
		v.x = p.cPosition.x;
		v.y = p.cPosition.y;
		v.z = p.cPosition.z;
		m_pInitPos[i] = v;

		m_locked[i] = locked[i];
		m_loaded[i] = loaded[i];
		m_loadDirVecs[i] = loadVecs[i];

		if (m_locked[i] || m_loaded[i])
			p.isBC = true;
		

		m_particles[i] = p;

		m_kernelConcistency.push_back(0.0f);

		float r = sqrt(v.x * v.x + v.y * v.y + v.z * v.z);

		if (r < dOriginMin) 
		{
			dOriginMin = r;
			m_pClosestToOrigin = i;
		}
	}


	for (int i = 0; i < 4; i++)
	{
		LoadingCylinder LC;

		LC.cPosition = loadCylinders[i].cPosition;
		LC.iPosition = loadCylinders[i].iPosition;
		LC.Normal = loadCylinders[i].Normal;
		LC.Radius = loadCylinders[i].Radius;
		LC.type = loadCylinders[i].type;

		m_loadCylinders.push_back(LC);
	}

	float length = 0;
	bool insideNonFailZone = false;

	int nonFailCounter = 0;

	for (int i = 0; i < m_nArms; i++)
	{
		Vec4 color = { 0.5f, 0.5f, 0.5f, 1.0f };
		Arm a;
		a.p1 = arms[i].p1;
		a.p2 = arms[i].p2; 

		length = Distance(m_particles[a.p1], m_particles[a.p2]);
		a.iLength = length;
		a.cLength = 0.0f;
		a.plasticStrain = 0.0f;
		a.strain = 0.0f;
		a.color = color;
		a.fMag = 0.0f;
		a.broken = false;
		a.elongation = 0.0f;
		a.elongationLimit = m_m.elongationLimit;
		a.insideNonFailZone = false;

		m_arms[i] = a;
	}

	std::cout << "Non fail arms count: " << nonFailCounter << std::endl;
}

void Structure::CopyTopology(int* ppTop, int* ppStartIndex, int* ppRowLength, int* paTop, int* paStartIndex, int* paRowLength)
{
	for (int i = 0; i < m_ppCount; i++)
		m_ppTopV[i] = ppTop[i];

	for (int i = 0; i < m_paCount; i++)
		m_paTopV[i] = paTop[i];

	for (int i = 0; i < m_nParticles; i++)
	{
		m_ppStartIndexV[i] = ppStartIndex[i];
		m_ppRowLengthV[i] = ppRowLength[i];
		m_paStartIndexV[i] = paStartIndex[i];
		m_paRowLengthV[i] = paRowLength[i];
	}
}

//Simulation
void Structure::RunAnalysis()
{
	ZeroParticleForce();			//P-loop

	CalcFirstPartialVelocity();		//P-loop

	CalcParticleMeanStrain();		//P-A-loop

	CalcArmForce();					//A-loop

	ApplyExternalSpringDisp2();		//P-loop

	CalcAcceleration();				//P-loop

	CalcSecondPartialVelocity();	//P-loop

	CalcReactionForce();			//P-loop

	CalcConvergenceForce();			//P-loop

	CalcPoissonsRatio();			//P-loop

	CalcDisplacement();				//P-loop

	//CalcSectionStress();			//A-loop

	//TestConsitencyCondition();	//P-A-loop

	//CalcDebugWeight();

	//CountBrokenArms();			//A-loop

	//CalcMass();

	m_disp.stepCounter++;

	//Increment time steps
	m_solver.t += m_solver.dt;
	m_solver.t2 = m_solver.t + m_solver.dt;
	m_solver.t1 = 0.5 * (m_solver.t + m_solver.t2);
}

void Structure::ZeroParticleForce()
{
	for (int i = 0; i < m_nParticles; i++)
	{
		m_particles[i].ArmForce.x = 0;
		m_particles[i].ArmForce.y = 0;
		m_particles[i].ArmForce.z = 0;
	}
}

void Structure::CalcFirstPartialVelocity()
{
	float co = m_solver.co;
	float t = m_solver.t;
	float t1 = m_solver.t1;
	float dt = m_solver.dt;

	for (int i = 0; i < m_nParticles; i++)
	{
		m_particles[i].Veloc.x = m_solver.co * m_particles[i].Veloc.x + (t1 - t) * m_particles[i].Accel.x;
		m_particles[i].Veloc.y = m_solver.co * m_particles[i].Veloc.y + (t1 - t) * m_particles[i].Accel.y;
		m_particles[i].Veloc.z = m_solver.co * m_particles[i].Veloc.z + (t1 - t) * m_particles[i].Accel.z;

		if (!m_loaded[i] && !m_locked[i])
		{
			//Allow movement in all directions
			m_particles[i].cPosition.x = m_particles[i].cPosition.x + m_particles[i].Veloc.x * dt;
			m_particles[i].cPosition.y = m_particles[i].cPosition.y + m_particles[i].Veloc.y * dt;
			m_particles[i].cPosition.z = m_particles[i].cPosition.z + m_particles[i].Veloc.z * dt;
		}
		else if ((m_loaded[i] || m_locked[i]) && m_bc == BC::springLoad)
		{
			//Allow movement in all directions
			m_particles[i].cPosition.x = m_particles[i].cPosition.x + m_particles[i].Veloc.x * dt;
			m_particles[i].cPosition.y = m_particles[i].cPosition.y + m_particles[i].Veloc.y * dt;
			m_particles[i].cPosition.z = m_particles[i].cPosition.z + m_particles[i].Veloc.z * dt;
		}
		else if (m_loaded[i] || m_locked[i] && m_bc == BC::prescribeZ)
		{
			//Allow movement in x and y direction
			m_particles[i].cPosition.x = m_particles[i].cPosition.x + m_particles[i].Veloc.x * dt;
			m_particles[i].cPosition.y = m_particles[i].cPosition.y + m_particles[i].Veloc.y * dt;
		}
		else if (m_loaded[i] || m_locked[i] && m_bc == BC::prescribeAll)
		{
			//Move nothing at all
		}
	}
}

void Structure::CalcParticleMeanStrain()
{
	float mb, ma, ha, hb, rab, Wab, Wba, strain_ab, hbCube;
	float rho = m_m.rho;
	Arm arm;
	Particle p, pNext;
	float damage = 0.0;
	int brokenBondsCounter = 0;
	

	for (int i = 0; i < m_nParticles; i++)
	{
		int startIndex = m_paStartIndexV[i];
		int rowLength = m_paRowLengthV[i];
		p = m_particles[i];
		ma = p.rMass;

		brokenBondsCounter = 0;

		float numerator = 0.0f;
		float denominator = 0.0f;

		//Loop around the connecting arms
		for (int j = 0; j < rowLength; j++)
		{
			int paFlatIndex = startIndex + j;
			int aNextIndex = m_paTopV[paFlatIndex];

			arm = m_arms[aNextIndex];
			rab = arm.cLength;

			pNext = m_particles[arm.p2];
			mb = pNext.rMass;
			hb = pNext.h;

			hbCube = hb * hb * hb;

			Wba = Evaluate(rab, hb, m_kernel);			//TODO: Precalculate values and store in an array instead?
			strain_ab = arm.strain;

			//Eq.(65) in paper version F. (Omitted in the final paper)
			numerator += (mb * strain_ab * Wba) / (rho * hbCube);
			denominator += (mb * Wba) / (rho * hbCube);

			if (arm.broken)
				brokenBondsCounter++;
		}

		damage = ((float)brokenBondsCounter) / ((float)rowLength);
		m_particles[i].damage = damage;

		if (denominator > 0)
		{
			m_particles[i].meanStrain = numerator / denominator;
		}
	}
}

void Structure::CalcArmForce()
{
	float ma, mb, Sa, Sb, rab, elongation, strain, plasticStrain;
	float rho = m_m.rho;
	float rhoSquared = rho * rho;
	Vec3 dirVec;

	for (int i = 0; i < m_nArms; i++)
	{
		if (!m_arms[i].broken)
		{
			Particle pa = m_particles[m_arms[i].p1];
			Particle pb = m_particles[m_arms[i].p2];

			m_arms[i].cLength = Distance(pa, pb);
			m_arms[i].elongation = m_arms[i].cLength - m_arms[i].iLength;
			m_arms[i].strain = m_arms[i].elongation / m_arms[i].iLength;

			if (!m_plasticityOn)
			{
				//Eq.(77)
				Sa = CalcForceFlux77(m_arms[i].strain, m_m, pa.meanStrain);
				Sb = CalcForceFlux77(m_arms[i].strain, m_m, pb.meanStrain);
			}
			else
			{
				strain = m_arms[i].strain;
				plasticStrain = m_arms[i].plasticStrain;

				//Eq. (111)
				if (strain - plasticStrain > m_m.yieldStrain)
					plasticStrain = strain - m_m.yieldStrain;
				else if (strain - plasticStrain < -m_m.yieldStrain)
					plasticStrain = strain + m_m.yieldStrain;

				m_arms[i].plasticStrain = plasticStrain;

				//Eq.(110)
				Sa = CalcForceFlux110(strain, m_arms[i].plasticStrain, m_m, pa.meanStrain);
				Sb = CalcForceFlux110(strain, m_arms[i].plasticStrain, m_m, pb.meanStrain);
			}

			float Wpa = EvaluateDerivative(m_arms[i].cLength, pa.h, m_kernel);
			float Wpb = EvaluateDerivative(m_arms[i].cLength, pb.h, m_kernel);
			bool isBroken = false;

			if (m_arms[i].elongation > m_m.elongationLimit)
				isBroken = true;

			float Tab = 0.0f;

			if (isBroken)
			{
				m_arms[i].fMag = 0.0f;
				m_arms[i].broken = true;
				m_arms[i].color = { 1.0f, 1.0f, 1.0f, 1.0f };
				m_nBrokenArms++;
			}
			else
			{
				ma = pa.rMass;
				mb = pb.rMass;

				//From eq.(70)
				Tab = -(ma * mb) * (((Sa * Wpa) / rhoSquared) + ((Sb * Wpb) / rhoSquared));
				m_arms[i].fMag = Tab;
				dirVec = GetUnitVector(pa, pb, m_arms[i].cLength);

				//Positive to pa
				m_particles[m_arms[i].p1].ArmForce.x += dirVec.x * Tab;
				m_particles[m_arms[i].p1].ArmForce.y += dirVec.y * Tab;
				m_particles[m_arms[i].p1].ArmForce.z += dirVec.z * Tab;

				//Negative to pb
				m_particles[m_arms[i].p2].ArmForce.x += -dirVec.x * Tab;
				m_particles[m_arms[i].p2].ArmForce.y += -dirVec.y * Tab;
				m_particles[m_arms[i].p2].ArmForce.z += -dirVec.z * Tab;
			}
		}
	}
}

void Structure::CountBrokenArms()
{
	int aCounter = 0;

	for (int i = 0; i < m_nArms; i++)
	{
		if (m_arms[i].broken)
			aCounter++;
	}

	std::cout << "Broken arms counter: " << aCounter << std::endl;
}

void Structure::ApplyExternalLoad()
{
	float pVol = 0;
	float load = 0;
	float loadFraction = (float)m_load.stepCounter / (float)m_load.nLoadSteps;

	if (m_load.stepCounter < m_load.nLoadSteps)
	{
		load = loadFraction * m_load.magnitude;
		m_incLoad.push_back(load);

		for (int i = 0; i < m_nParticles; i++)
		{
			pVol = m_particles[i].rMass / m_m.rho;

			m_particles[i].ExtForce.x = m_loadDirVecs[i].x * pVol * load;
			m_particles[i].ExtForce.y = m_loadDirVecs[i].y * pVol * load;
			m_particles[i].ExtForce.z = m_loadDirVecs[i].z * pVol * load;
		}
	}

	m_load.stepCounter++;
}

void Structure::ApplyExternalDisp()
{
	if (m_disp.stepCounter == 1)
		m_incDisp.push_back(0);

	float disp = 0;
	float dispFraction = 1.0f / (float)m_disp.nDispSteps;
	float dispIncrement = dispFraction * m_disp.magnitude;
	float accumulatedDisp = m_incDisp[m_disp.stepCounter - 1];

	if (m_disp.stepCounter < m_disp.nDispSteps)
	{
		for (int i = 0; i < m_nParticles; i++)
		{
			if (m_loaded[i] && m_bc == BC::prescribeAll)
			{
				m_particles[i].cPosition.x += m_loadDirVecs[i].x * dispIncrement;
				m_particles[i].cPosition.y += m_loadDirVecs[i].y * dispIncrement;
				m_particles[i].cPosition.z += m_loadDirVecs[i].z * dispIncrement;
			}
			else if (m_loaded[i] && m_bc == BC::prescribeZ)
			{
				m_particles[i].cPosition.z += m_loadDirVecs[i].z * dispIncrement;
			}
		}

		accumulatedDisp += dispIncrement;
		m_incDisp.push_back(accumulatedDisp);
	}
}

void Structure::ApplyExternalSpringDisp()
{
	if (m_disp.stepCounter == 1)
		m_incDisp.push_back(0);

	//Displace the top of the spring and resulting force to the particle

	float disp = 0;
	float dispStep = m_disp.magnitude / (float)m_disp.nDispSteps;
	float accumulatedDisp = m_incDisp[m_disp.stepCounter - 1];

	float totSpringForceTop = 0;
	float totSpringForceBot = 0;
	float topStress = 0;

	if (m_disp.stepCounter < m_disp.nDispSteps)
	{
		for (int i = 0; i < m_nParticles; i++)
		{
			if (m_loaded[i] || m_locked[i])
			{
				Vec3 dirVec = { 0.0f, 0.0f, -1.0 };
				if (m_locked[i])
					dirVec = { 0.0f, 0.0f, 1.0f };

				float springDeformation = accumulatedDisp;
				float f = m_spring.k * springDeformation;

				m_particles[i].ExtForce.x = dirVec.x * f;
				m_particles[i].ExtForce.y = dirVec.y * f;
				m_particles[i].ExtForce.z = dirVec.z * f;

				if (m_loaded[i]) 
					totSpringForceTop += f;
				else
					totSpringForceBot += f;
			}
		}

		accumulatedDisp += dispStep;
		m_incDisp.push_back(accumulatedDisp);

		m_incSpringForceTop.push_back(totSpringForceTop);
		m_incSpringForceBot.push_back(totSpringForceBot);
	}
}

void Structure::ApplyExternalSpringDisp2()
{
	if (m_disp.stepCounter == 1)
	{
		m_incDisp.push_back(0);
		m_incSpringForceTop.push_back(0);
		m_incSpringForceBot.push_back(0);
	}

	//Displace the top of the spring and resulting force to the particle

	float disp = 0;
	float dispStep = m_disp.magnitude / (float)m_disp.nDispSteps;
	float accumulatedDisp = m_incDisp[m_disp.stepCounter - 1];

	//Increasing the load in a linear fashion for the case of steel.
	/*if (m_simType == simulationType::SteelTest) 
	{
		if (m_disp.stepCounter < m_disp.loadIncreaseSteps)
		{
			dispStep = m_disp.startMag + m_disp.stepCounter * m_disp.loadIncreaseStepSize;
		}
		else
		{
			dispStep = m_disp.endMag;
		}
	}*/

	float totSpringForceTop = 0;
	float totSpringForceBot = 0;
	float topStress = 0;
	float dSz, dPz, f, sign;

	if (m_disp.stepCounter < m_disp.nDispSteps)
	{
		for (int i = 0; i < m_nParticles; i++)
		{
			if(m_loaded[i])
			{	
				sign = -1.0f;	//Displacement in downwards direction

				//Flipping the load in the tension test!
				if (m_simType == simulationType::DirectTension || m_simType == simulationType::SteelTest)
					sign = 1.0f;

				Vec3 dirVec = { 0.0f, 0.0f, sign };
				Vec3 springVec = { 0, 0, (sign * -1.0f) * m_spring.iLength };
				Vec3 springDispFreeEnd = { 0,0, sign * accumulatedDisp };

				Vec3 springFreePos_i = AddVectors(m_particles[i].iPosition, springVec);
				Vec3 springFreePos_c = AddVectors(springFreePos_i, springDispFreeEnd);
					
				Vec3 springPartPos_i = m_particles[i].iPosition;
				Vec3 springPartPos_c = m_particles[i].cPosition;

				float Li = Distance(springFreePos_i, springPartPos_i);
				float Lc = Distance(springFreePos_c, springPartPos_c);

				float dL = Li - Lc;
					
				//Esle the spring is elongated thus in tension and no force should be added
				if (dL > 0) 
				{
					f = dL * m_spring.k;
					m_particles[i].ExtForce.x = dirVec.x * f;
					m_particles[i].ExtForce.y = dirVec.y * f;
					m_particles[i].ExtForce.z = dirVec.z * f;
					totSpringForceTop += f;
				}
			}
			if (m_locked[i]) //&& (m_simType == simulationType::Cylinder || m_simType == simulationType::SplitCylinder))
			{
				sign = 1.0f;	//Displacement in upwards direction

				//Flipping the load in the tension test!
				if (m_simType == simulationType::DirectTension || m_simType == simulationType::SteelTest)
					sign = -1.0f;

				Vec3 dirVec = { 0.0f, 0.0f, sign };
				Vec3 springVec = { 0, 0, (sign * -1.0f) * m_spring.iLength };
				Vec3 springDispFreeEnd = { 0,0, sign * accumulatedDisp };

				Vec3 springFreePos_i = AddVectors(m_particles[i].iPosition, springVec);
				Vec3 springFreePos_c = AddVectors(springFreePos_i, springDispFreeEnd);

				Vec3 springPartPos_i = m_particles[i].iPosition;
				Vec3 springPartPos_c = m_particles[i].cPosition;

				float Li = Distance(springFreePos_i, springPartPos_i);
				float Lc = Distance(springFreePos_c, springPartPos_c);

				float dL = Li - Lc;

				//Esle the spring is elongated thus in tension and no force should be added
				if (dL > 0)
				{
					f = dL * m_spring.k;
					m_particles[i].ExtForce.x = dirVec.x * f;
					m_particles[i].ExtForce.y = dirVec.y * f;
					m_particles[i].ExtForce.z = dirVec.z * f;
					totSpringForceBot += f;
				}
			}
		}

		accumulatedDisp += dispStep;
		m_incDisp.push_back(accumulatedDisp);
		m_incSpringForceTop.push_back(totSpringForceTop);
		m_incSpringForceBot.push_back(totSpringForceBot);
	}
}

void Structure::CalcReactionForce()
{
	Vec3 reactionForceVecTop = { 0, 0, 0 };
	Vec3 reactionForceVecBot = { 0, 0, 0 };

	if (m_disp.stepCounter == 1)
	{
		m_reactionForceTop.push_back(0);
		m_reactionForceBot.push_back(0);
	}

	if (m_disp.stepCounter < m_disp.nDispSteps)
	{
		for (int i = 0; i < m_nParticles; i++)
		{
			if (m_loaded[i])
			{
				reactionForceVecTop.x += m_particles[i].ArmForce.x;
				reactionForceVecTop.y += m_particles[i].ArmForce.y;
				reactionForceVecTop.z += m_particles[i].ArmForce.z;
			}
			else if (m_locked[i]) 
			{
				reactionForceVecBot.x += m_particles[i].ArmForce.x;
				reactionForceVecBot.y += m_particles[i].ArmForce.y;
				reactionForceVecBot.z += m_particles[i].ArmForce.z;
			}
		}

		float reactionForceMagTop = GetVectorLength(reactionForceVecTop);
		float reactionForceMagBot = GetVectorLength(reactionForceVecBot);

		m_reactionForceTop.push_back(reactionForceMagTop);
		m_reactionForceBot.push_back(reactionForceMagBot);
	}
}

void Structure::CalcConvergenceForce()
{
	Vec3 obfVec = { 0, 0, 0 };

	if (m_disp.stepCounter == 1)
		m_incObfAvrg.push_back(0);

	int counter = 0;
	float avrgObf;

	for (int i = 0; i < m_nParticles; i++)
	{
		if (!m_loaded[i] && !m_locked[i])
		{
			obfVec.x += m_particles[i].ArmForce.x;
			obfVec.y += m_particles[i].ArmForce.y;
			obfVec.z += m_particles[i].ArmForce.z;
			counter++;
		}
	}

	avrgObf = GetVectorLength(obfVec) / ((float)counter);
	m_incObfAvrg.push_back(avrgObf);
}

void Structure::CalcAcceleration()
{
	//.9 Compute acceleration _accel = _rMass * f. Where f = f_int + f_ext + f_cont.
	//Also multiply with the volume to get the units right.

	for (int i = 0; i < m_nParticles; i++)
	{
		m_particles[i].Accel.x = (m_particles[i].ArmForce.x + m_particles[i].ExtForce.x) / m_particles[i].fMass;
		m_particles[i].Accel.y = (m_particles[i].ArmForce.y + m_particles[i].ExtForce.y) / m_particles[i].fMass;
		m_particles[i].Accel.z = (m_particles[i].ArmForce.z + m_particles[i].ExtForce.z) / m_particles[i].fMass;
	}
}

void Structure::CalcSecondPartialVelocity()
{
	float t1 = m_solver.t1;
	float t2 = m_solver.t2;

	//10.Second partial velocity update
	for (int i = 0; i < m_nParticles; i++)
	{
		m_particles[i].Veloc.x = m_particles[i].Veloc.x + (t2 - t1) * m_particles[i].Accel.x;
		m_particles[i].Veloc.y = m_particles[i].Veloc.y + (t2 - t1) * m_particles[i].Accel.y;
		m_particles[i].Veloc.z = m_particles[i].Veloc.z + (t2 - t1) * m_particles[i].Accel.z;
	}
}

void Structure::CalcDisplacement()
{
	if (m_simType == simulationType::Cylinder)
		CalcDisplacementVertical();
	else
		CalcDisplacementVertical();

}

void Structure::CalcDisplacementVertical()
{
	int topCounter = 0;
	int botCounter = 0;
	double avrgTopDisp = 0.0f;
	double avrgBotDisp = 0.0f;

	Vec3 topDisp = { 0, 0, 0 };
	Vec3 botDisp = { 0, 0, 0 };

	if (m_disp.stepCounter == 1)
	{
		m_avrgDispTop.push_back(0);
		m_avrgDispBot.push_back(0);
		m_avrgDispTotVer.push_back(0);
	}

	for (int i = 0; i < m_nParticles; i++)
	{
		//If true we'r at a particle in the top of the cylinder
		if (m_loaded[i])
		{
			float xDiff = m_particles[i].cPosition.x - m_particles[i].iPosition.x;
			float yDiff = m_particles[i].cPosition.y - m_particles[i].iPosition.y;
			float zDiff = m_particles[i].cPosition.z - m_particles[i].iPosition.z;

			topDisp.x += xDiff;
			topDisp.y += yDiff;
			topDisp.z += zDiff;
			topCounter++;
		}
		else if (m_locked[i])
		{
			float xDiff = m_particles[i].cPosition.x - m_particles[i].iPosition.x;
			float yDiff = m_particles[i].cPosition.y - m_particles[i].iPosition.y;
			float zDiff = m_particles[i].cPosition.z - m_particles[i].iPosition.z;

			botDisp.x += xDiff;
			botDisp.y += yDiff;
			botDisp.z += zDiff;
			botCounter++;
		}
	}

	float topDispMag = GetVectorLength(topDisp) / (float)topCounter;
	float botDispMag = GetVectorLength(botDisp) / (float)botCounter;
	float totDispMag = topDispMag + botDispMag;

	m_avrgDispTop.push_back(topDispMag);
	m_avrgDispBot.push_back(botDispMag);
	m_avrgDispTotVer.push_back(totDispMag);
}

void Structure::CalcDisplacementHorizontal() 
{
	int leftCounter = 0;
	int rightCounter = 0;
	double avrgLeftDisp = 0.0f;
	double avrgRightDisp = 0.0f;

	Vec3 leftDisp = { 0, 0, 0 };
	Vec3 rightDisp = { 0, 0, 0 };

	if (m_disp.stepCounter == 1)
	{
		m_avrgDispLeft.push_back(0);
		m_avrgDispRight.push_back(0);
		m_avrgDispTotHor.push_back(0);
	}

	//Should be less the 1 to select part of the domain.
	float factor = 0.85;

	for (int i = 0; i < m_nParticles; i++)
	{
		//If true we'r the left side of the cylinder..
		if(m_particles[i].iPosition.y < -1 * factor * m_ms.radius)
		{
			float xDiff = m_particles[i].cPosition.x - m_particles[i].iPosition.x;
			float yDiff = m_particles[i].cPosition.y - m_particles[i].iPosition.y;
			float zDiff = m_particles[i].cPosition.z - m_particles[i].iPosition.z;

			leftDisp.x += xDiff;
			leftDisp.y += yDiff;
			leftDisp.z += zDiff;
			leftCounter++;
		}
		else if (m_particles[i].iPosition.y > factor * m_ms.radius)
		{
			float xDiff = m_particles[i].cPosition.x - m_particles[i].iPosition.x;
			float yDiff = m_particles[i].cPosition.y - m_particles[i].iPosition.y;
			float zDiff = m_particles[i].cPosition.z - m_particles[i].iPosition.z;

			rightDisp.x += xDiff;
			rightDisp.y += yDiff;
			rightDisp.z += zDiff;
			rightCounter++;
		}
	}

	float leftDispMag = GetVectorLength(leftDisp) / (float)leftCounter;
	float rightDispMag = GetVectorLength(rightDisp) / (float)rightCounter;
	float totDispMag = leftDispMag + rightDispMag;

	m_avrgDispLeft.push_back(leftDispMag);
	m_avrgDispRight.push_back(rightDispMag);
	m_avrgDispTotHor.push_back(totDispMag);
}

void Structure::CalcPoissonsRatio()
{
	//Poissons ratio is only calculated for the vertical cylinder
	if (m_simType == simulationType::Cylinder)
		CalcPoissonsRatioVertical();
}

void Structure::CalcPoissonsRatioHorizontal()
{
	float hLimit = 0.95f * m_ms.radius;
	float wLimit = 0.95f * m_ms.radius;
	float avrgWi = 0;
	float avrgWc = 0;
	float avrgHi = 0;
	float avrgHc = 0;
	int wCounter = 0;
	int hCounter = 0;

	for (int i = 0; i < m_nParticles; i++)
	{
		//Width condition. Particles here are used to calculate the change in width.
		if (m_particles[i].iPosition.y < -1.0f * wLimit || m_particles[i].iPosition.y > wLimit)
		{
			float yi = m_particles[i].iPosition.y;
			float yc = m_particles[i].cPosition.y;
			
			avrgWi += yi * 2;
			avrgWc += yc * 2;
			wCounter++;
		}

		//Height condition. Particles here are used to calculate the change in hight for the cylinder.
		if (m_particles[i].iPosition.z < -1.0f * hLimit || m_particles[i].iPosition.z > hLimit)
		{
			float zi = std::abs(m_particles[i].iPosition.z);
			float zc = std::abs(m_particles[i].cPosition.z);

			avrgHi += zi * 2;
			avrgHc += zc * 2;
			hCounter++;
		}
	}
	
	avrgWi = avrgWi / wCounter;
	avrgWc = avrgWc / wCounter;

	avrgHi = avrgHi / hCounter;
	avrgHc = avrgHc / hCounter;

	float numerator = (avrgWc - avrgWi) / avrgWi;
	float denominator = (avrgHc - avrgHi) / avrgHi;

	if (denominator != 0)
	{
		float poisson = -1.0f * numerator / denominator;
		m_incPossionsRatio.push_back(poisson);
	}

}

void Structure::CalcPoissonsRatioVertical() 
{
	float avrgDi = 0;
	float avrgDc = 0;
	float avrgHi = 0;
	float avrgHc = 0;

	int hCounter = 0;
	int rCounter = 0;

	for (int i = 0; i < m_nParticles; i++) 
	{
		//Waist condition. Particles here are used to calculate the change in diameter.
		if (m_particles[i].iPosition.z < m_prs.waistHeight / 2.0f && m_particles[i].iPosition.z > -1 * (m_prs.waistHeight / 2.0f))
		{
			float xi = m_particles[i].iPosition.x;
			float yi = m_particles[i].iPosition.y;
			float ri = sqrt(xi * xi + yi * yi);
			float xc = m_particles[i].cPosition.x;
			float yc = m_particles[i].cPosition.y;
			float rc = sqrt(xc * xc + yc * yc);

			if (ri > m_prs.rLimit)
			{
				avrgDi += ri * 2;
				avrgDc += rc * 2;
				rCounter++;
			}
		}

		//Height condition. Particles here are used to calculate the change in hight for the cylinder.
		if (m_particles[i].iPosition.z < -1 * m_prs.hLimit || m_particles[i].iPosition.z > m_prs.hLimit)
		{
			float zi = std::abs(m_particles[i].iPosition.z);
			float zc = std::abs(m_particles[i].cPosition.z);

			avrgHi += zi * 2;
			avrgHc += zc * 2;
			hCounter++;
		}
	}
	
	//Make values average.
	avrgDi = avrgDi / rCounter;
	avrgDc = avrgDc / rCounter;

	avrgHi = avrgHi / hCounter;
	avrgHc = avrgHc / hCounter;

	float numerator = (avrgDc - avrgDi) / avrgDi;
	float denominator = (avrgHc - avrgHi) / avrgHi;

	if (denominator != 0) 
	{
		float poisson = -1.0f * numerator / denominator;
		m_incPossionsRatio.push_back(poisson);
	}
}

void Structure::CalcSectionStress() 
{
	if (m_disp.stepCounter == 1) 
	{
		m_incSectionStress.push_back(0);
		m_incSectionStressTen.push_back(0);
		m_incSectionStressCom.push_back(0);
	}

	float fTot = 0.0f;
	float tenTot = 0.0f;
	float comTot = 0.0f;

	float area = 0.0f;
	float areaCom = 0.0f;
	float areaTen = 0.0f;
	int counter = 0;
	int tenCounter = 0;
	int comCounter = 0;
	int brokenCounter = 0;

	if (m_simType == simulationType::Cylinder) 
	{	
		for (int i = 0; i < m_arms.size(); i++)
		{	
			//Looking for stress i z-dir
			float z1 = m_particles[m_arms[i].p1].iPosition.z;
			float z2 = m_particles[m_arms[i].p2].iPosition.z;

			if (std::signbit(z1) != std::signbit(z2))
			{
				Vec3 dirVec = GetUnitVector(m_particles[m_arms[i].p1], m_particles[m_arms[i].p2], m_arms[i].cLength);
				float fMag = m_arms[i].fMag;
				float fz = std::abs(dirVec.z) * fMag;
				fTot += fz;
			}
		}
		area = m_ms.AreaSmall;
	}
	else //Horizontal
	{
		for (int i = 0; i < m_arms.size(); i++)
		{
			//Looking for stress in y-dir
			float y1 = m_particles[m_arms[i].p1].iPosition.y;
			float y2 = m_particles[m_arms[i].p2].iPosition.y;

			if((y1 > 0 && y2 < 0) || (y2 > 0 && y1 < 0))			//if (std::signbit(y1) != std::signbit(y2))
			{
				Vec3 dirVec = GetUnitVector(m_particles[m_arms[i].p1], m_particles[m_arms[i].p2], m_arms[i].cLength);
				float fMag = m_arms[i].fMag;
				float fy = std::abs(dirVec.y) * fMag;

				if (m_arms[i].broken) 
				{
					brokenCounter++;
				}
				else if (fy > 0)
				{
					tenTot += fy;
					tenCounter++;
				}
				else
				{
					comTot += fy;
					comCounter++;
				}
				
				fTot += fy;
				counter++;
			}
		}
		area = m_ms.AreaLarge;
		areaTen = area;
		areaCom = area;

		if(tenCounter > 0 && counter > 0)
			areaTen = (((float)tenCounter + (float) brokenCounter) / (float)counter) * area;
		
		if(comCounter > 0 && counter > 0)
			areaCom = ((float)comCounter / (float)counter) * area;
	}


	float stress = fTot / area;
	float stressTen = tenTot / areaTen;
	float stressCom = comTot / areaCom;

	m_incSectionStress.push_back(stress);
	m_incSectionStressTen.push_back(stressTen);
	m_incSectionStressCom.push_back(stressCom);
}

void Structure::CalcMass() 
{
	float mTot = 0;

	for (int i = 0; i < m_nParticles; i++) 
	{
		mTot += m_particles[i].rMass;
	}

	std::cout << "Total Mass: " << mTot << std::endl;
}

bool Structure::InsideNonFailZone(Vec3 v1, Vec3 v2)
{
	Vec3 midPt = GetAverageVector(v1, v2);
	midPt.y = 0.0f;
	float scaleFactor = 2.0f;
	float dist;
	float smallestDist = 1e30;
	float rLimit = scaleFactor * m_loadCylinders[0].Radius;		//Assuming all load cylidners have the same size

	for (int i = 0; i < m_loadCylinders.size(); i++)
	{
		//if (m_loadCylinders[i].type == BCtype::load) 
		{
			dist = Distance(m_loadCylinders[i].cPosition, midPt);

			if (dist < smallestDist)
				smallestDist = dist;
		}
	}

	if (smallestDist < rLimit)
		return true;

	return false;
}

void Structure::TestConsitencyCondition()
{
	float num; //Should be close to 1.
	Arm arm;
	float avrgNum = 0;
	int counter = 0;
	float rab, mb;
	Particle pNext;

	float totVol = 0;


	for (int i = 0; i < m_nParticles; i++) 
	{
		int startIndex = m_paStartIndexV[i];
		int rowLength = m_paRowLengthV[i];
		Particle p = m_particles[i];
		
		float W, Wprime, dV;

		num = 0;

		float pdV = p.rMass / m_m.rho;

		W = Evaluate(0.0f, p.h, m_kernel);

		//Adding the particle iself
		num += W * pdV;

		totVol += pdV;

		if (i == m_pClosestToOrigin) 
		{
			int a = 10;
		}

		
		//Loop around the connecting arms
		for (int j = 0; j < rowLength; j++)
		{
			int paFlatIndex = startIndex + j;
			int aNextIndex = m_paTopV[paFlatIndex];

			arm = m_arms[aNextIndex];
			rab = arm.cLength;

			pNext = m_particles[arm.p2];
			
			W = Evaluate(rab, pNext.h, m_kernel);

			Wprime = abs(EvaluateDerivative(rab, pNext.h, m_kernel));

			dV = pNext.rMass / m_m.rho;

			num += W * dV;

			counter++;
		}

		avrgNum += num;

		m_kernelConcistency[i] = num;
	}

	avrgNum = avrgNum / m_nParticles;

	std::cout << "Kernel concistency number average: " << avrgNum << std::endl;

	std::cout << "Total volume: " << totVol << std::endl;

		
}

void Structure::OutputResults()
{
	CollectData();

	float area = 0.0f;
	float height = 0.0f;
	float diameter = 0.0f;
	
	if (m_simType == simulationType::Cylinder)
	{	
		area = m_ms.AreaSmall;
		height = m_ms.height;

		std::ofstream fileStress("output/stressCC.txt");
		for (int i = 0; i < m_reactionForceTop.size(); i++)
		{
			float stress = m_reactionForceTop[i] / area;
			fileStress << stress << '\n';
		}
		fileStress.close();

		std::ofstream fileStrain("output/strainCC.txt");
		for (int i = 0; i < m_avrgDispTotVer.size(); i++)
		{
			float strain = m_avrgDispTotVer[i] / height;
			fileStrain << strain << '\n';
		}
		fileStrain.close();

		std::ofstream filePoisson("output/poissonCC.txt");
		for (int i = 0; i < m_incPossionsRatio.size(); i++)
		{
			float poisson = m_incPossionsRatio[i];
			filePoisson << poisson << '\n';
		}
		filePoisson.close();

		std::ofstream fileSF("output/springForceCC.txt");
		for (int i = 0; i < m_incSpringForceTop.size(); i++)
		{
			float springForce = m_incSpringForceTop[i];
			fileSF << springForce << '\n';
		}
		fileSF.close();

		std::ofstream fileOBF("output/obfCC.txt");
		for (int i = 0; i < m_incObfAvrg.size(); i++)
		{
			float obf = m_incObfAvrg[i];
			fileOBF << obf << '\n';
		}
		fileOBF.close();
	}
	else if (m_simType == simulationType::SplitCylinder) //Horizontal cylinder or Modulus of rupture
	{
		std::ofstream fileSF("output/springForceSC.txt");
		for (int i = 0; i < m_incSpringForceTop.size(); i++)
		{
			float springForce = m_incSpringForceTop[i];
			fileSF << springForce << '\n';
		}
		fileSF.close();

	}
	else if (m_simType == simulationType::ModulusOfRupture) 
	{
		std::ofstream fileSF("output/springForceMOR.txt");
		for (int i = 0; i < m_incSpringForceTop.size(); i++)
		{
			float springForce = m_incSpringForceTop[i];
			fileSF << springForce << '\n';
		}
		fileSF.close();
	}
	else if (m_simType == simulationType::DirectTension)
	{
		area = m_ms.AreaSmall;

		std::ofstream fileSF("output/springForceDT.txt");
		for (int i = 0; i < m_incSpringForceTop.size(); i++)
		{
			float springForce = m_incSpringForceTop[i];
			fileSF << springForce << '\n';
		}
		fileSF.close();

		std::ofstream fileStress("output/stressDT.txt");
		for (int i = 0; i < m_reactionForceTop.size(); i++)
		{
			float stress = m_reactionForceTop[i] / area;
			fileStress << stress << '\n';
		}
		fileStress.close();
	}
	else if (m_simType == simulationType::SteelTest)
	{
		area = m_ms.AreaSmall;
		height = m_ms.height;

		std::ofstream fileSF("output/springForceST.txt");
		for (int i = 0; i < m_incSpringForceTop.size(); i++)
		{
			float springForce = m_incSpringForceTop[i];
			fileSF << springForce << '\n';
		}
		fileSF.close();

		std::ofstream fileStrain("output/strainST.txt");
		for (int i = 0; i < m_avrgDispTotVer.size(); i++)
		{
			float strain = m_avrgDispTotVer[i] / height;
			fileStrain << strain << '\n';
		}
		fileStrain.close();

		std::ofstream fileStress("output/stressST.txt");
		for (int i = 0; i < m_reactionForceTop.size(); i++)
		{
			float stress = m_reactionForceTop[i] / area;
			fileStress << stress << '\n';
		}
		fileStress.close();

		std::ofstream fileLoad("output/loadST.txt");
		for (int i = 0; i < m_incDisp.size(); i++)
		{
			float load = m_incDisp[i];
			fileLoad << load << '\n';
		}
		fileLoad.close();

	}



}

void Structure::CollectData() 
{
	std::string filename = "";

	if (m_simType == simulationType::Cylinder)
	{
		filename = "output/simulationDataCC.txt";
	}
	else if (m_simType == simulationType::SplitCylinder)
	{
		filename = "output/simulationDataSC.txt";
	}
	else if (m_simType == simulationType::ModulusOfRupture)
	{
		filename = "output/simulationDataMOR.txt";
	}
	else if (m_simType == simulationType::DirectTension)
	{
		filename = "output/simulationDataDT.txt";
	}
	else if (m_simType == simulationType::SteelTest)
	{
		filename = "output/simulationDataST.txt";
	}

	std::ofstream file(filename);

	if (m_simType == simulationType::Cylinder)
	{
		file << "Vertical Cylinder Simulation Settings" << '\n';	
		file << "Cylinder height: " << m_ms.height << '\n';
		file << "Cylinder radius: " << m_ms.radius << '\n';
		
		file << "Poissions ratio height limit: " << m_prs.hLimit << '\n';
		file << "Poissions ratio waist height: " << m_prs.waistHeight << '\n';
		file << "Poissions ratio radius limit: " << m_prs.rLimit << '\n';
	}
	else if (m_simType == simulationType::SplitCylinder) 
	{
		file << "Horizontal Cylinder Simulation Settings" << '\n';
		file << "Cylinder length: " << m_ms.height << '\n';
		file << "Cylinder radius: " << m_ms.radius << '\n';
	}
	else if (m_simType == simulationType::ModulusOfRupture)
	{
		file << "MOR Beam Simulation Settings" << '\n';
		file << "Beam length: " << m_ms.bbSize.x - 2.0f * m_ms.edgePadding << '\n';
		file << "Additional length padding: " << m_ms.edgePadding << '\n';
 		file << "Beam height: " << m_ms.bbSize.z << '\n';
		file << "Beam width: " << m_ms.bbSize.y << '\n';
	}
	else if (m_simType == simulationType::DirectTension)
	{
		file << "Direct Tension Simulation Settings" << '\n';
		file << "Specimen height: " << m_ms.bbSize.z << '\n';
		file << "Diameter (Large): " << m_ms.bbSize.x << '\n';
		file << "Diameter waist " << 2.0f * m_ms.radius * m_ms.waistSF << '\n';
	}
	else if (m_simType == simulationType::SteelTest)
	{
		file << "Direct Tension Simulation Settings" << '\n';
		file << "Specimen height: " << m_ms.bbSize.z << '\n';
		file << "Diameter (Large): " << m_ms.bbSize.x << '\n';
		file << "Diameter waist " << 2.0f * m_ms.radius * m_ms.waistSF << '\n';
	}


	file << "Random seed for shaking: " << m_ms.rndSeed << '\n';

	file << "BC thickness: " << m_ms.bcThickness << '\n';

	file << "Cross section area small: " << m_ms.AreaSmall << '\n';
	file << "Cross section area large: " << m_ms.AreaLarge << '\n';
	
	file << "nP: " << m_nParticles << '\n';
	file << "nA: " << m_nArms << '\n';
	file << "nZ: " << m_nZones << '\n';

	file << "Average arm count per particle: " << m_stats.nBondsPerParticle << '\n';
	file << "Average particle count per zone: " << m_stats.nParticlesPerZone << '\n';
	file << "Smallest zone over horizon (should be greater than 1!):: " << m_stats.zoneSizeParticleSizeRatio << '\n';

	file << "Elongation limit: " << m_m.elongationLimit << '\n';
	file << "Yield strain: " << m_m.yieldStrain << '\n';
	file << "Youngs Modulus: " << m_m.E << '\n';
	file << "Shear Modulus: " << m_m.G << '\n';
	file << "Bulk Modulus: " << m_m.K << '\n';
	file << "Poissons number: " << m_m.v << '\n';

	file << "Time step: " << m_solver.dt << '\n';
	file << "Carry over factor: " << m_solver.co << '\n';

	file << "Displacement magnitude: " << m_disp.magnitude << '\n';
	file << "Max load steps: " << m_disp.nDispSteps << '\n';
	file << "Used load steps: " << m_disp.stepCounter << '\n';

	file << "Kernel k: " << m_kernel.k << '\n';
	file << "Kernel C: " << m_kernel.C << '\n';
	file << "Kernel alpha: " << m_kernel.alpha << '\n';

	file << "Plasticity on: " << m_plasticityOn << '\n';
	file << "Check fracture on: " << m_CheckFracture << '\n';

	file << "Load spring initial length: " << m_spring.iLength << '\n';
	file << "Load spring stiffness: " << m_spring.k << '\n';

	file.close();

}

void Structure::ExportModel() 
{
	std::ofstream fileP("output/coord.txt");

	for (int i = 0; i < m_particles.size(); i++)
	{
		float x = m_particles[i].iPosition.x;
		float y = m_particles[i].iPosition.y;
		float z = m_particles[i].iPosition.z;
		float r = m_particles[i].rSize;

		fileP << x << ',' << y << ',' << z << ',' << r << '\n';
	}
	fileP.close();

	std::ofstream fileC("output/color.txt");

	for (int i = 0; i < m_particles.size(); i++)
	{
		float x = m_particles[i].Color.x;
		float y = m_particles[i].Color.y;
		float z = m_particles[i].Color.z;
		float r = m_particles[i].Color.w;

		fileC << x << ',' << y << ',' << z << ',' << r << '\n';
	}
	fileC.close();

	/*
	std::ofstream file2("output/arms1.txt");
	std::ofstream file3("output/arms2.txt");

	for (int i = 0; i < m_arms.size(); i++)
	{
		//Looking for stress in y-dir
		float y1 = m_particles[m_arms[i].p1].iPosition.y;
		float y2 = m_particles[m_arms[i].p2].iPosition.y;

		if ((y1 > 0 && y2 < 0) || (y2 > 0 && y1 < 0))			//if (std::signbit(y1) != std::signbit(y2))
		{
			Vec3 dirVec = GetUnitVector(m_particles[m_arms[i].p1], m_particles[m_arms[i].p2], m_arms[i].cLength);
			float fMag = m_arms[i].fMag;
			float fy = std::abs(dirVec.y) * fMag;

			float x1 = m_particles[m_arms[i].p1].iPosition.x;
			float y1 = m_particles[m_arms[i].p1].iPosition.y;
			float z1 = m_particles[m_arms[i].p1].iPosition.z;
			
			float x2 = m_particles[m_arms[i].p2].iPosition.x;
			float y2 = m_particles[m_arms[i].p2].iPosition.y;
			float z2 = m_particles[m_arms[i].p2].iPosition.z;

			file2 << x1 << ',' << y1 << ',' << z1 << ',' << fy << '\n';
			file3 << x2 << ',' << y2 << ',' << z2 << ',' << fy << '\n';
		}
	}

	file2.close();
	file3.close();
	*/
}




