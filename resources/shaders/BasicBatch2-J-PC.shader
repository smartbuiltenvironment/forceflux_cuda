#shader vertex
#version 450 core
layout(location = 0) in vec3 position;
layout(location = 1) in vec4 a_Color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
out vec4 v_Color;


void main()
{
	gl_Position = projection * view * model * vec4(position, 1.0);
	v_Color = a_Color;
}

#shader fragment
#version 450 core
layout(location = 0) out vec4 o_Color;

in vec4 v_Color;

void main()
{	
	//o_Color = vec4(1.0);
	o_Color = v_Color;
}