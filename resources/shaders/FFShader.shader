#shader vertex
#version 450 core

layout(location = 0) in vec3 a_Position;
layout(location = 1) in vec4 a_Color;

uniform mat4 u_Model;
uniform mat4 u_View;
uniform mat4 u_Proj;
uniform float u_clipDistance1;
uniform float u_clipDistance2;
uniform float u_clipDistance3;
uniform float u_clipDistance4;

out vec4 v_Color;

void main()
{
	vec4 clippingPlane1 = vec4(0, 1, 0, u_clipDistance1);
	vec4 clippingPlane2 = vec4(0, -1, 0, u_clipDistance2);
	vec4 clippingPlane3 = vec4(0, 0, -1, u_clipDistance3);
	vec4 clippingPlane4 = vec4(0, 0, 1, u_clipDistance4);

	vec4 worldPosition = u_Model * vec4(a_Position, 1);

	gl_ClipDistance[0] = dot(worldPosition, clippingPlane1);
	gl_ClipDistance[1] = dot(worldPosition, clippingPlane2);
	gl_ClipDistance[2] = dot(worldPosition, clippingPlane3);
	gl_ClipDistance[3] = dot(worldPosition, clippingPlane4);

	gl_Position = u_Proj * u_View * worldPosition;
	v_Color = a_Color;
};

#shader fragment
#version 450 core
layout(location = 0) out vec4 o_Color;

in vec4 v_Color;

void main()
{
	o_Color = v_Color;
};