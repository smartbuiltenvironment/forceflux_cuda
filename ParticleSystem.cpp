#include "ParticleSystem.h"



ParticleSystem::ParticleSystem()
{
	N = 0;
	mPos = 0;
	mVel = 0;
	mAcc = 0;
	mFor = 0;
	mTop = 0;

	mArrPos = 0;
	mArrTop = 0;
}


ParticleSystem::~ParticleSystem() 
{
	

}


void ParticleSystem::InitialiseParticlePositions(vector<float> &x, vector<float>& y, vector<float>& z)
{
	int size = x.size();

	mPos = new float*[size];

	for (int i = 0; i < size; i++)
	{
		mPos[i] = new float[3];
	}

	for (int i = 0; i < size; i++)
	{
		mPos[i][0] = x[i];
		mPos[i][1] = y[i];
		mPos[i][2] = z[i];
	}
	
	return;
}


void ParticleSystem::InitialiseTriangleTopology(vector<int>& a, vector<int>& b, vector<int>& c)
{
	int size = a.size();

	mTop = new int* [size];
	
	for (int i = 0; i < size; i++)
	{
		mTop[i] = new int[3];
	}

	for (int i = 0; i < size; i++)
	{
		mTop[i][0] = a[i];
		mTop[i][1] = b[i];
		mTop[i][2] = c[i];
	}

	return;
}


