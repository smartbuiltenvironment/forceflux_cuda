#include "Test.h"
#include "imgui/imgui.h"

namespace testNS 
{

	TestMenu::TestMenu(Test*& currentTestPointer)
		:m_CurrentTest(currentTestPointer)
	{
	}

	void TestMenu::OnImGuiRenderer() 
	{
		for (auto& test : m_Tests)
		{
			if (ImGui::Button(test.first.c_str()))			//.first referes to the key.
			{
				m_CurrentTest = test.second();				//.second referes to the value
			}
		}
	}

}
