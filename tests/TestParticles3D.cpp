

#include "TestParticles3D.h" 
#include "imgui/imgui.h"
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include <array>


namespace testNS 
{

	static std::array<Vertex, 4> CreateQuad(float x, float y, float z, float size)
	{
		Vertex v0, v1, v2, v3;
		v0.Position = { x - size, y - size, z-size };
		v0.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v1.Position = { x + size, y - size, z - size };
		v1.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v2.Position = { x + size, y + size, z - size };
		v2.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v3.Position = { x - size, y + size, z - size };
		v3.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		return { v0,v1,v2,v3 };
	}

	static Vec3 Add3Vec3s(Vec3 v1, Vec3 v2, Vec3 v3) 
	{
		return { v1.x + v2.x + v3.x, v1.y + v2.y + v3.y, v1.z + v2.z + v3.z };
	}

	static Vec3 Reverse(Vec3 v1)
	{
		return {-1.0f * v1.x, -1.0f * v1.y, -1.0f * v1.z};
	}


	static std::array<Vertex, 4> CreatePerpQuad(float x, float y, float z, Vec3 xVec, Vec3 yVec)
	{
		Vertex v0, v1, v2, v3;
		v0.Position =  Add3Vec3s({x, y, z}, Reverse(xVec), Reverse(yVec));
		v0.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v1.Position = Add3Vec3s({ x, y, z }, xVec, Reverse(yVec));
		v1.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v2.Position = Add3Vec3s({ x, y, z }, xVec, yVec);
		v2.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v3.Position = Add3Vec3s({ x, y, z }, Reverse(xVec), yVec);
		v3.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		return { v0,v1,v2,v3 };
	}



	TestParticles3D::TestParticles3D()
		:   m_Proj(glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f)),
			m_View(glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up)),
			m_Model(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),			//glm::mat4 m4(1.0f) => constructs an identity matrix
			m_TranslationA(0, 0, 0),
			m_TranslationB(0, 0, 0),
			m_nParticles(1000000),
			m_Time(0.0f),
			m_Fov(45.0f),
			m_pSize(0.003f)
	{
		m_Camera = Camera();
		m_Shader = make_unique<Shader>("resources/shaders/BasicBatch2.shader");
		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
			
		m_ParticleVertices = new Vertex[m_nParticles * 4];			//4 vertices per quad
		m_ParticlePoints = new Point[m_nParticles];			//4 vertices per quad
		m_ParticleIndices = new unsigned int[m_nParticles * 6];		//6 indices per quad
		
		m_ColorStep = 0.05f;
		m_ColorIncrement = m_ColorStep;
		m_ColorScaleValue = 0.0f;

		CreateLines();

		CreateParticles();
	}

	TestParticles3D::~TestParticles3D()
	{
	
	}
		
	void TestParticles3D::CreateLines()
	{
		glGenVertexArrays(1, &m_IndexVA_Lines);
		glBindVertexArray(m_IndexVA_Lines);

		m_LineVertexCount = 14 * 7;

		m_LineVertices = new GLfloat[]
		{
		  -1.0f, -1.0f,  -1.0f, 0.18f, 0.6f, 0.96f, 1.0f,
		   1.0f, -1.0f,  -1.0f, 0.18f, 0.6f, 0.96f, 1.0f,
		   1.0f,  1.0f,  -1.0f, 0.18f, 0.6f, 0.96f, 1.0f,
		  -1.0f,  1.0f,  -1.0f, 0.18f, 0.6f, 0.96f, 1.0f,

		  -1.0f, -1.0f,   1.0f, 0.18f, 0.6f, 0.96f, 1.0f,
		   1.0f, -1.0f,   1.0f, 0.18f, 0.6f, 0.96f, 1.0f,
		   1.0f,  1.0f,   1.0f, 0.18f, 0.6f, 0.96f, 1.0f,
		  -1.0f,  1.0f,   1.0f, 0.18f, 0.6f, 0.96f, 1.0f,

		   0.0f,  0.0f,   0.0f,  0.0f, 0.0f, 1.0f, 1.0f,
		   3.0f,  0.0f,   0.0f,  0.0f, 0.0f, 1.0f, 1.0f,

		   0.0f,  0.0f,   0.0f,  0.0f, 1.0f, 0.0f, 1.0f,
		   0.0f,  3.0f,   0.0f,  0.0f, 1.0f, 0.0f, 1.0f,
		   
		   0.0f,  0.0f,   0.0f,  1.0f, 0.0f, 0.0f, 1.0f,
		   0.0f,  0.0f,   3.0f,  1.0f, 0.0f, 0.0f, 1.0f
		};

		glGenBuffers(1, &m_IndexVB_Lines);
		glBindBuffer(GL_ARRAY_BUFFER, m_IndexVB_Lines);
		glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * m_LineVertexCount, m_LineVertices, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*)0);

		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 7 * sizeof(GLfloat), (void*) 12);

		m_LineIndexCount = 24 + 6;

		m_LineIndices = new unsigned int[m_LineIndexCount]
		{
				0, 1,
				1, 2,
				2, 3,
				3, 0,

				4, 5,
				5, 6,
				6, 7,
				7, 4,

				0, 4,
				1, 5,
				2, 6,
				3, 7,

				8, 9,
				10, 11,
				12, 13
		};

		glGenBuffers(1, &m_IndexIB_Lines);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndexIB_Lines);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_LineIndexCount, m_LineIndices, GL_STATIC_DRAW);

	}

	void TestParticles3D::CreateParticles() 
	{
		glGenVertexArrays(1, &m_IndexVA_Particles);
		glBindVertexArray(m_IndexVA_Particles);
 
		GenPositions2();
		GenIndicies();

		glGenBuffers(1, &m_IndexVB_Particles);
		glBindBuffer(GL_ARRAY_BUFFER, m_IndexVB_Particles);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles * 4, m_ParticleVertices, GL_DYNAMIC_DRAW);

		glEnableVertexAttribArray(0); //Attribute 0 is position
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position));

		glEnableVertexAttribArray(1); //Attribute 1 is color
		glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color));

		glGenBuffers(1, &m_IndexIB_Particles);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndexIB_Particles);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nParticles * 6, m_ParticleIndices, GL_STATIC_DRAW);
	}

	void TestParticles3D::GenPositions2() 
	{
		float scale = 1.0f;

		int uStepCount = 300;
		float uMin = -3.14;
		float uMax = 3.14;
		float uStep = (uMax - uMin) / uStepCount;
		float u = uMin;

		int vStepCount = 200;
		float vMin = -0.6;
		float vMax = 0.6;
		float vStep = (vMax - vMin) / vStepCount;
		float v = vMin;

		int cStepCount = 10;
		float cMin = 0.4;
		float cMax = 0.7;
		float cStep = (cMax - cMin) / cStepCount;
		float c = cMin;

		float vScale = scale * 1;
		float pScale = 50;
		int counter = 0;
		int iv = 0;

		Vec3 xVec = { m_pSize * m_Camera.m_Right.x , m_pSize * m_Camera.m_Right.y, m_pSize * m_Camera.m_Right.z };
		Vec3 yVec = { m_pSize * m_Camera.m_Up.x , m_pSize * m_Camera.m_Up.y, m_pSize * m_Camera.m_Up.z };

		float maxDist = 1.0f;
		int particleCounter = 0;

		for (int i = 0; i < uStepCount; i++)
		{
			v = vMin;

			for (int j = 0; j < vStepCount; j++)
			{
				c = cMin;

				for (int k = 0; k < cStepCount; k++)
				{
					//u += uStep * ((float)rand() / (RAND_MAX) - 0.5);
					//v += vStep * ((float)rand() / (RAND_MAX) - 0.5);
					//c += cStep * ((float)rand() / (RAND_MAX) - 0.5);

					float x, y, z;
					x = c * std::cosh(v / c) * std::cos(u);
					y = c * std::cosh(v / c) * std::sin(u);
					z = v;

					float dist = std::sqrt(x * x + y * y + z * z);

					float perc = 100 * (dist / maxDist);

					m_ParticlePoints[i].Position.x = x;
					m_ParticlePoints[i].Position.y = y;
					m_ParticlePoints[i].Position.z = z;

					auto quad = CreatePerpQuad(x, y, z, xVec, yVec);

					for (int j = 0; j < 4; j++)
					{
						quad[j].Color = GetBlendedColor(perc);
						m_ParticleVertices[counter] = quad[j];
						counter++;
					}

					c += cStep;
				}
				v += vStep;
			}
			u += uStep;
		}
	}


	void TestParticles3D::GenPositions()
	{
		int counter = 0;
		float width = 1.8;
		float height = 1.3;

		float maxDist = height;
		float minDist = 0;

		Vec3 xVec = { m_pSize * m_Camera.m_Right.x , m_pSize * m_Camera.m_Right.y, m_pSize * m_Camera.m_Right.z};
		Vec3 yVec = { m_pSize * m_Camera.m_Up.x , m_pSize * m_Camera.m_Up.y, m_pSize * m_Camera.m_Up.z };


		for (int i = 0; i < m_nParticles; i++)
		{
			float r =				height *	(float)rand() / (RAND_MAX);
			float theta =			3.14f  *	(float)rand() / (RAND_MAX);
			float phi =		(2.0f * 3.14f) *	(float)rand() / (RAND_MAX);

			float x = r * std::sin(theta) * std::cos(phi);
			float y = r * std::sin(theta) * std::sin(phi);
			float z = r * std::cos(theta);

			float perc = 100 * (r / maxDist);

			m_ParticlePoints[i].Position.x = x;
			m_ParticlePoints[i].Position.y = y;
			m_ParticlePoints[i].Position.z = z;

			auto quad = CreatePerpQuad(x, y, z, xVec, yVec);

			for (int j = 0; j < 4; j++)
			{
				quad[j].Color = GetBlendedColor(perc);
				m_ParticleVertices[counter] = quad[j];
				counter++;
			}
		}
	}


	void TestParticles3D::UpdateQuad() 
	{
		int counter = 0;
		Vec3 xVec = { m_pSize * m_Camera.m_Right.x , m_pSize * m_Camera.m_Right.y, m_pSize * m_Camera.m_Right.z };
		Vec3 yVec = { m_pSize * m_Camera.m_Up.x , m_pSize * m_Camera.m_Up.y, m_pSize * m_Camera.m_Up.z };

		for (int i = 0; i < m_nParticles; i++)
		{

			auto quad = CreatePerpQuad(m_ParticlePoints[i].Position.x, m_ParticlePoints[i].Position.y, m_ParticlePoints[i].Position.z, xVec, yVec);

			for (int j = 0; j < 4; j++)
			{
				m_ParticleVertices[counter].Position.x = quad[j].Position.x;
				m_ParticleVertices[counter].Position.y = quad[j].Position.y;
				m_ParticleVertices[counter].Position.z = quad[j].Position.z;
				counter++;
			}
		}
	}


	void TestParticles3D::GenIndicies()
	{
		int quadCounter = 0;
		int index = 0;

		for (int i = 0; i < m_nParticles; i++)
		{
			m_ParticleIndices[i * 6] = index;
			m_ParticleIndices[i * 6 + 1] = index + 1;
			m_ParticleIndices[i * 6 + 2] = index + 2;
			m_ParticleIndices[i * 6 + 3] = index + 2;
			m_ParticleIndices[i * 6 + 4] = index + 3;
			m_ParticleIndices[i * 6 + 5] = index;

			index += 4;
		}
	}

	void TestParticles3D::OnUpdateCameraKey(keyAcion ka)
	{
		m_Camera.ProcessKeyInput(ka);
	}

	void TestParticles3D::OnUpdateCameraMouseRotate(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputRotate(xPos, yPos);
	}

	void TestParticles3D::OnUpdateCameraMousePan(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputRotate(xPos, yPos);
	}

	void TestParticles3D::OnUpdateCameraScroll(float fov)
	{
		m_Camera.ProcessScrollInput(fov);
	}

	void TestParticles3D::OnUpdateCameraReset()
	{
		m_Camera.m_FirstMouseRotate = true;
	}

	void TestParticles3D::OnUpdateCameraResetPan()
	{
		m_Camera.m_FirstMousePan = true;
	}

	void TestParticles3D::OnUpdate()
	{
		//cout << m_Camera.m_Right.x << ", " << m_Camera.m_Right.y << ", " << m_Camera.m_Right.z << endl;

		GLCall(glGenBuffers(1, &m_IndexVB_Particles));

		//UpdateQuad();

		//GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles * 4, m_ParticleVertices));


		GLCall(glClear(GL_COLOR_BUFFER_BIT));

		m_Shader->Bind();
		
		m_Proj = glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f/9.0f, 0.01f, 100.0f);
		m_View = glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up);
		m_Model = glm::translate(glm::mat4(1.0f), m_TranslationA);

		m_Shader->SetUniformMat4f("model", m_Model);
		m_Shader->SetUniformMat4f("view", m_View);
		m_Shader->SetUniformMat4f("projection", m_Proj);
	}

	void TestParticles3D::OnRender()
	{
		//GLCall(glBindVertexArray(m_IndexVA_Lines));
		//GLCall(glDrawElements(GL_LINES, m_LineIndexCount, GL_UNSIGNED_INT, nullptr));

		GLCall(glBindVertexArray(m_IndexVA_Particles));
		GLCall(glDrawElements(GL_TRIANGLES, m_nParticles * 6, GL_UNSIGNED_INT, nullptr));
	}

	void TestParticles3D::OnImGuiRenderer()
	{
		//Passing the memory adress of x and y z will then follow automatically.    
		ImGui::SliderFloat3("Translation A", &m_TranslationA.x, -10.0f, 10.0f);  
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}
}

