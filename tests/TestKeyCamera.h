#pragma once

#include "Test.h"
#include <glew.h>
#include "../Shader.h"
#include "../Camera.h"

#include <memory>

using namespace std;

namespace testNS
{

	class TestKeyCamera : public Test
	{
	public:
		TestKeyCamera();
		~TestKeyCamera();

		void OnUpdate() override;
		void OnRender() override;
		void OnImGuiRenderer() override;
		void OnUpdateCameraKey(keyAcion ka) override;
		void OnUpdateCameraMouseRotate(float xPos, float yPos) override;
		void OnUpdateCameraMousePan(float xPos, float yPos) override;
		void OnUpdateCameraScroll(float fov) override;
		void OnUpdateCameraReset() override;

	private:
		unique_ptr<Shader> m_Shader;

		glm::mat4 m_Proj, m_View, m_Model;

		glm::vec3 m_TranslationA, m_TranslationB;

		unsigned int m_IndexVA;
		unsigned int m_IndexVB;
		unsigned int m_IndexIB;

		int m_nQuads;

		int m_IndexCount;
		int m_LineCount;

		unsigned int* m_Indices;
		Vertex* m_CornerPos;

		float m_ColorScaleValue;
		float m_ColorIncrement;
		float m_ColorStep;

		float m_Time;

		Camera m_Camera;

		float m_Fov;

	};

};