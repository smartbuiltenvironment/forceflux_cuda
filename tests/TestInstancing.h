#pragma once

#include "Test.h"
#include <glew.h>
#include "../Shader.h"
#include "../Camera.h"

#include <memory>

using namespace std;

namespace testNS
{

	class TestInstancing : public Test
	{
	public:
		TestInstancing();
		~TestInstancing();

		void OnUpdate() override;
		void OnRender() override;
		void OnImGuiRenderer() override;
		void OnUpdateCameraKey(keyAcion ka) override;
		void OnUpdateCameraMouseRotate(float xPos, float yPos) override;
		void OnUpdateCameraMousePan(float xPos, float yPos) override;
		void OnUpdateCameraScroll(float fov) override;
		void OnUpdateCameraReset() override;
		void OnUpdateCameraResetPan() override;


		void UpdateQuad();

		void CreateLines();
		void CreateParticles();

		void GenPositions();
		void GenPositions2();
		void GenIndicies();

		float AngleBetween(glm::vec3 a, glm::vec3 b);


		void QuadFromVector(GLfloat* g, glm::vec3 camDir);

	private:
		unique_ptr<Shader> m_Shader;

		glm::mat4 m_Proj, m_View, m_Model, m_Trans, m_Rot;
		glm::vec3 m_TranslationA, m_TranslationB;

		unsigned int m_IndexVA_Lines;
		unsigned int m_IndexVB_Lines;
		unsigned int m_IndexIB_Lines;
		unsigned int m_IndexVA_Particles;
		unsigned int m_IndexVB_Particles;
		unsigned int m_IndexIB_Particles;

		int m_nParticles;
		const int m_MaxParticles;


		int m_LineIndexCount;
		int m_LineVertexCount;
		int m_LineCount;

		unsigned int* m_LineIndices;
		Vertex* m_ParticleVertices;
		Point* m_ParticlePoints;
		Color* m_ParticleColors;


		unsigned int* m_ParticleIndices;

		GLfloat* m_LineVertices;

		float m_ColorScaleValue;
		float m_ColorIncrement;
		float m_ColorStep;

		float m_Time;

		Camera m_Camera;

		float m_Fov;

		float m_pSize;

		unsigned int m_particles_position_buffer;
		unsigned int m_particles_color_buffer;
		unsigned int m_billboard_vertex_buffer;


	};

};