#pragma once

#include "Test.h"
#include <glew.h>
#include "../Shader.h"


#include <memory>

using namespace std;

namespace testNS
{

	class TestBatch : public Test
	{
	public:
		TestBatch();
		~TestBatch();

		void OnUpdate() override;
		void OnRender() override;
		void OnImGuiRenderer() override;
		void GenPositions();
		void GenIndicies();
		void UpdatePositions();
		void UpdateColor();

	private:
		unique_ptr<Shader> m_Shader;				//Smart pointers, deleted automatically etc..
		
		glm::mat4 m_Proj, m_View;

		glm::vec3 m_TranslationA, m_TranslationB;

		unsigned int m_IndexVA;
		unsigned int m_IndexVB;
		unsigned int m_IndexIB;

		int m_nQuads;

		unsigned int* m_Indices;
		Vertex* m_CornerPos;

		float m_ColorScaleValue;
		float m_ColorIncrement;
		float m_ColorStep;

	};

};