#pragma once

#include "Test.h"
#include <glew.h>
#include "../Camera.h"
#include "../Shader.h"

#include <memory>

using namespace std;

namespace testNS
{

	class TestParticles3D : public Test
	{
	public:
		TestParticles3D();
		~TestParticles3D();

		void OnUpdate() override;
		void OnRender() override;
		void OnImGuiRenderer() override;
		void OnUpdateCameraKey(keyAcion ka) override;
		void OnUpdateCameraMouseRotate(float xPos, float yPos) override;
		void OnUpdateCameraMousePan(float xPos, float yPos) override;
		void OnUpdateCameraScroll(float fov) override;
		void OnUpdateCameraReset() override;
		void OnUpdateCameraResetPan() override;

		void UpdateQuad();

		void CreateLines();
		void CreateParticles();

		void GenPositions();
		void GenPositions2();
		void GenIndicies();

	private:
		unique_ptr<Shader> m_Shader;

		glm::mat4 m_Proj, m_View, m_Model;
		glm::vec3 m_TranslationA, m_TranslationB;

		unsigned int m_IndexVA_Lines;
		unsigned int m_IndexVB_Lines;
		unsigned int m_IndexIB_Lines;
		unsigned int m_IndexVA_Particles;
		unsigned int m_IndexVB_Particles;
		unsigned int m_IndexIB_Particles;

		int m_nParticles;

		int m_LineIndexCount;
		int m_LineVertexCount;
		int m_LineCount;

		unsigned int* m_LineIndices;
		Vertex* m_ParticleVertices;
		Point* m_ParticlePoints;

		unsigned int* m_ParticleIndices;

		GLfloat* m_LineVertices;

		float m_ColorScaleValue;
		float m_ColorIncrement;
		float m_ColorStep;

		float m_Time;

		Camera m_Camera;

		float m_Fov;

		float m_pSize;

	};

};