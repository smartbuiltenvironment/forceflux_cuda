#include "TestLight.h" 
#include "imgui/imgui.h"
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include <array>


namespace testNS 
{
	TestLight::TestLight()
		:   m_Proj(glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f)),
			m_View(glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up)),
			m_Model(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),			//glm::mat4 m4(1.0f) => constructs an identity matrix
			m_TranslationA(0, 0, 0),
			m_TranslationB(0, 0, 0),
			m_nQuads(100),
			m_Time(0.0f),
			m_Fov(45.0f)
	{

		m_Camera = Camera();
		m_Shader = make_unique<Shader>("resources/shaders/BasicBatchLight.shader");

		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));

		Vec4 color1 = { 0.18f, 0.6f,  0.96f, 1.0f };
		Vec4 color2 = { 0.48f, 0.32f, 0.16f, 1.0f };

		vector<VertexN> icosaVertices1 = CreateIcoSphere(0, 0, 0, 0.5, color1);
		vector<VertexN> icosaVertices2 = CreateIcoSphere(0.7, 0.7, 0, 0.4, color2);

		vCounter = 0;

		m_icosaVertices = new VertexN[2 * 12];
		for (int i = 0; i < icosaVertices1.size(); i++)
		{
			m_icosaVertices[vCounter] = icosaVertices1[i];
			vCounter++;
		}
		
		for (int i = 0; i < icosaVertices2.size(); i++)
		{
			m_icosaVertices[vCounter] = icosaVertices2[i];
			vCounter++;
		}

		vector<int> icosaIndices1 = GetIcoSphereIndices(0);
		vector<int> icosaIndices2 = GetIcoSphereIndices(12);
		iCounter = 0;
		m_icosaIndices = new unsigned int[2 * 60];
		
		for (int i = 0; i < icosaIndices1.size(); i++) 
		{
			m_icosaIndices[iCounter] = icosaIndices1[i];
			iCounter++;
		}
		
		for (int i = 0; i < icosaIndices2.size(); i++) 
		{
			m_icosaIndices[iCounter] = icosaIndices2[i];
			iCounter++;
		}
		

		GLfloat vertices[] = {
		  -1.0f, -1.0f,  -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		   1.0f, -1.0f,  -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		   1.0f,  1.0f,  -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		  -1.0f,  1.0f,  -1.0f, 1.0f, 1.0f, 1.0f, 1.0f,

		  -1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		   1.0f, -1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		   1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		  -1.0f,  1.0f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f,

		   0.0f,  0.0f,  0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		   3.0f,  0.0f,  0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		   0.0f,  3.0f,  0.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		   0.0f,  0.0f,  3.0f, 1.0f, 1.0f, 1.0f, 1.0f,
		 };

		glGenVertexArrays(1, &m_IndexVA);
		glBindVertexArray(m_IndexVA);

		glGenBuffers(1, &m_IndexVB);
		glBindBuffer(GL_ARRAY_BUFFER, m_IndexVB);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position));

		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color));

		m_IndexCount = 24+6;

		m_Indices = new unsigned int[m_IndexCount] 
		{ 
			0,1,
			1,2,
			2,3,
			3,0,

			4,5,
			5,6,
			6,7,
			7,4,
			
			0, 4,
			1, 5,
			2, 6,
			3, 7,

			8,9,
			8,10,
			8,11
		};

		glGenBuffers(1, &m_IndexIB);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndexIB);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_IndexCount, m_Indices, GL_STATIC_DRAW);


		//Create light cube
		lightPosition = glm::vec3(1.5, -1.5, 1.5);

		Vec4 color3 = { 1.0f, 1.0f, 1.0f, 1.0f };
		vector<VertexN> vCube = CreateCube(lightPosition.x, lightPosition.y, lightPosition.z, 0.1, color3);
		m_cubeVertices = new VertexN[8];

		for (int i = 0; i < vCube.size(); i++)
			m_cubeVertices[i] = vCube[i];

		vector<int> iCube = GetCubeIndices(0);
		m_cubeIndices = new unsigned int[12 * 3];

		for (int i = 0; i < iCube.size(); i++)
			m_cubeIndices[i] = iCube[i];


		glGenVertexArrays(1, &m_lightVA);
		glBindVertexArray(m_lightVA);
		
		GLCall(glGenBuffers(1, &m_lightVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_lightVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(VertexN) * 8, nullptr, GL_DYNAMIC_DRAW));

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexN), (const void*)offsetof(VertexN, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is normal
		GLCall(glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexN), (const void*)offsetof(VertexN, Normal)));

		GLCall(glEnableVertexAttribArray(2)); //Attribute 2 is color
		GLCall(glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(VertexN), (const void*)offsetof(VertexN, Color)));

		GLCall(glGenBuffers(1, &m_lightIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_lightIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 12 * 3, m_cubeIndices, GL_STATIC_DRAW));



		//Icosahedron triangles
		GLCall(glGenVertexArrays(1, &m_icosaIndexVA));
		GLCall(glBindVertexArray(m_icosaIndexVA));

		GLCall(glGenBuffers(1, &m_icosaIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_icosaIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(VertexN) * 2 * 12, nullptr, GL_DYNAMIC_DRAW));

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(VertexN), (const void*)offsetof(VertexN, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is normal
		GLCall(glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(VertexN), (const void*)offsetof(VertexN, Normal)));

		GLCall(glEnableVertexAttribArray(2)); //Attribute 2 is color
		GLCall(glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(VertexN), (const void*)offsetof(VertexN, Color)));

		GLCall(glGenBuffers(1, &m_icosaIndexIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_icosaIndexIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * 2 * 20 * 3, m_icosaIndices, GL_STATIC_DRAW));


		glEnable(GL_DEPTH_TEST);
	}

	TestLight::~TestLight()
	{
	
	}

	void TestLight::OnUpdateCameraKey(keyAcion ka)
	{
		m_Camera.ProcessKeyInput(ka);
	}

	void TestLight::OnUpdateCameraMouseRotate(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputRotate(xPos, yPos);
	}

	void TestLight::OnUpdateCameraMousePan(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputPan(xPos, yPos);
	}

	void TestLight::OnUpdateCameraScroll(float fov)
	{
		m_Camera.ProcessScrollInput(fov);
	}

	void TestLight::OnUpdateCameraReset()
	{
		m_Camera.m_FirstMouseRotate = true;
	}

	void TestLight::OnUpdate()
	{
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_icosaIndexVB));
		GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(VertexN) * 2 * 12, m_icosaVertices));

		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_lightVB));
		GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(VertexN) * 8, m_cubeVertices));

		GLCall(glClear(GL_COLOR_BUFFER_BIT));
		GLCall(glClear(GL_DEPTH_BUFFER_BIT));

		m_Shader->Bind();
		
		m_Proj = glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f/9.0f, 0.01f, 100.0f);
		m_View = glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up);
		m_Model = glm::translate(glm::mat4(1.0f), m_TranslationA);

		m_Shader->SetUniformMat4f("u_model", m_Model);
		m_Shader->SetUniformMat4f("u_view", m_View);
		m_Shader->SetUniformMat4f("u_proj", m_Proj);

		m_Shader->SetUniform3f("objectColor", 1.0f, 0.5f, 0.31f);
		m_Shader->SetUniform3f("lightColor", 1.0f, 1.0f, 1.0f);
		m_Shader->SetUniform3f("lightPosition", lightPosition.x, lightPosition.y, lightPosition.z);
		m_Shader->SetUniform3f("viewPosition", m_Camera.m_Position.x, m_Camera.m_Position.y, m_Camera.m_Position.z);


	}

	void TestLight::OnRender()
	{
		//Renderer renderer;
		GLCall(glBindVertexArray(m_IndexVA));
		GLCall(glDrawElements(GL_LINES, m_IndexCount, GL_UNSIGNED_INT, nullptr));

		GLCall(glBindVertexArray(m_icosaIndexVA));
		GLCall(glDrawElements(GL_TRIANGLES, 2 * 20 * 3, GL_UNSIGNED_INT, nullptr));

		GLCall(glBindVertexArray(m_lightVA));
		GLCall(glDrawElements(GL_TRIANGLES, 12 * 3, GL_UNSIGNED_INT, nullptr));
	}

	void TestLight::OnImGuiRenderer()
	{
		//Passing the memory adress of x and y z will then follow automatically.    
		ImGui::SliderFloat3("Translation A", &m_TranslationA.x, -10.0f, 10.0f);  
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}

	vector<VertexN> TestLight::CreateCube(float x, float y, float z, float size, Vec4 color)
	{
		//Predefined coordinates for the 12 vertices of an icosahedron with 1 in radius
		float xIco[8] = {-0.5, -0.5,  0.5,  0.5, -0.5, -0.5, 0.5,  0.5};
		float yIco[8] = {-0.5,  0.5,  0.5, -0.5, -0.5,  0.5, -0.5, 0.5};
		float zIco[8] = {-0.5, -0.5, -0.5, -0.5,  0.5,  0.5, 0.5,  0.5};

		vector<VertexN> vs;
		float a, aStep, cX, cY, cZ;

		for (int i = 0; i < 8; i++)
		{
			cX = x + size * xIco[i];
			cY = y + size * yIco[i];
			cZ = z + size * zIco[i];

			VertexN vNext;
			vNext.Position = { cX, cY, cZ };
			vNext.Normal = { xIco[i], yIco[i], zIco[i] };
			vNext.Color = color;
			vs.push_back(vNext);
		}
		return vs;
	}

	vector<int> TestLight::GetCubeIndices(int startIndex)
	{
		//Indices for 12 triangles for the predifined icosahedron
		vector<int> indices =
		{	0,1,2,
			1,0,4,
			0,3,6,
			3,2,7,
			2,1,5,
			4,6,7,
			0,2,3,
			1,4,5,
			0,6,4,
			3,7,6,
			2,5,7,
			4,7,5,
		};

		for (int i = 0; i < indices.size(); i++)
			indices[i] += startIndex;

		return indices;
	}

	vector<VertexN> TestLight::CreateIcoSphere(float x, float y, float z, float radius, Vec4 color)
	{
		//Predefined coordinates for the 12 vertices of an icosahedron with 1 in radius
		float xIco[12] = { 0, 0.894427, 0.276393, -0.723607, -0.723607, 0.276393, 0.723607, -0.276393, -0.894427, -0.276393, 0.723607, 0.0 };
		float yIco[12] = { 0, 0, 0.850651, 0.525731, -0.525731, -0.850651, 0.525731, 0.850651, 0.0, -0.850651, -0.525731, 0.0 };
		float zIco[12] = { 1, 0.447214, 0.447214, 0.447214, 0.447214, 0.447214, -0.447214, -0.447214, -0.447214, -0.447214, -0.447214, -1.0 };

		vector<VertexN> vs;
		float a, aStep, cX, cY, cZ;
		float nX, ny, nz;

		for (int i = 0; i < 12; i++)
		{
			cX = x + radius * xIco[i];
			cY = y + radius * yIco[i];
			cZ = z + radius * zIco[i];

			VertexN vNext;
			vNext.Position = { cX, cY, cZ };
			vNext.Normal = { xIco[i], yIco[i], zIco[i] };
			vNext.Color = color;
			vs.push_back(vNext);
		}
		return vs;
	}

	vector<int> TestLight::GetIcoSphereIndices(int startIndex)
	{
		//Indices for 20 triangles for the predifined icosahedron
		vector<int> indices =
		{ 0,1,2,
			0,2,3,
			0,3,4,
			0,4,5,
			0,5,1,
			1,5,10,
			1,6,2,
			1,10,6,
			2,6,7,
			2,7,3,
			3,7,8,
			3,8,4,
			4,8,9,
			4,9,5,
			5,9,10,
			6,10,11,
			6,11,7,
			7,11,8,
			8,11,9,
			9,11,10
		};

		for (int i = 0; i < indices.size(); i++)
			indices[i] += startIndex;

		return indices;
	}

}

