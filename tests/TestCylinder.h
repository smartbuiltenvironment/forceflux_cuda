#pragma once

#include "Test.h"
#include <glew.h>
#include <vector>
#include "../Shader.h"
#include "../Camera.h"
#include "../FFCuda.h"
#include <math.h>
#include "../Structure.h"

#include <memory>

using namespace std;

namespace testNS
{
	class TestCylinder : public Test
	{
	public:
		TestCylinder();
		~TestCylinder();

		void OnUpdate() override;
		void OnRender() override;
		void OnImGuiRenderer() override;
		
		void InitialiseArrays();
		void InitialiseOpenGL();
		void UpdatePositions();
		void UpdateDiscSize();
		void UpdateIcosaSize();
		void UpdateDiscPosition();
		void UpdateIcosaPosition();
		void UpdateCylinderPosition();



		void OnUpdateCameraKey(keyAcion ka) override;
		void OnUpdateCameraMouseRotate(float xPos, float yPos) override;
		void OnUpdateCameraMousePan(float xPos, float yPos) override;
		void OnUpdateCameraScroll(float fov) override;
		void OnUpdateCameraReset() override;
		void OnUpdateCameraResetPan() override;

		vector<Vertex> CreateDisc(float x, float y, float z, float radius, int n, Vec4 color);
		vector<Vertex> CreateCylinder(Vec3 position, float radius, int n, float length);
		vector<int> GetDiscIndices(int startIndex, int n);
		vector<Vertex> CreateIcoSphere(float x, float y, float z, float radius, Vec4 color);
		vector<int> GetIcoSphereIndices(int startIndex);
		vector<int> GetCylinderIndices(int startIndex, int n);

		void Setup(int zoneCountWidth1, int zoneCountWidth2, int zoneCountHeight);
		void CreateParticlesRandom();
		void CreateParticlesRegular();
		void CreateParticlesRegularRectangle();
		void CreateParticlesRegularDirectTension();
		void CreateParticlesRegularSteelTest();
		void CreateParticlesRegularSlice();
		void CreateParticlesRegularGradient();
		void CreateLoadCylinders();
		void GenDiscs();
		void GenIcosas();
		void GenCylinders();

		void UpdateBcCylinder();
		void UpdateBcRectangle();
		bool InsideDirectTension(float d, float z, float radius);
		bool InsideSteelTest(float d, float z, float radius);
		bool InsideSteelTest2(float d, float z);

		void CopyPositions();

		void EstimateBaseSize();
		void ScaleBaseSizeEstimate();
		void CalcBaseSize(int nIterations, int nNeighbours);
		void CalcMassSize();
		void CalcHaSize(float sf);
		void IncrementSizeGPU();
		void IncrementParticleSize();
		void IncrementParticleSize1D(int requiredFriendsCount);

		void ShakeParticles(int rndSeed);
		void ShakeParticlesIteration(int n);
		void RunShaking();
		void RunXray();
		void RunDefAni();

		void CreateArms();
		void CreateZones();
		void CreateZone(float x, float y, float z);
		void CreateZoneTopology(float xMp, float yMp, float zMp);

		void ComputeStats();
		void GenFlatTopology1();
		void GenFlatTopology2();

		bool DoesBondExist(int vIndex, int vNextIndex);
		int FindNextParticleIndex(int vIndex, int vNextIndex);
		int FindExistingArmFlatIndex(int vIndex, int vNextIndex);
		float GenRandom(float min, float max);
		
		//Simulation functions
		void LaplacianCPU();
		void LaplacianGPU();
		void RunGPU();
		void RunAnalysis();
		void RunOutput();

		void RunExport();

		void ExportModel();

		void ColorParticles();
		void ColorArms();
		

	private:

		unique_ptr<Shader> m_Shader;				//Smart pointers, deleted automatically etc..

		glm::mat4 m_Proj, m_View, m_Model, m_Trans, m_Rot;
		glm::vec3 m_TranslationA, m_TranslationB;

		unsigned int m_pIndexVA;
		unsigned int m_pIndexVB;
		unsigned int m_pIndexIB;

		unsigned int m_zLineIndexVA;
		unsigned int m_zLineIndexVB;
		unsigned int m_zLineIndexIB;

		unsigned int m_bLineIndexVA;
		unsigned int m_bLineIndexVB;
		unsigned int m_bLineIndexIB;

		unsigned int m_discIndexVA;
		unsigned int m_discIndexVB;
		unsigned int m_discIndexIB;

		unsigned int m_icosaIndexVA;
		unsigned int m_icosaIndexVB;
		unsigned int m_icosaIndexIB;

		unsigned int m_bLineIndexVA2;
		unsigned int m_bLineIndexVB2;
		unsigned int m_bLineIndexIB2;

		unsigned int m_cylinderIndexVA;
		unsigned int m_cylinderIndexVB;
		unsigned int m_cylinderIndexIB;

		int m_nParticles;
		int m_nDiscSides;
		int m_nCylinderTriangles;
		int m_shakeCounter;

		int m_armIndexCounter;
		int m_armCounter;
		int m_armVertexCounter;

		int m_zLineIndexCounter;
		int m_zLineCounter;
		int m_zVertexCounter;
		
		unsigned int* m_Indices;
		unsigned int* m_arms;
		unsigned int* m_arms2;
		unsigned int* m_zoneLineIndices;
		unsigned int* m_discIndices;
		unsigned int* m_icosaIndices;
		unsigned int* m_cylinderIndices;
		

		float* m_pBaseSize;			//Radius for a based size related to number of neighbours
		float* m_pMassSize;			//Radius for the material size (true volume)
		float* m_pHaSize;			//Radius for the horizon, ie particle the kernel-reach	
		float* m_pMass;
		bool* m_locked;
		bool* m_loaded;

		float m_baseSizeEstimate;
		float m_massSizeEstimate;

		Vertex* m_vertices;			//Vertices for the n-particles	
		Vertex* m_vertices2;		//Vertices for arms with unique colors
		Vertex* m_zoneVertices1D;	//Flat list of zone corners
		Vertex* m_discVertices;		
		Vertex* m_icosaVertices;
		Vertex* m_icosaCenter;
		Vertex* m_cylinderVertices;
		Vertex* m_cylinderVerticesInit;

		vector<int> m_arms3V;

		vector<vector<Vertex>> m_zoneVertices2D;


		int* m_pzTopFlat;					//Particel zone topology

		vector<vector<int>> m_zpTop;		//Zone Particle topology
		vector<int> m_zpTopFlatV;
		int* m_zpTopFlat;
		int* m_zpStartIndex;
		int* m_zpRowLength;
		int m_zpCounter;

		vector<vector<int>> m_zzTop;		//Zone zone topology
		vector<int> m_zzTopFlatV;
		int* m_zzTopFlat;
		int* m_zzStartIndex;
		int* m_zzRowLength;
		int m_zzCounter;

		vector<vector<int>> m_ppTop;		//Particle particles topology
		int* m_ppTopFlat;
		int* m_ppStartIndex; 
		int* m_ppRowLength;
		int m_ppCounter;
		
		vector<vector<int>> m_paTop;
		int* m_paTopFlat;
		int* m_paStartIndex;
		int* m_paRowLength;
		int m_paCounter;

		LoadingCylinder* m_loadCylinders;

		ArmIndices* m_armi;

		ModelSettings m_ms;
		Camera m_Camera;
		FFCuda m_ffCuda;
		Structure m_structure;
		viewType m_viewType;

		float m_orthoSF;

		int m_nZones;

		//Simulation parameters
		float m_Horizon;
		Vec3* m_loadDir;					//Load on the particles
		float m_loadMag;				//Loading magnitude
		float m_alpha;
		
		bool m_runShaking;
		bool m_runAnalysis;
		bool m_runOutput;
		bool m_runExport;
		
		bool m_btnRunAnalysis;
		bool m_btnRunShaking;
		bool m_btnRunOutput;
		bool m_btnRunExport;

		bool m_btnXrayYZ;
		bool m_XrayYZ;
		int m_XrayDir;

		bool m_btnXrayXY;
		bool m_XrayXY;
		int m_XrayDir2;

		bool m_btnAniDeformation;
		bool m_AniDeformation;
		int m_AniDefDir;


		int m_runCounter;
		colorParticleBy m_colorParticleBy;
		colorArmBy m_colorArmBy;

		//Camer parameters
		float m_Fov;
		float m_pSize;

		//UI parameters
		bool m_drawDisc;
		bool m_drawIcosa;
		bool m_drawZones;
		bool m_drawBonds;
		bool m_drawBonds2;
		bool m_drawBroken;
		bool m_drawParticles;
		bool m_drawCylinders;
		int m_selectedItemArmColorBy;
		int m_selectedItemParticleColorBy;
		float m_scaleFactor;
		bool m_enableClipping;

		int m_brokenArmsCounter;
		float m_tol;

		int* m_pCurrentFriendsCount;
		int m_pRequiredFriendsCount;
		
		bool m_IncrementSizeCPU;
		bool m_IncrementSizeGPU;
		int m_IncrementSizeCounterCPU;
		int m_IncrementSizeCounterGPU;

		float m_clipDistance1;
		float m_clipDistance2;
		float m_clipDistance3;
		float m_clipDistance4;

		bool m_aColorByStrain;
		bool m_aColorByBroken;

		float m_zMin;
		float m_zMax;
		float m_zPart;
		float m_zPartST;
		float m_waistSF; //For direct tension test setup.

		int m_rndSeed;
		
		Stats m_Stats;
		simulationType m_simType;
		
		std::chrono::steady_clock::time_point m_startTimeAnalysis;
		std::chrono::steady_clock::time_point m_nowTimeAnalysis;
		std::chrono::duration<float> m_durationAnalysis;



	};

};