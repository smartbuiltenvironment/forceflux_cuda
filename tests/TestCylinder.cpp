
#include "TestCylinder.h" 
#include "imgui/imgui.h"
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include <array>


namespace testNS
{

	TestCylinder::TestCylinder()
		: m_Proj(glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f)),
		m_View(glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up)),
		m_Model(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),			//glm::mat4 m4(1.0f) => constructs an identity matrix
		m_Trans(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),
		m_Rot(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),
		m_TranslationA(0, 0, 0),
		m_TranslationB(0, 0, 0),
		m_tol(0.000001),
		m_clipDistance1(0.100f),
		m_clipDistance2(0.100f),
		m_clipDistance3(0.250f),
		m_clipDistance4(0.250f),
		m_btnRunAnalysis(false),
		m_btnRunShaking(false),
		m_btnRunOutput(false),
		m_btnRunExport(false),
		m_runCounter(0),
		m_shakeCounter(0),
		m_colorParticleBy(colorParticleBy::damage),
		m_colorArmBy(colorArmBy::elongationToFracture),
		m_scaleFactor(1.0),
		m_nParticles(30000),		//Images made for nP = 30000.
		m_Horizon(0.013f),
		m_runAnalysis(false),
		m_runShaking(false),
		m_runOutput(false),
		m_selectedItemArmColorBy(0),
		m_enableClipping(true),
		m_XrayYZ(false),
		m_XrayDir(1),
		m_XrayDir2(-1),
		m_AniDefDir(1)
	{
		m_Shader = make_unique<Shader>("resources/shaders/FFShader.shader");

		m_drawParticles = true;
		m_drawBonds = false;
		m_drawBonds2 = false;
		m_drawBroken = false;
		m_drawZones = false;
		m_drawDisc = false;
		m_drawIcosa = false;
		m_drawCylinders = true;

		int zoneCountHeight = 12;
		int zoneCountWidth1 = 4;		//Controls the setup of cylinder tests
		int zoneCountWidth2 = 4;

		Vec3 target = { 0.0f, 0.0f, 0.0f };
		//Vec3 target = { -(1.0f / 6.0f) * 0.15f * 3.0f, -0.15f / 4.0f,  0.15f / 2.0f };

		m_Camera = Camera(0.6f, target);
		m_nDiscSides = 20;
		m_nCylinderTriangles = 100;

		m_armIndexCounter = 0;
		m_armCounter = 0;
		m_alpha = 4.5;   //4.5
		m_ffCuda = FFCuda();
		m_ms.rndSeed = 1;

		m_viewType = viewType::orthographic;
		pArrangement pArr = pArrangement::irregular;
		m_simType = simulationType::Cylinder;
		
		auto start = std::chrono::high_resolution_clock::now();


		std::cout << "--------------------------Setting up geometry------------------------" << std::endl;

		Setup(zoneCountWidth1, zoneCountWidth2, zoneCountHeight);

		InitialiseArrays();

		if (pArr == pArrangement::regular || pArr == pArrangement::irregular)
		{
			if (m_simType == simulationType::Cylinder || m_simType == simulationType::SplitCylinder) 
			{
				CreateParticlesRegular(); //CreateParticlesRegularSlice()
			}
			else if (m_simType == simulationType::ModulusOfRupture)
			{
				CreateParticlesRegularRectangle();
			}
			else if (m_simType == simulationType::DirectTension)
			{
				CreateParticlesRegularDirectTension();
			}
			else if (m_simType == simulationType::SteelTest)
			{
				CreateParticlesRegularSteelTest();
			}

			EstimateBaseSize();
		}

		if (m_simType == simulationType::ModulusOfRupture)
			CreateLoadCylinders();

		CreateZones();

		GenFlatTopology1();

		if (pArr == pArrangement::irregular)
		{
			CalcBaseSize(10, 6);

			CalcMassSize();

			ShakeParticlesIteration(100);
		}

		if (m_simType == simulationType::Cylinder || m_simType == simulationType::SplitCylinder || m_simType == simulationType::DirectTension || m_simType == simulationType::SteelTest)
			UpdateBcCylinder();
		else if (m_simType == simulationType::ModulusOfRupture)
			UpdateBcRectangle();

		CalcBaseSize(10, 6);

		CalcMassSize();

		CalcHaSize(m_alpha);

		GenDiscs();

		GenIcosas();

		if(m_simType == simulationType::ModulusOfRupture)
			GenCylinders();

		CreateArms();

		GenFlatTopology2();

		ComputeStats();

		std::cout << "-------------------------- Setting up CUDA ----------------------" << std::endl;


		//m_ffCuda.Initialise(m_nParticles, m_nZones, m_zzCounter, m_ppCounter);

		//Only copied once since the topology never changes
		//m_ffCuda.CopyFromHostToDevice1(m_pzTopFlat, m_pHaSize, m_pCurrentFriendsCount);
		//m_ffCuda.CopyFromHostToDevice2(m_zzTopFlat, m_zzCounter, m_zzStartIndex, m_zzRowLength);
		//m_ffCuda.CopyFromHostToDevice3(m_zpTopFlat, m_zpCounter, m_zpStartIndex, m_zpRowLength);
		//m_ffCuda.CopyFromHostToDevice4(m_ppTopFlat, m_ppCounter, m_ppStartIndex, m_ppRowLength);

		std::cout << "-------------------------- Setting up structure ----------------------" << std::endl;

		m_structure = Structure(
			m_nParticles,
			m_armCounter,
			m_nZones,
			m_alpha,
			m_ppCounter,
			m_paCounter,
			m_vertices,
			m_armi,
			m_pMassSize,
			m_loadDir,
			m_ppTopFlat,
			m_ppStartIndex,
			m_ppRowLength,
			m_paTopFlat,
			m_paStartIndex,
			m_paRowLength,
			m_locked,
			m_loaded,
			m_simType,
			m_ms,
			m_Stats,
			m_loadCylinders);

		std::cout << "-------------------------- Setting up OpenGL ----------------------" << std::endl;

		InitialiseOpenGL();

		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<float> duration = end - start;
		std::cout << "Time elapsed: " << duration.count() << " seconds" << std::endl;
	}

	TestCylinder::~TestCylinder()
	{
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
		GLCall(glBindVertexArray(0));
		m_Shader->Unbind();

		delete[] m_Indices;
		delete[] m_arms;
		delete[] m_arms2;
		delete[] m_zoneLineIndices;
		delete[] m_pzTopFlat;
		delete[] m_vertices;
		delete[] m_vertices2;
		delete[] m_zoneVertices1D;
		delete[] m_discIndices;
		delete[] m_discVertices;
		delete[] m_icosaIndices;
		delete[] m_icosaVertices;
		delete[] m_icosaCenter;

		delete[] m_pBaseSize;
		delete[] m_pMassSize;
		delete[] m_pHaSize;
		delete[] m_pMass;

		delete[] m_loadDir;
		delete[] m_ppTopFlat;

		delete[] m_zzTopFlat;
		delete[] m_zzStartIndex;
		delete[] m_zzRowLength;

		delete[] m_zpTopFlat;
		delete[] m_zpStartIndex;
		delete[] m_zpRowLength;

	}

	void TestCylinder::InitialiseArrays()
	{
		int armsPerParticleEstimate = 150;

		m_Indices = new unsigned int[m_nParticles];
		m_zoneLineIndices = new unsigned int[m_nZones * 24]; //Allocating a little more space than nessecary.
		m_arms = new unsigned int[m_nParticles * armsPerParticleEstimate]; //Allocating more space than nessecary.
		m_arms2 = new unsigned int[m_nParticles * armsPerParticleEstimate]; //Allocating more space than nessecary.
		
		m_armi = new ArmIndices[m_nParticles * armsPerParticleEstimate]; //Allocating more space than nessecary.

		m_icosaIndices = new unsigned int[m_nParticles * 20 * 3];

		m_cylinderIndices = new unsigned int[(m_nCylinderTriangles-1) * 2 * 3 *4];
		m_cylinderVertices = new Vertex[m_nCylinderTriangles * 2 * 4];
		m_cylinderVerticesInit = new Vertex[m_nCylinderTriangles * 2 * 4];

		m_vertices = new Vertex[m_nParticles];
		m_vertices2 = new Vertex[m_nParticles * armsPerParticleEstimate * 2];
		m_icosaCenter = new Vertex[m_nParticles];
		m_pzTopFlat = new int[m_nParticles];
		m_zoneVertices1D = new Vertex[m_nZones * 8];
		m_icosaVertices = new Vertex[m_nParticles * 12];
		m_locked = new bool[m_nParticles];
		m_loaded = new bool[m_nParticles];

		m_pBaseSize = new float[m_nParticles];
		m_pMassSize = new float[m_nParticles];
		m_pHaSize = new float[m_nParticles];
		m_pMass = new float[m_nParticles];

		m_discIndices = new unsigned int[m_nParticles * m_nDiscSides * 3];
		m_discVertices = new Vertex[m_nParticles * (m_nDiscSides + 1)];

		m_loadDir = new Vec3[m_nParticles];

		m_loadCylinders = new LoadingCylinder[4];
	}

	void TestCylinder::InitialiseOpenGL()
	{
		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));

		GLCall(glGenVertexArrays(1, &m_pIndexVA));
		GLCall(glBindVertexArray(m_pIndexVA));

		GLCall(glGenBuffers(1, &m_pIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_pIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles * 10, nullptr, GL_DYNAMIC_DRAW)); //Allocating more then needed 

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		glGenBuffers(1, &m_pIndexIB);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pIndexIB);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nParticles, m_Indices, GL_STATIC_DRAW);


		//Zone lines
		GLCall(glGenVertexArrays(1, &m_zLineIndexVA));
		GLCall(glBindVertexArray(m_zLineIndexVA));

		GLCall(glGenBuffers(1, &m_zLineIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_zLineIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_zVertexCounter, nullptr, GL_STATIC_DRAW));

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		GLCall(glGenBuffers(1, &m_zLineIndexIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_zLineIndexIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_zLineIndexCounter, m_zoneLineIndices, GL_STATIC_DRAW));


		//Bond lines
		GLCall(glGenVertexArrays(1, &m_bLineIndexVA));
		GLCall(glBindVertexArray(m_bLineIndexVA));

		GLCall(glGenBuffers(1, &m_bLineIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles, nullptr, GL_DYNAMIC_DRAW));

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		GLCall(glGenBuffers(1, &m_bLineIndexIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bLineIndexIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_armIndexCounter, m_arms, GL_STATIC_DRAW));


		//Bond lines 2 with unique vertices for unique bond colors
		GLCall(glGenVertexArrays(1, &m_bLineIndexVA2));
		GLCall(glBindVertexArray(m_bLineIndexVA2));

		GLCall(glGenBuffers(1, &m_bLineIndexVB2));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB2));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_armIndexCounter, nullptr, GL_DYNAMIC_DRAW));

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		GLCall(glGenBuffers(1, &m_bLineIndexIB2));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bLineIndexIB2));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_armIndexCounter, m_arms2, GL_STATIC_DRAW));

		//Disc triangles
		GLCall(glGenVertexArrays(1, &m_discIndexVA));
		GLCall(glBindVertexArray(m_discIndexVA));

		GLCall(glGenBuffers(1, &m_discIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_discIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles * (m_nDiscSides + 1), nullptr, GL_DYNAMIC_DRAW));

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		GLCall(glGenBuffers(1, &m_discIndexIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_discIndexIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nParticles * (m_nDiscSides * 3), m_discIndices, GL_STATIC_DRAW));


		//Icosahedron triangles
		GLCall(glGenVertexArrays(1, &m_icosaIndexVA));
		GLCall(glBindVertexArray(m_icosaIndexVA));

		GLCall(glGenBuffers(1, &m_icosaIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_icosaIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles * 12, nullptr, GL_DYNAMIC_DRAW));

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		GLCall(glGenBuffers(1, &m_icosaIndexIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_icosaIndexIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nParticles * 20 * 3, m_icosaIndices, GL_STATIC_DRAW));

		//Draw load and support cylinders for modulus of rupture
		GLCall(glGenVertexArrays(1, &m_cylinderIndexVA));
		GLCall(glBindVertexArray(m_cylinderIndexVA));

		GLCall(glGenBuffers(1, &m_cylinderIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_cylinderIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)* m_nCylinderTriangles * 2 * 4, nullptr, GL_DYNAMIC_DRAW));

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		GLCall(glGenBuffers(1, &m_cylinderIndexIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_cylinderIndexIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * (m_nCylinderTriangles - 1)* 2 * 3 * 4, m_cylinderIndices, GL_STATIC_DRAW)); //114 *4 , m_cylinderIndices, GL_STATIC_DRAW));


		// depth test
		glEnable(GL_DEPTH_TEST);

		glPointSize(4.0); //1.5

	}

	void TestCylinder::Setup(int zoneCountWidth1, int zoneCountWidth2, int zoneCountHeight)
	{
		if (m_simType == simulationType::Cylinder) 
		{
			m_ms.zoneCount.x = zoneCountWidth1;
			m_ms.zoneCount.y = zoneCountWidth1;
			m_ms.zoneCount.z = zoneCountWidth1 * 2;
			m_nParticles = 30000;   //32000; //Resulting in 24450
			m_orthoSF = 0.2;
			m_ms.rndSeed = 3;

		}
		else if(m_simType == simulationType::SplitCylinder)
		{
			m_ms.zoneCount.x = zoneCountWidth1 * 2;
			m_ms.zoneCount.y = zoneCountWidth1;
			m_ms.zoneCount.z = zoneCountWidth1;
			m_nParticles = 32000; //Resulting in 24450
			m_rndSeed = 1;
			m_orthoSF = 0.12;
		}
		else if (m_simType == simulationType::ModulusOfRupture)
		{
			m_ms.zoneCount.x = zoneCountHeight;
			m_ms.zoneCount.z = zoneCountWidth1;
			m_ms.zoneCount.y = zoneCountWidth2;
			m_nParticles = 28000; //Resulting in 26400
			m_rndSeed = 3;
			m_orthoSF = 0.15;

		}
		else if (m_simType == simulationType::DirectTension)
		{
			m_ms.zoneCount.x = zoneCountWidth1;
			m_ms.zoneCount.y = zoneCountWidth1;
			m_ms.zoneCount.z = zoneCountWidth1 * 3;
			m_nParticles = 55000; //Resulting in 24128 
			m_rndSeed = 4;
			m_orthoSF = 0.25;
		}
		else if (m_simType == simulationType::SteelTest)
		{
			m_ms.zoneCount.x = 3; //zoneCountWidth1/2.0f;
			m_ms.zoneCount.y = 3;//zoneCountWidth1/2.0f;
			m_ms.zoneCount.z = 18; //zoneCountHeight;
			m_nParticles = 75000; //Resulting in 24128 
			m_rndSeed = 4;
			m_orthoSF = 0.04;
		}


		m_nZones = m_ms.zoneCount.x * m_ms.zoneCount.y * m_ms.zoneCount.z;

		float pi = 3.14159265359f;
		float diameter = 0.15f;				//Dimensions of standard concrete compressive test piece 
		float radius = diameter / 2.0f;
		float height = 0.3f;
		float area = radius * radius * pi;


		m_ms.bcAngle = 10.0f; //shold be 5.7 deg to meet requirements	//half the bc angle
		float arcLength = Deg2Rad(m_ms.bcAngle) * 2 * radius;
		
		//For the MOR case
		m_ms.edgePadding = 0.02f;

		m_ms.height = height;
		m_ms.radius = radius;
		m_ms.AreaSmall = area;
		m_ms.AreaLarge = height * diameter;

		if (m_simType == simulationType::Cylinder)
		{
			m_ms.bcThickness = 0.015f;
			m_ms.bbSize.x = diameter;
			m_ms.bbSize.y = diameter;
			m_ms.bbSize.z = height;
		}
		else if(m_simType == simulationType::SplitCylinder)
		{
			m_ms.bcThickness = 0.01f;
			m_ms.bbSize.x = height;
			m_ms.bbSize.y = diameter;
			m_ms.bbSize.z = diameter;
		}
		else if (m_simType == simulationType::ModulusOfRupture)
		{
			m_ms.bcThickness = 0.01f;
			float boxHeight = 0.12f;
			m_ms.bbSize.z = boxHeight;
			m_ms.bbSize.x = m_ms.bbSize.z * 3.0f + m_ms.edgePadding * 2.0f; 
			m_ms.bbSize.y = boxHeight;
			m_ms.AreaSmall = boxHeight * boxHeight;
			m_ms.AreaLarge = boxHeight * m_ms.bbSize.x;
		}
		else if (m_simType == simulationType::DirectTension)
		{	
			m_waistSF = 0.5; //Only works for 0.5!
			m_ms.waistSF = m_waistSF;
			
			m_ms.bcThickness = 0.01f;
			m_ms.bbSize.x = diameter;
			m_ms.bbSize.y = diameter;
			m_ms.bbSize.z = diameter * 3 + 0.03;

			m_ms.AreaSmall = (m_waistSF * m_ms.radius) * (m_waistSF * m_ms.radius) * pi;
		}
		else if (m_simType == simulationType::SteelTest)
		{	
			m_waistSF = 0.5; //Only works for 0.5!
			m_ms.waistSF = m_waistSF;
			
			m_ms.height = 0.064;
			m_ms.radius = 0.005;

			m_ms.bcThickness = 0.002f;
			m_ms.bbSize.x = 0.01;
			m_ms.bbSize.y = 0.01;
			m_ms.bbSize.z = 0.064;

			m_ms.AreaSmall = (m_waistSF * m_ms.radius) * (m_waistSF * m_ms.radius) * pi;
		}


		m_ms.zoneStep.x = m_ms.bbSize.x / m_ms.zoneCount.x;
		m_ms.zoneStep.y = m_ms.bbSize.y / m_ms.zoneCount.y;
		m_ms.zoneStep.z = m_ms.bbSize.z / m_ms.zoneCount.z;

		std::cout << "Setup Completed" << std::endl;
	}

	void TestCylinder::CreateParticlesRandom()
	{
		float radius = m_ms.radius;
		float height = m_ms.height;
		float minDist = 0;

		//Move geometry to the positive quadrand by adding this values.
		float addX = m_ms.bbSize.x / 2.0f;
		float addY = m_ms.bbSize.y / 2.0f;
		float addZ = m_ms.bbSize.z / 2.0f;

		for (int i = 0; i < m_nZones; i++)
		{
			vector<int> zpi;
			m_zpTop.push_back(zpi);
		}

		float x, y, z, d, perc;
		int attempts = 0;
		int pCounter = 0;
		m_zpCounter = 0;
		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
		int zonesPerLevel = m_ms.zoneCount.x * m_ms.zoneCount.y;
		int maxZoneIndex = m_ms.zoneCount.x * m_ms.zoneCount.y * m_ms.zoneCount.z - 1;

		while (pCounter < m_nParticles && attempts < m_nParticles * 1000)
		{
			x = GenRandom(-1 * m_ms.bbSize.x / 2.0f, m_ms.bbSize.x / 2.0f);
			y = GenRandom(-1 * m_ms.bbSize.y / 2.0f, m_ms.bbSize.y / 2.0f);
			z = GenRandom(-1 * m_ms.bbSize.z / 2.0f, m_ms.bbSize.z / 2.0f);

			if (m_simType == simulationType::Cylinder)
			{
				d = std::sqrt(x * x + y * y);
				perc = 100 * (d / radius);
			}
			else //Horizontal
			{
				d = std::sqrt(y * y + z * z);
				perc = 100 * (d / radius);
			}

			//Check so the particle is inside the cylinder base
			if (d < radius)
			{
				int zoneIndexX = (int)((x + addX) / m_ms.zoneStep.x);
				int zoneIndexY = (int)((y + addY) / m_ms.zoneStep.y);
				int zoneIndexZ = (int)((z + addZ) / m_ms.zoneStep.z);

				//To avoid round off errors with the zone indexing
				if (zoneIndexX >= m_ms.zoneCount.x) zoneIndexX = m_ms.zoneCount.x - 1;
				if (zoneIndexY >= m_ms.zoneCount.y) zoneIndexY = m_ms.zoneCount.y - 1;
				if (zoneIndexZ >= m_ms.zoneCount.z)	zoneIndexZ = m_ms.zoneCount.z - 1;

				int zoneIndex = zoneIndexX + zoneIndexY * m_ms.zoneCount.x + zoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;

				Vertex v;
				v.Position = { x, y, z };
				Vec4 color;

				if (zoneIndex < zonesPerLevel)
				{
					//Bottom of cylinder
					color = loadZoneColor;
					m_locked[pCounter] = true;
					m_loaded[pCounter] = false;
				}
				else if (zoneIndex > (maxZoneIndex - zonesPerLevel))
				{
					//Top of cylinder
					color = loadZoneColor;
					m_loadDir[pCounter] = { 0, 0, -1 };
					m_locked[pCounter] = false;
					m_loaded[pCounter] = true;
				}
				else
				{
					m_loadDir[pCounter] = { 0, 0, 0 };
					color = GetBlendedColor(perc);
					m_locked[pCounter] = false;
					m_loaded[pCounter] = false;
				}

				v.Color = color;

				m_vertices[pCounter] = v;
				m_pzTopFlat[pCounter] = zoneIndex;

				m_zpTop[zoneIndex].push_back(pCounter);
				m_zpCounter++;

				pCounter++;
			}
			attempts++;
		}

		//Generate particle indices
		for (int i = 0; i < m_nParticles; i++)
			m_Indices[i] = i;

		std::cout << "Create particles completed" << std::endl;
	}

	void TestCylinder::CreateParticlesRegular()
	{
		float radius = m_ms.radius;
		float height = m_ms.height;
		float minDist = 0;

		int nx, ny, nz, cubePCount, pCountPerSide;
		double zsf, xsf;

		if (m_simType == simulationType::Cylinder)
		{
			zsf = m_ms.bbSize.z / m_ms.bbSize.x;
			cubePCount = (int)(m_nParticles / zsf);
			pCountPerSide = (int)std::pow(cubePCount, 1.0 / 3.0);

			nx = pCountPerSide;
			ny = pCountPerSide;
			nz = (int)(pCountPerSide * zsf);
		}
		else //Horizontal
		{
			xsf = m_ms.bbSize.x / m_ms.bbSize.y;
			cubePCount = (int)(m_nParticles / xsf);
			pCountPerSide = (int)std::pow(cubePCount, 1.0 / 3.0);

			nx = (int)(pCountPerSide * xsf);
			ny = pCountPerSide;
			nz = pCountPerSide;
		}

		float dx = m_ms.bbSize.x / nx;
		float dy = m_ms.bbSize.y / ny;
		float dz = m_ms.bbSize.z / nz;

		//Move geometry to the positive quadrand by adding this values.
		float addX = m_ms.bbSize.x / 2.0f;
		float addY = m_ms.bbSize.y / 2.0f;
		float addZ = m_ms.bbSize.z / 2.0f;

		for (int i = 0; i < m_nZones; i++)
		{
			vector<int> zpi;
			m_zpTop.push_back(zpi);
		}

		float x, y, z, d, perc;
		int attempts = 0;
		int pCounter = 0;
		m_zpCounter = 0;

		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
		int zonesPerLevel = m_ms.zoneCount.x * m_ms.zoneCount.y;
		int maxZoneIndex = m_ms.zoneCount.x * m_ms.zoneCount.y * m_ms.zoneCount.z - 1;
		float constraintsZlimit = 0; //m_zoneSideAverage * 0.45;

		float hLimit = (m_ms.bbSize.z / 2.0f) - m_ms.bcThickness;
		float lLimit = (m_ms.bbSize.x / 2.0f) - m_ms.bcThickness;
		bool insideBC, insideL;

		float b = 0.0f;
		float corrFac = 0.0f;

		if (m_simType == simulationType::SplitCylinder)
		{
			b = 2.0f * std::sqrt(2.0f * m_ms.radius - m_ms.bcThickness * m_ms.bcThickness);
			float D = m_ms.radius * 2.0f;
			corrFac = std::pow((1.0f - std::pow((b / D), 2.0f)), (3.0f / 2.0f));
		}


		z = -m_ms.bbSize.z / 2.0f + (dz / 2.0f);
		for (int k = 0; k < nz; k++)
		{
			x = -m_ms.bbSize.x / 2.0f + (dx / 2.0f);
			for (int i = 0; i < nx; i++)
			{
				y = -m_ms.bbSize.y / 2.0f + (dy / 2.0f);
				for (int j = 0; j < ny; j++)
				{

					if (m_simType == simulationType::Cylinder)
						d = std::sqrt(x * x + y * y);
					else //Horisontal
						d = std::sqrt(y * y + z * z);
					
					perc = 100 * (d / radius);

					if (d < radius)
					{
						int zoneIndexX = (int)((x + addX) / m_ms.zoneStep.x);
						int zoneIndexY = (int)((y + addY) / m_ms.zoneStep.y);
						int zoneIndexZ = (int)((z + addZ) / m_ms.zoneStep.z);

						//To avoid round off errors with the zone indexing
						if (zoneIndexX >= m_ms.zoneCount.x) zoneIndexX = m_ms.zoneCount.x - 1;
						if (zoneIndexY >= m_ms.zoneCount.y) zoneIndexY = m_ms.zoneCount.y - 1;
						if (zoneIndexZ >= m_ms.zoneCount.z)	zoneIndexZ = m_ms.zoneCount.z - 1;

						int zoneIndex = zoneIndexX + zoneIndexY * m_ms.zoneCount.x + zoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;

						Vertex v;
						v.Position = { x, y, z };
						Vec4 color;

						/*
						if (z < (-1.0 * hLimit))
						{
							//Neg z => Bottom of cylinder
							color = loadZoneColor;
							m_locked[pCounter] = true;
							m_loaded[pCounter] = false;
							bcCounter++;
						}
						else if (z > hLimit)
						{
							//Pos z => Top of cylinder
							color = loadZoneColor;
							m_loadDir[pCounter] = { 0, 0, -1 };
							m_locked[pCounter] = false;
							m_loaded[pCounter] = true;
							bcCounter++;
						}
						else
						{
							m_loadDir[pCounter] = { 0, 0, 0 };
							color = GetBlendedColor(perc);
							m_locked[pCounter] = false;
							m_loaded[pCounter] = false;
						}
						*/

						color = GetBlendedColor(perc);

						v.Color = color;
						m_vertices[pCounter] = v;
						m_pzTopFlat[pCounter] = zoneIndex;

						m_zpTop[zoneIndex].push_back(pCounter);
						m_zpCounter++;

						pCounter++;
					}

					y += dy;
				}//y-loop

				x += dx;
			}//x-loop

			z += dz;
		}//z-loop

		if (pCounter > m_nParticles)
			std::cout << "!WARNING! particle count larger then allocation size" << std::endl;

		m_nParticles = pCounter;

		//Generate particle indices
		for (int i = 0; i < m_nParticles; i++)
			m_Indices[i] = i;

		std::cout << "Create regular particles completed" << std::endl;
	}

	void TestCylinder::CreateParticlesRegularRectangle() 
	{
		float radius = m_ms.radius;
		float height = m_ms.height;
		float minDist = 0;

		int nx, ny, nz, cubePCount, pCountPerSide;
		double zsf, xsf;

		//Assuming that the y side is the smallest
		float numberOfYCubes_z = m_ms.bbSize.z / m_ms.bbSize.y;
		float numberOfYCubes_x = m_ms.bbSize.x / m_ms.bbSize.y;

		float numberOfYcubes = 1 * numberOfYCubes_z * numberOfYCubes_x;
		float numberOfParticlesPerYCube = m_nParticles / numberOfYcubes;
		float numberOfParticlesPerYcubeSide = (int)std::pow(numberOfParticlesPerYCube, 1.0 / 3.0);

		nx = numberOfParticlesPerYcubeSide * numberOfYCubes_x;
		ny = numberOfParticlesPerYcubeSide;
		nz = numberOfParticlesPerYcubeSide * numberOfYCubes_z;

		float dx = m_ms.bbSize.x / nx;
		float dy = m_ms.bbSize.y / ny;
		float dz = m_ms.bbSize.z / nz;

		//Move geometry to the positive quadrand by adding this values.
		float addX = m_ms.bbSize.x / 2.0f;
		float addY = m_ms.bbSize.y / 2.0f;
		float addZ = m_ms.bbSize.z / 2.0f;

		for (int i = 0; i < m_nZones; i++)
		{
			vector<int> zpi;
			m_zpTop.push_back(zpi);
		}

		float x, y, z, d, perc;
		int attempts = 0;
		int pCounter = 0;
		m_zpCounter = 0;

		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
		Vec4 paddingColor = { 0.75f, 0.75f, 0.75f, 1.0f };
		int zonesPerLevel = m_ms.zoneCount.x * m_ms.zoneCount.y;
		int maxZoneIndex = m_ms.zoneCount.x * m_ms.zoneCount.y * m_ms.zoneCount.z - 1;
		float hLimit = m_ms.bbSize.z / 2.0f - m_ms.bcThickness;
		float lLimit = m_ms.bbSize.x / 2.0f - m_ms.edgePadding;

		bool insideCylinder = false;
		bool insideH, insideL;


		z = -m_ms.bbSize.z / 2.0f + (dz / 2.0f);
		for (int k = 0; k < nz; k++)
		{
			x = -m_ms.bbSize.x / 2.0f + (dx / 2.0f);
			for (int i = 0; i < nx; i++)
			{
				y = -m_ms.bbSize.y / 2.0f + (dy / 2.0f);
				for (int j = 0; j < ny; j++)
				{
					if (m_simType == simulationType::Cylinder)
					{
						d = std::sqrt(x * x + y * y);
						perc = 100 * (d / radius);
					}
					else //Horisontal
					{
						d = std::sqrt(y * y + z * z);
						perc = 100 * (d / radius);
					}

					int zoneIndexX = (int)((x + addX) / m_ms.zoneStep.x);
					int zoneIndexY = (int)((y + addY) / m_ms.zoneStep.y);
					int zoneIndexZ = (int)((z + addZ) / m_ms.zoneStep.z);
					
					//To avoid round off errors with the zone indexing
					if (zoneIndexX >= m_ms.zoneCount.x) zoneIndexX = m_ms.zoneCount.x - 1;
					if (zoneIndexY >= m_ms.zoneCount.y) zoneIndexY = m_ms.zoneCount.y - 1;
					if (zoneIndexZ >= m_ms.zoneCount.z)	zoneIndexZ = m_ms.zoneCount.z - 1;

					int zoneIndex = zoneIndexX + zoneIndexY * m_ms.zoneCount.x + zoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;
					
					Vertex v;
					v.Position = { x, y, z };
					Vec4 color;
					
					insideL = !InsideLengthBC(lLimit, v.Position);

					/*
					
					insideH = false;

					if (z < 0 && insideH)
					{
						//Neg z => Bottom of rectangle
						color = loadZoneColor;
						m_locked[pCounter] = true;
						m_loaded[pCounter] = false;
					}
					else if (z > 0 && insideH)
					{
						//Pos z => Top of rectangle
						color = loadZoneColor;
						m_loadDir[pCounter] = { 0, 0, -1 };
						m_locked[pCounter] = false;
						m_loaded[pCounter] = true;
					}
					else
					{

					}
					if (insideL)
					{
					}
					*/

					if (insideL)
					{
						color = paddingColor;
					}
					else
					{
						m_loadDir[pCounter] = { 0, 0, 0 };
						color = GetBlendedColor(perc);
						m_locked[pCounter] = false;
						m_loaded[pCounter] = false;
					}


					v.Color = color;
					m_vertices[pCounter] = v;
					m_pzTopFlat[pCounter] = zoneIndex;

					m_zpTop[zoneIndex].push_back(pCounter);
					m_zpCounter++;

					pCounter++;
					
					y += dy;
				}//y-loop

				x += dx;
			}//x-loop

			z += dz;
		}//z-loop

		if (pCounter > m_nParticles)
			std::cout << "!WARNING! particle count larger then allocation size" << std::endl;

		m_nParticles = pCounter;

		//Generate particle indices
		for (int i = 0; i < m_nParticles; i++)
			m_Indices[i] = i;

		std::cout << "Create regular particles completed" << std::endl;
	}

	void TestCylinder::CreateParticlesRegularSteelTest()
	{
		float radius = m_ms.radius;
		float height = m_ms.height;
		float minDist = 0;

		int nx, ny, nz, cubePCount, pCountPerSide;
		double zsf, xsf;

		zsf = m_ms.bbSize.z / m_ms.bbSize.x;
		cubePCount = (int)(m_nParticles / zsf);
		pCountPerSide = (int)std::pow(cubePCount, 1.0 / 3.0);

		nx = pCountPerSide;
		ny = pCountPerSide;
		nz = (int)(pCountPerSide * zsf);

		float dx = m_ms.bbSize.x / nx;
		float dy = m_ms.bbSize.y / ny;
		float dz = m_ms.bbSize.z / nz;

		//Move geometry to the positive quadrand by adding this values.
		float addX = m_ms.bbSize.x / 2.0f;
		float addY = m_ms.bbSize.y / 2.0f;
		float addZ = m_ms.bbSize.z / 2.0f;

		for (int i = 0; i < m_nZones; i++)
		{
			vector<int> zpi;
			m_zpTop.push_back(zpi);
		}

		float x, y, z, d, perc;
		int attempts = 0;
		int pCounter = 0;
		m_zpCounter = 0;

		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
		int zonesPerLevel = m_ms.zoneCount.x * m_ms.zoneCount.y;
		int maxZoneIndex = m_ms.zoneCount.x * m_ms.zoneCount.y * m_ms.zoneCount.z - 1;
		float constraintsZlimit = 0; //m_zoneSideAverage * 0.45;

		float hLimit = (m_ms.bbSize.z / 2.0f) - m_ms.bcThickness;
		float lLimit = (m_ms.bbSize.x / 2.0f) - m_ms.bcThickness;
		bool insideBC, insideL;

		float b = 0.0f;
		float corrFac = 0.0f;

		m_zMin = -m_ms.bbSize.z / 2.0f + (dz / 2.0f);
		m_zMax = m_ms.bbSize.z / 2.0f - (dz / 2.0f);
		m_zPartST = m_zMax / 3.0f;
		float rLim = radius;

		z = -m_ms.bbSize.z / 2.0f + (dz / 2.0f);
		for (int k = 0; k < nz; k++)
		{
			x = -m_ms.bbSize.x / 2.0f + (dx / 2.0f);
			for (int i = 0; i < nx; i++)
			{
				y = -m_ms.bbSize.y / 2.0f + (dy / 2.0f);
				for (int j = 0; j < ny; j++)
				{
					d = std::sqrt(x * x + y * y);

					bool inside = InsideSteelTest2(d, z);

					perc = 100 * (d / radius);

					if (inside)
					{
						int zoneIndexX = (int)((x + addX) / m_ms.zoneStep.x);
						int zoneIndexY = (int)((y + addY) / m_ms.zoneStep.y);
						int zoneIndexZ = (int)((z + addZ) / m_ms.zoneStep.z);

						//To avoid round off errors with the zone indexing
						if (zoneIndexX >= m_ms.zoneCount.x) zoneIndexX = m_ms.zoneCount.x - 1;
						if (zoneIndexY >= m_ms.zoneCount.y) zoneIndexY = m_ms.zoneCount.y - 1;
						if (zoneIndexZ >= m_ms.zoneCount.z)	zoneIndexZ = m_ms.zoneCount.z - 1;

						int zoneIndex = zoneIndexX + zoneIndexY * m_ms.zoneCount.x + zoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;

						Vertex v;
						v.Position = { x, y, z };
						Vec4 color;

						color = GetBlendedColor(perc);

						v.Color = color;
						m_vertices[pCounter] = v;
						m_pzTopFlat[pCounter] = zoneIndex;

						m_zpTop[zoneIndex].push_back(pCounter);
						m_zpCounter++;

						pCounter++;
					}

					y += dy;
				}//y-loop

				x += dx;
			}//x-loop

			z += dz;
		}//z-loop

		if (pCounter > m_nParticles)
			std::cout << "!WARNING! particle count larger then allocation size" << std::endl;

		m_nParticles = pCounter;

		//Generate particle indices
		for (int i = 0; i < m_nParticles; i++)
			m_Indices[i] = i;

		std::cout << "Create regular particles completed" << std::endl;

	}

	void TestCylinder::CreateParticlesRegularDirectTension()
	{
		float radius = m_ms.radius;
		float height = m_ms.height;
		float minDist = 0;

		int nx, ny, nz, cubePCount, pCountPerSide;
		double zsf, xsf;

		zsf = m_ms.bbSize.z / m_ms.bbSize.x;
		cubePCount = (int)(m_nParticles / zsf);
		pCountPerSide = (int)std::pow(cubePCount, 1.0 / 3.0);

		nx = pCountPerSide;
		ny = pCountPerSide;
		nz = (int)(pCountPerSide * zsf);
		
		float dx = m_ms.bbSize.x / nx;
		float dy = m_ms.bbSize.y / ny;
		float dz = m_ms.bbSize.z / nz;

		//Move geometry to the positive quadrand by adding this values.
		float addX = m_ms.bbSize.x / 2.0f;
		float addY = m_ms.bbSize.y / 2.0f;
		float addZ = m_ms.bbSize.z / 2.0f;

		for (int i = 0; i < m_nZones; i++)
		{
			vector<int> zpi;
			m_zpTop.push_back(zpi);
		}

		float x, y, z, d, perc;
		int attempts = 0;
		int pCounter = 0;
		m_zpCounter = 0;

		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
		int zonesPerLevel = m_ms.zoneCount.x * m_ms.zoneCount.y;
		int maxZoneIndex = m_ms.zoneCount.x * m_ms.zoneCount.y * m_ms.zoneCount.z - 1;
		float constraintsZlimit = 0; //m_zoneSideAverage * 0.45;

		float hLimit = (m_ms.bbSize.z / 2.0f) - m_ms.bcThickness;
		float lLimit = (m_ms.bbSize.x / 2.0f) - m_ms.bcThickness;
		bool insideBC, insideL;

		float b = 0.0f;
		float corrFac = 0.0f;

		if (m_simType == simulationType::SplitCylinder)
		{
			b = 2.0f * std::sqrt(2.0f * m_ms.radius - m_ms.bcThickness * m_ms.bcThickness);
			float D = m_ms.radius * 2.0f;
			corrFac = std::pow((1.0f - std::pow((b / D), 2.0f)), (3.0f / 2.0f));
		}

		m_zMin = -m_ms.bbSize.z / 2.0f + (dz / 2.0f);
		m_zMax = m_ms.bbSize.z / 2.0f - (dz / 2.0f);
		m_zPart = m_zMax / 3.0f;
		float rLim = radius;

		z = -m_ms.bbSize.z / 2.0f + (dz / 2.0f);
		for (int k = 0; k < nz; k++)
		{
			x = -m_ms.bbSize.x / 2.0f + (dx / 2.0f);
			for (int i = 0; i < nx; i++)
			{
				y = -m_ms.bbSize.y / 2.0f + (dy / 2.0f);
				for (int j = 0; j < ny; j++)
				{
					d = std::sqrt(x * x + y * y);
					
					/*
					if (z > (m_zMin + 2.0f * zPart) && z < (m_zMax - 2.0f * zPart))
					{
						rLim = 0.5 * radius;
					}
					else if (z > (m_zMin + zPart) && z < (m_zMin + 2.0f * zPart))
					{
						float diff = z - (m_zMin + zPart);
						float domain = (m_zMin + 2.0f * zPart) - (m_zMin + zPart);
						float ratio = 0.5 + 0.5 *(1 - (diff / domain));
						rLim = (ratio) * radius;
					}
					else if (z > (zPart) && z < (2.0f * zPart))
					{	
						float diff = (2.0f * zPart) - z;
						float domain = zPart;
						float ratio = 0.5 + 0.5 * (1 - (diff / domain));
						rLim = (ratio)*radius;
					}
					else
					{
						rLim = radius;
					}
					*/

					bool inside = InsideDirectTension(d, z, radius);

					perc = 100 * (d / radius);

					if (inside)
					{
						int zoneIndexX = (int)((x + addX) / m_ms.zoneStep.x);
						int zoneIndexY = (int)((y + addY) / m_ms.zoneStep.y);
						int zoneIndexZ = (int)((z + addZ) / m_ms.zoneStep.z);

						//To avoid round off errors with the zone indexing
						if (zoneIndexX >= m_ms.zoneCount.x) zoneIndexX = m_ms.zoneCount.x - 1;
						if (zoneIndexY >= m_ms.zoneCount.y) zoneIndexY = m_ms.zoneCount.y - 1;
						if (zoneIndexZ >= m_ms.zoneCount.z)	zoneIndexZ = m_ms.zoneCount.z - 1;

						int zoneIndex = zoneIndexX + zoneIndexY * m_ms.zoneCount.x + zoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;

						Vertex v;
						v.Position = { x, y, z };
						Vec4 color;

						color = GetBlendedColor(perc);

						v.Color = color;
						m_vertices[pCounter] = v;
						m_pzTopFlat[pCounter] = zoneIndex;

						m_zpTop[zoneIndex].push_back(pCounter);
						m_zpCounter++;

						pCounter++;
					}

					y += dy;
				}//y-loop

				x += dx;
			}//x-loop

			z += dz;
		}//z-loop

		if (pCounter > m_nParticles)
			std::cout << "!WARNING! particle count larger then allocation size" << std::endl;

		m_nParticles = pCounter;

		//Generate particle indices
		for (int i = 0; i < m_nParticles; i++)
			m_Indices[i] = i;

		std::cout << "Create regular particles completed" << std::endl;

	}

	void TestCylinder::CreateParticlesRegularSlice()
	{
		float radius = m_ms.radius;
		float height = m_ms.height;
		float minDist = 0;

		int nx, ny, nz, cubePCount, pCountPerSide;
		double zsf, xsf;

		if (m_simType == simulationType::Cylinder)
		{
			zsf = m_ms.bbSize.z / m_ms.bbSize.x;
			cubePCount = (int)(m_nParticles / zsf);
			pCountPerSide = (int)std::pow(cubePCount, 1.0 / 3.0);

			nx = pCountPerSide;
			ny = pCountPerSide;
			nz = (int)(pCountPerSide * zsf);
		}
		else //Horizontal
		{
			xsf = m_ms.bbSize.x / m_ms.bbSize.y;
			cubePCount = (int)(m_nParticles / xsf);
			pCountPerSide = (int)std::pow(cubePCount, 1.0 / 3.0);

			nx = (int)(pCountPerSide * xsf);
			ny = pCountPerSide;
			nz = pCountPerSide;
		}

		float dx = m_ms.bbSize.x / nx;
		float dy = m_ms.bbSize.y / ny;
		float dz = m_ms.bbSize.z / nz;

		//Move geometry to the positive quadrand by adding this values.
		float addX = m_ms.bbSize.x / 2.0f;
		float addY = m_ms.bbSize.y / 2.0f;
		float addZ = m_ms.bbSize.z / 2.0f;

		for (int i = 0; i < m_nZones; i++)
		{
			vector<int> zpi;
			m_zpTop.push_back(zpi);
		}

		float x, y, z, d, perc;
		int attempts = 0;
		int pCounter = 0;
		m_zpCounter = 0;

		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
		int zonesPerLevel = m_ms.zoneCount.x * m_ms.zoneCount.y;
		int maxZoneIndex = m_ms.zoneCount.x * m_ms.zoneCount.y * m_ms.zoneCount.z - 1;
		
		float aLimit = m_ms.bcAngle;
		float rLimit = m_ms.radius - m_ms.bcThickness;
		float hLimit = m_ms.bbSize.z / 2.0f - m_ms.bcThickness;
		float lLimit = m_ms.bbSize.x / 2.0f - m_ms.bcThickness;
		Vec3 vBase = { 0, 0, 1};

		z = -m_ms.bbSize.z / 2.0f + (dz / 2.0f);
		for (int k = 0; k < nz; k++)
		{
			x = -m_ms.bbSize.x / 2.0f + (dx / 2.0f);
			for (int i = 0; i < nx; i++)
			{
				y = -m_ms.bbSize.y / 2.0f + (dy / 2.0f);
				for (int j = 0; j < ny; j++)
				{
					if (m_simType == simulationType::Cylinder)
					{
						d = std::sqrt(x * x + y * y);
						perc = 100 * (d / radius);
					}
					else //Horisontal
					{
						d = std::sqrt(y * y + z * z);
						perc = 100 * (d / radius);
					}


					if (d < radius)
					{
						int zoneIndexX = (int)((x + addX) / m_ms.zoneStep.x);
						int zoneIndexY = (int)((y + addY) / m_ms.zoneStep.y);
						int zoneIndexZ = (int)((z + addZ) / m_ms.zoneStep.z);

						//To avoid round off errors with the zone indexing
						if (zoneIndexX >= m_ms.zoneCount.x) zoneIndexX = m_ms.zoneCount.x - 1;
						if (zoneIndexY >= m_ms.zoneCount.y) zoneIndexY = m_ms.zoneCount.y - 1;
						if (zoneIndexZ >= m_ms.zoneCount.z)	zoneIndexZ = m_ms.zoneCount.z - 1;

						int zoneIndex = zoneIndexX + zoneIndexY * m_ms.zoneCount.x + zoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;

						Vertex v;
						v.Position = { x, y, z };
						Vec4 color;
						bool insideBC = false;
						bool insideL = false;

						if (m_simType == simulationType::Cylinder)
						{
							insideBC = InsideHeightBC(hLimit, v.Position);
							insideL = true;
						}
						else
						{
							insideBC = InsideSliceBC(rLimit, aLimit, vBase, v.Position);
							insideL = InsideLengthBC(lLimit, v.Position);
						}
						
						if (z < 0 && (insideBC && insideL))
						{
							//Neg z => Bottom of cylinder
							color = loadZoneColor;
							m_locked[pCounter] = true;
							m_loaded[pCounter] = false;
						}
						else if (z > 0 && (insideBC && insideL))
						{
							//Pos z => Top of cylinder
							color = loadZoneColor;
							m_loadDir[pCounter] = { 0, 0, -1 };
							m_locked[pCounter] = false;
							m_loaded[pCounter] = true;
						}
						else
						{
							m_loadDir[pCounter] = { 0, 0, 0 };
							color = GetBlendedColor(perc);
							m_locked[pCounter] = false;
							m_loaded[pCounter] = false;
						}

						v.Color = color;
						m_vertices[pCounter] = v;
						m_pzTopFlat[pCounter] = zoneIndex;

						m_zpTop[zoneIndex].push_back(pCounter);
						m_zpCounter++;

						pCounter++;
					}

					y += dy;
				}//y-loop

				x += dx;
			}//x-loop

			z += dz;
		}//z-loop

		if (pCounter > m_nParticles)
			std::cout << "!WARNING! particle count larger then allocation size" << std::endl;

		m_nParticles = pCounter;

		//Generate particle indices
		for (int i = 0; i < m_nParticles; i++)
			m_Indices[i] = i;

		std::cout << "Create regular particles completed" << std::endl;
	}

	void TestCylinder::CreateParticlesRegularGradient()
	{
		float radius = m_ms.radius;
		float diameter = 2.0f * m_ms.radius;
		float height = m_ms.height;
		float minDist = 0;

		//Move geometry to the positive quadrand by adding this values.
		float addX = m_ms.bbSize.x / 2.0f;
		float addY = m_ms.bbSize.y / 2.0f;
		float addZ = m_ms.bbSize.z / 2.0f;

		for (int i = 0; i < m_nZones; i++)
		{
			vector<int> zpi;
			m_zpTop.push_back(zpi);
		}

		vector<int> nx = vector<int>();
		vector<int> ny = vector<int>();
		vector<float> zSteps = vector<float>();

		int nyMin = (int) pow(m_nParticles / 4.0f, 1.0f / 3.0f) - 2;

		int yRange = nyMin * 2;
		int nz = nyMin * 2 + 1;

		float zStep = 2.0 / (nz+1); //(nz - 1.0f);
		float val = -1;
		float valExpX = 0;
		float valExpY = 0;
		float valExpZ = 0;
		float zSum = 0;

		int exponenet = 1;

		for (int i = 0; i < nz; i++)
		{	
			val = val + zStep;
			valExpY = abs((yRange - nyMin) * pow(val, exponenet));
			valExpX = valExpY;
			valExpZ = -1 * abs(pow(val, exponenet));
			int nSteps = (int)(nyMin + valExpX);
			nx.push_back(2 * nSteps);
			ny.push_back(nSteps);
			zSteps.push_back(1 + valExpZ);
			zSum += zSteps[i];
		}

		float sf = diameter / zSum;
		for (int i = 0; i < nz; i++)
			zSteps[i] = sf * zSteps[i];

		float x, y, z, d, perc;
		int attempts = 0;
		int pCounter = 0;
		m_zpCounter = 0;

		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
		int zonesPerLevel = m_ms.zoneCount.x * m_ms.zoneCount.y;
		int maxZoneIndex = m_ms.zoneCount.x * m_ms.zoneCount.y * m_ms.zoneCount.z - 1;
		float constraintsZlimit = m_ms.bcThickness;

		float padding = zSteps[0]/2.0;

		//Generate particles
		z = -diameter / 2.0;

		for (int i = 0; i < zSteps.size(); i++)
		{
			float xPadding = 1.0f / nx[i];
			float length = 2 * diameter;
			float lengthWithPaddingX = (1.0f - xPadding) * length;

			float dx = lengthWithPaddingX / nx[i];
			x = -lengthWithPaddingX / 2.0f;

			for (int j = 0; j <= nx[i]; j++)
			{
				float yPadding = 1.0f / ny[i];
				float diameterWithPaddingY = (1 + yPadding) * diameter;

				float dy = diameterWithPaddingY / ny[i];
				y = -diameterWithPaddingY / 2.0;

				float pSize = 0.8 * GetSmallest(dx, dy, zSteps[i]);	//Get the size estimate from the grid stepping	

				for (int k = 0; k <= ny[i]; k++)
				{
					d = sqrt(y * y + z * z);
					perc = 100 * (d / radius);

					if (d < (radius - padding))			//Add some padding
					{
						//Create particles
						int zoneIndexX = (int)((x + addX) / m_ms.zoneStep.x);
						int zoneIndexY = (int)((y + addY) / m_ms.zoneStep.y);
						int zoneIndexZ = (int)((z + addZ) / m_ms.zoneStep.z);

						//To avoid round off errors with the zone indexing
						if (zoneIndexX >= m_ms.zoneCount.x) zoneIndexX = m_ms.zoneCount.x - 1;
						if (zoneIndexY >= m_ms.zoneCount.y) zoneIndexY = m_ms.zoneCount.y - 1;
						if (zoneIndexZ >= m_ms.zoneCount.z)	zoneIndexZ = m_ms.zoneCount.z - 1;

						int zoneIndex = zoneIndexX + zoneIndexY * m_ms.zoneCount.x + zoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;

						Vertex v;
						v.Position = { x, y, z };
						Vec4 color;

						//if (zoneIndex < zonesPerLevel)
						if (z < (-m_ms.bbSize.z / 2.0f) + constraintsZlimit)
						{
							//Bottom of cylinder
							color = loadZoneColor;
							m_locked[pCounter] = true;
							m_loaded[pCounter] = false;
						}
						//else if (zoneIndex > (maxZoneIndex - zonesPerLevel))
						else if (z > (m_ms.bbSize.z / 2.0f - constraintsZlimit))
						{
							//Top of cylinder
							color = loadZoneColor;
							m_loadDir[pCounter] = { 0, 0, -1 };
							m_locked[pCounter] = false;
							m_loaded[pCounter] = true;
						}
						else
						{
							m_loadDir[pCounter] = { 0, 0, 0 };
							color = GetBlendedColor(perc);
							m_locked[pCounter] = false;
							m_loaded[pCounter] = false;
						}

						v.Color = color;
						m_vertices[pCounter] = v;
						m_pzTopFlat[pCounter] = zoneIndex;

						m_pMassSize[pCounter] = pSize;
						m_pBaseSize[pCounter] = 2 * pSize;


						m_zpTop[zoneIndex].push_back(pCounter);
						m_zpCounter++;

						pCounter++;

					}
					y += dy;
				}
				x+= dx;
			}
			z += zSteps[i];
		}


		if (pCounter > m_nParticles)
			std::cout << "!WARNING! particle count larger then allocation size" << std::endl;

		m_nParticles = pCounter;

		//Generate particle indices
		for (int i = 0; i < m_nParticles; i++)
			m_Indices[i] = i;

		std::cout << "Create regular particles with gradient completed" << std::endl;
	}

	void TestCylinder::UpdateBcCylinder() 
	{
		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };
		float d = 0.0f;
		float perc = 0.0f;
		float hLimit = (m_ms.bbSize.z / 2.0f) - m_ms.bcThickness;

		Vec4 color;

		for (int i = 0; i < m_nParticles; i++) 
		{	
			m_locked[i] = false;
			m_loaded[i] = false;

			Vertex v = m_vertices[i];

			if (v.Position.z < (-1.0 * hLimit))
			{
				//Neg z => Bottom of cylinder
				m_vertices[i].Color = loadZoneColor;
				m_locked[i] = true;
				m_loaded[i] = false;
			}
			else if (v.Position.z > hLimit)
			{
				//Pos z => Top of cylinder
				m_vertices[i].Color = loadZoneColor;
				m_locked[i] = false;
				m_loaded[i] = true;
			}
		}
	}

	void TestCylinder::UpdateBcRectangle() 
	{
		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };

		for (int i = 0; i < 4; i++)
		{
			Vec3 cp = m_loadCylinders[i].iPosition;
			float smallestDist = 1e30;

			for (int j = 0; j < m_nParticles; j++)
			{
				bool b = IsParticleInsideCylinder(m_vertices[j].Position, m_loadCylinders[i]);

				if (b && m_loadCylinders[i].type == BCtype::load)
				{
					m_locked[j] = false;
					m_loaded[j] = true;
					m_vertices[j].Color = loadZoneColor;
				}
				else if (b && m_loadCylinders[i].type == BCtype::support)
				{
					m_locked[j] = true;
					m_loaded[j] = false;
					m_vertices[j].Color = loadZoneColor;
				}
			}
		}
	}

	bool TestCylinder::InsideDirectTension(float d, float z, float radius) 
	{	
		float rLim = 0.0f;

		if (z > (m_zMin + 2.0f * m_zPart) && z < (m_zMax - 2.0f * m_zPart))
		{
			rLim = m_waistSF * radius;
		}
		else if (z > (m_zMin + m_zPart) && z < (m_zMin + 2.0f * m_zPart))
		{
			float diff = z - (m_zMin + m_zPart);
			float domain = (m_zMin + 2.0f * m_zPart) - (m_zMin + m_zPart);
			float ratio = m_waistSF + m_waistSF * (1 - (diff / domain));
			rLim = (ratio)*radius;
		}
		else if (z > (m_zPart) && z < (2.0f * m_zPart))
		{
			float diff = (2.0f * m_zPart) - z;
			float domain = m_zPart;
			float ratio = m_waistSF + m_waistSF * (1 - (diff / domain));
			rLim = (ratio)*radius;
		}
		else
		{
			rLim = radius;
		}

		if (d < rLim)
			return true;

		return false;
	}

	bool TestCylinder::InsideSteelTest(float d, float z, float radius)
	{
		float rLim = 0.0f;

		if (z > (m_zMin + 2.0f * m_zPartST) && z < (m_zMax - 2.0f * m_zPartST))
		{
			rLim = m_waistSF * radius;
		}
		else if (z > (m_zMin + m_zPartST) && z < (m_zMin + 2.0f * m_zPartST))
		{
			float diff = z - (m_zMin + m_zPartST);
			float domain = (m_zMin + 2.0f * m_zPartST) - (m_zMin + m_zPartST);
			float ratio = m_waistSF + m_waistSF * (1 - (diff / domain));
			rLim = (ratio)*radius;
		}
		else if (z > (m_zPartST) && z < (2.0f * m_zPartST))
		{
			float diff = (2.0f * m_zPartST) - z;
			float domain = m_zPartST;
			float ratio = m_waistSF + m_waistSF * (1 - (diff / domain));
			rLim = (ratio)*radius;
		}
		else
		{
			rLim = radius;
		}

		if (d < rLim)
			return true;

		return false;
	}

	bool TestCylinder::InsideSteelTest2(float d, float z) 
	{
		float hz = m_ms.height;
		float pLim1 = 0.075f;
		float pLim2 = 0.15f;

		float lim0 = -hz / 2.0f;
		float lim1 = pLim1 * hz - hz / 2.0f;
		float lim2 = pLim2 * hz - hz / 2.0f;

		float lim3 = (1.0f - pLim2) * hz - hz / 2.0f;
		float lim4 = (1.0f - pLim1) * hz - hz / 2.0f;
		float lim5 = hz / 2.0f;

		float rLarge = m_ms.radius;
		float rSmall = m_ms.radius * m_ms.waistSF;
		float rMid = (rLarge + rSmall) / 2.0f;

		bool inside = false;

		if (z > lim0 && z < lim1)
		{
			if(d < rLarge)
				inside = true;
		}
		else if (z > lim1 && z < lim2)
		{
			float range = lim2 - lim1;
			float valInRange = z - lim1;
			float percent = valInRange / range;

			float rRange = rLarge - rSmall;
			float rLimit = rSmall + std::pow((1.0f - percent), 2.0f) * rRange;

			//Gradual decrease
			if (d < rLimit)
				inside = true;
		}
		else if (z > lim2 && z < lim3)
		{
			//Constant smalletst r
			if (d < rSmall)
				inside = true;
		}
		else if (z > lim3 && z < lim4)
		{
			float range = lim4 - lim3;
			float valInRange = z - lim3;
			float percent = valInRange / range;

			float rRange = rLarge - rSmall;
			float rLimit = rSmall + std::pow((percent),2.0f) *rRange;

			//Gradual increase
			if (d < rLimit)
				inside = true;
		}
		else if (z > lim4 && z < lim5)
		{
			//Do nothing
			if (d < rLarge)
				inside = true;
		}

		if (inside)
			return true;

		return false;
	}


	void TestCylinder::CreateLoadCylinders() 
	{
		float pSpacing = Distance(m_vertices[0], m_vertices[1]);

		float cylinderRadius = 0.015f;
		Vec3 normal = { 0.0f , 1.0f, 0.0f };

		float z = m_ms.bbSize.z; 
		float L = 3.0f * z;

		float SC1x = -1.0f * L / 2.0f;
		float SC2x = L / 2.0f;
		float SC1y = 0.0f;
		float SC2y = 0.0f;
		float SC1z = -1.0f * (z / 2.0f) - cylinderRadius + m_ms.bcThickness; //+ (pSpacing / 2.0f);
		float SC2z = -1.0f * (z / 2.0f) - cylinderRadius + m_ms.bcThickness; // +(pSpacing / 2.0f);

		Vec3 SC1p = { SC1x, SC1y, SC1z };
		Vec3 SC2p = { SC2x, SC2y, SC2z };
		Vec3 iSC1p = { SC1x, SC1y, SC1z };
		Vec3 iSC2p = { SC2x, SC2y, SC2z };
		
		LoadingCylinder SC1 = {SC1p, normal, cylinderRadius, BCtype::support, iSC1p};
		LoadingCylinder SC2 = {SC2p, normal, cylinderRadius, BCtype::support, iSC2p};

		float LC1x = -1.0f * L / 6.0f;
		float LC2x = L / 6.0f;
		float LC1y = 0.0f;
		float LC2y = 0.0f;
		float LC1z = (z / 2.0f) + cylinderRadius - m_ms.bcThickness; // - (pSpacing / 2.0f);
		float LC2z = (z / 2.0f) + cylinderRadius - m_ms.bcThickness; // -(pSpacing / 2.0f);

		Vec3 LC1p = { LC1x, LC1y, LC1z };
		Vec3 LC2p = { LC2x, LC2y, LC2z };
		Vec3 iLC1p = { LC1x, LC1y, LC1z };
		Vec3 iLC2p = { LC2x, LC2y, LC2z };

		LoadingCylinder LC1 = { LC1p, normal, cylinderRadius, BCtype::load, iLC1p };
		LoadingCylinder LC2 = { LC2p, normal, cylinderRadius, BCtype::load, iLC2p };

		m_loadCylinders[0] = SC1;
		m_loadCylinders[1] = SC2;
		m_loadCylinders[2] = LC1;
		m_loadCylinders[3] = LC2;

		/*
		Vec4 loadZoneColor = { 0.5f, 0.5f, 0.5f, 1.0f };

		for (int i = 0; i < 4; i++) 
		{
			Vec3 cp = m_loadCylinders[i].iPosition;
			float smallestDist = 1e30;

			for (int j = 0; j < m_nParticles; j++) 
			{
				bool b = IsParticleInsideCylinder(m_vertices[j].Position, m_loadCylinders[i]);

				if (b && m_loadCylinders[i].type == BCtype::load) 
				{
					m_locked[j] = false;
					m_loaded[j] = true;
					m_vertices[j].Color = loadZoneColor;
				}
				else if (b && m_loadCylinders[i].type == BCtype::support)
				{
					m_locked[j] = true;
					m_loaded[j] = false;
					m_vertices[j].Color = loadZoneColor;
				}
			}
		}
		*/
	}

	void TestCylinder::EstimateBaseSize()
	{
		float pi = 3.1415;
		float volTarget = m_ms.radius * m_ms.radius * pi * m_ms.height;

		if (m_simType == simulationType::SteelTest)
			volTarget = 3.0430e-06;

		float volP = volTarget / m_nParticles;

		//Calc radius from particle volume
		m_massSizeEstimate = pow(((3 * volP) / (4 * pi)), (1.0f / 3.0f));

		//Set base size to twice that size. Used later for size calcs.
		m_baseSizeEstimate = 2 * m_massSizeEstimate;


		for (int i = 0; i < m_nParticles; i++)
		{
			m_pBaseSize[i] = m_baseSizeEstimate;

			m_pMassSize[i] = m_massSizeEstimate;
		}

		std::cout << "Base size estimate: " << m_baseSizeEstimate << std::endl;
	}

	void TestCylinder::CreateZones()
	{
		float xMp, yMp, zMp;
		//Zero zone related counters
		m_zVertexCounter = 0;
		m_zLineIndexCounter = 0;
		m_zzCounter = 0;

		//Creating zone vertices and zone line indices for the zones. Allowing dup vertices positions for simplicity.
		zMp = (-1.0f * m_ms.bbSize.z / 2.0f) + (m_ms.zoneStep.z / 2.0f);
		for (int i = 0; i < m_ms.zoneCount.z; i++)
		{
			yMp = (-1.0f * m_ms.bbSize.y / 2.0f) + (m_ms.zoneStep.y / 2.0f);;
			for (int j = 0; j < m_ms.zoneCount.y; j++)
			{
				xMp = (-1.0f * m_ms.bbSize.x / 2.0f) + (m_ms.zoneStep.x / 2.0f);;
				for (int k = 0; k < m_ms.zoneCount.x; k++)
				{
					CreateZone(xMp, yMp, zMp);
					CreateZoneTopology(xMp, yMp, zMp);
					xMp += m_ms.zoneStep.x;
				}
				yMp += m_ms.zoneStep.y;
			}
			zMp += m_ms.zoneStep.z;
		}

		std::cout << "Create zones completed" << std::endl;
	}

	void TestCylinder::CreateZone(float x, float y, float z)
	{
		float xHalfStep = m_ms.zoneStep.x / 2.0f;
		float yHalfStep = m_ms.zoneStep.y / 2.0f;
		float zHalfStep = m_ms.zoneStep.z / 2.0f;

		Vec4 color = { 0.9f, 0.9f, 0.9f, 0.5f };
		Vertex v1, v2, v3, v4, v5, v6, v7, v8;
		v1.Position = { x - xHalfStep, y - yHalfStep, z - zHalfStep };
		v2.Position = { x + xHalfStep, y - yHalfStep, z - zHalfStep };
		v3.Position = { x + xHalfStep, y + yHalfStep, z - zHalfStep };
		v4.Position = { x - xHalfStep, y + yHalfStep, z - zHalfStep };

		v5.Position = { x - xHalfStep, y + yHalfStep, z + zHalfStep };
		v6.Position = { x - xHalfStep, y - yHalfStep, z + zHalfStep };
		v7.Position = { x + xHalfStep, y - yHalfStep, z + zHalfStep };
		v8.Position = { x + xHalfStep, y + yHalfStep, z + zHalfStep };

		vector<Vertex> zv = { v1,v2,v3,v4,v5,v6,v7,v8 };

		for (int i = 0; i < 8; i++)
		{
			zv[i].Color = color;
			m_zoneVertices1D[m_zVertexCounter] = zv[i];
			m_zVertexCounter++;

			m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 1;
			m_zLineIndexCounter++;

			if (i == 7)
				m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 4;
			else
				m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter;

			m_zLineIndexCounter++;
		}

		m_zoneVertices2D.push_back(zv);

		m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 8;
		m_zLineIndexCounter++;
		m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 8 + 5;
		m_zLineIndexCounter++;

		m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 7;
		m_zLineIndexCounter++;
		m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 7 + 5;
		m_zLineIndexCounter++;

		m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 6;
		m_zLineIndexCounter++;
		m_zoneLineIndices[m_zLineIndexCounter] = m_zVertexCounter - 6 + 5;
		m_zLineIndexCounter++;
	}

	void TestCylinder::CreateZoneTopology(float xMp, float yMp, float zMp)
	{
		//Move geometry to the positive quadrand by adding this values.
		float addX = m_ms.bbSize.x / 2.0f;
		float addY = m_ms.bbSize.y / 2.0f;
		float addZ = m_ms.bbSize.z / 2.0f;

		xMp += addX;
		yMp += addY;
		zMp += addZ;

		int zoneIndexX = (int)(xMp / m_ms.zoneStep.x);
		int zoneIndexY = (int)(yMp / m_ms.zoneStep.y);
		int zoneIndexZ = (int)(zMp / m_ms.zoneStep.z);

		int zoneIndex = zoneIndexX + zoneIndexY * m_ms.zoneCount.x + zoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;

		vector<int> neighIndex;
		neighIndex.push_back(zoneIndex);	//Add self first!
		m_zzCounter++;						//And increment the counter.

		int nZoneIndexX, nZoneIndexY, nZoneIndexZ;

		for (int i = -1; i < 2; i++)
		{
			nZoneIndexX = zoneIndexX + i;

			for (int j = -1; j < 2; j++)
			{
				nZoneIndexY = zoneIndexY + j;

				for (int k = -1; k < 2; k++)
				{
					nZoneIndexZ = zoneIndexZ + k;

					if (nZoneIndexX >= 0 && nZoneIndexX < m_ms.zoneCount.x &&
						nZoneIndexY >= 0 && nZoneIndexY < m_ms.zoneCount.y &&
						nZoneIndexZ >= 0 && nZoneIndexZ < m_ms.zoneCount.z)
					{
						int nZoneIndex = nZoneIndexX + nZoneIndexY * m_ms.zoneCount.x + nZoneIndexZ * m_ms.zoneCount.x * m_ms.zoneCount.y;

						if (nZoneIndex < m_nZones && nZoneIndex != zoneIndex)
						{
							neighIndex.push_back(nZoneIndex);
							m_zzCounter++;
						}
					}
				}
			}
		}

		m_zzTop.push_back(neighIndex);
	}

	void TestCylinder::GenDiscs()
	{
		int counter = 0;
		int nextIndex = 0;
		int vertexIndexCounter = 0;
		float smallestHa = 1e10;
		float largestHa = -1e10;

		for (int i = 0; i < m_nParticles; i++)
		{
			if (m_pMassSize[i] > largestHa)
				largestHa = m_pMassSize[i];

			if (m_pMassSize[i] < smallestHa)
				smallestHa = m_pMassSize[i];
		}

		float haMaxForColor = largestHa - smallestHa;

		for (int i = 0; i < m_nParticles; i++)
		{
			float haForColor = m_pMassSize[i] - smallestHa; //Make sure the size goes from 0 -> upwards for percentage calc.
			float perc = 100 * (haForColor / haMaxForColor);

			Vec4 color = GetBlendedColor(perc);

			Vertex v = m_vertices[i];
			vector<Vertex> discVertices = CreateDisc(v.Position.x, v.Position.y, v.Position.z, m_pMassSize[i], m_nDiscSides, color);
			vector<int> discIndices = GetDiscIndices(nextIndex, m_nDiscSides);

			nextIndex += m_nDiscSides + 1;

			//Copy vertices to global array
			for (int j = 0; j < discVertices.size(); j++)
			{
				m_discVertices[counter] = discVertices[j];
				counter++;
			}

			//Copy indices to global array
			for (int j = 0; j < discIndices.size(); j++)
			{
				m_discIndices[vertexIndexCounter] = discIndices[j];
				vertexIndexCounter++;
			}
		}
	}

	void TestCylinder::GenIcosas()
	{
		int counter = 0;
		int nextIndex = 0;
		int vertexIndexCounter = 0;

		for (int i = 0; i < m_nParticles; i++)
		{
			Vertex v = m_vertices[i];
			vector<Vertex> icosaVertices = CreateIcoSphere(v.Position.x, v.Position.y, v.Position.z, m_pMassSize[i], m_vertices[i].Color);
			vector<int> icosaIndices = GetIcoSphereIndices(nextIndex);

			nextIndex += 12;

			Vertex vCenter;
			vCenter.Position = m_vertices[i].Position;
			vCenter.Color = m_vertices[i].Color;
			m_icosaCenter[i] = vCenter;

			//Copy vertices to global array
			for (int j = 0; j < icosaVertices.size(); j++)
			{
				m_icosaVertices[counter] = icosaVertices[j];
				counter++;
			}

			//Copy indices to global array
			for (int j = 0; j < icosaIndices.size(); j++)
			{
				m_icosaIndices[vertexIndexCounter] = icosaIndices[j];
				vertexIndexCounter++;
			}
		}

	}

	void TestCylinder::GenCylinders() 
	{
		int counter = 0;
		int nextIndex = 0;
		int vertexIndexCounter = 0;
		int divCount = 20;
		float length = m_ms.bbSize.y;

		for (int i = 0; i < 4; i++) 
		{
			LoadingCylinder LC = m_loadCylinders[i];
			float addZ = m_ms.bcThickness;
			if (LC.iPosition.z < 0)
				addZ = -1.0 * addZ;

			Vec3 cp = {LC.iPosition.x, LC.iPosition.y, LC.iPosition.z + addZ};
			vector<Vertex> cylinderVertices = CreateCylinder(cp, LC.Radius, m_nCylinderTriangles, length);
			vector<int> cylinderIndices = GetCylinderIndices(nextIndex, m_nCylinderTriangles);
			nextIndex += m_nCylinderTriangles * 2;

			//Copy vertices to global array
			for (int j = 0; j < cylinderVertices.size(); j++)
			{
				m_cylinderVertices[counter] = cylinderVertices[j];
				Vertex vCopy;
				vCopy.Position.x = cylinderVertices[j].Position.x;
				vCopy.Position.y = cylinderVertices[j].Position.y;
				vCopy.Position.z = cylinderVertices[j].Position.z;
				m_cylinderVerticesInit[counter] = vCopy;
				counter++;
			}

			//Copy indices to global array
			for (int j = 0; j < cylinderIndices.size(); j++)
			{
				m_cylinderIndices[vertexIndexCounter] = cylinderIndices[j];
				vertexIndexCounter++;
			}
		
		}
	}

	void TestCylinder::IncrementParticleSize()
	{
		Vertex v, vNext;
		float d = 0;
		int vIndex, vNextIndex;
		m_pRequiredFriendsCount = 5;
		float averageSize = 0;

		//Loop over the particles
		for (int i = 0; i < m_nParticles; i++)
		{
			int currentZone = m_pzTopFlat[i];
			vIndex = i;
			v = m_vertices[vIndex];
			m_pCurrentFriendsCount[i] = 0;

			//Loop over neighbouring zones
			for (int j = 0; j < m_zzTop[currentZone].size(); j++)
			{
				int neighbZone = m_zzTop[currentZone][j];

				//Loop over particles in neighbouring zone
				for (int k = 0; k < m_zpTop[neighbZone].size(); k++)
				{
					vNextIndex = m_zpTop[neighbZone][k];
					vNext = m_vertices[vNextIndex];
					d = Distance(v, vNext);

					//Create a line
					if (d < m_pBaseSize[i] && vIndex != vNextIndex)
					{
						m_pCurrentFriendsCount[i]++;
					}
				}
			}
			m_pBaseSize[i] += 0.0001 * (m_pRequiredFriendsCount - m_pCurrentFriendsCount[i]);
			averageSize += m_pBaseSize[i] / m_nParticles;
		}
		std::cout << "Size increment done, with average particle size: " << averageSize << std::endl;
	}

	void TestCylinder::IncrementParticleSize1D(int requiredFriendsCount)
	{
		int currentFriendsCount = 0;
		float averageSize = 0;

		for (int i = 0; i < m_nParticles; i++)
		{
			int currentZone = m_pzTopFlat[i];
			int nNeighZones = m_zzRowLength[currentZone];
			int pIndexGlobal = i;

			Vertex p = m_vertices[i];
			currentFriendsCount = 0;

			float baseSize = m_pBaseSize[pIndexGlobal];

			float haSquared = baseSize * baseSize;

			for (int neighZoneRowIndex = 0; neighZoneRowIndex < nNeighZones; neighZoneRowIndex++)
			{
				int zzFlatIndex = m_zzStartIndex[currentZone] + neighZoneRowIndex;
				int zIndexGlobal = m_zzTopFlat[zzFlatIndex];

				int nParticlesInNeighZone = m_zpRowLength[zIndexGlobal];

				//Loop over the particles in the neighbouring zone
				for (int pRowIndexInNeighZone = 0; pRowIndexInNeighZone < nParticlesInNeighZone; pRowIndexInNeighZone++)
				{
					int zpFlatIndexNZ = m_zpStartIndex[zIndexGlobal] + pRowIndexInNeighZone;
					int pIndexGlobalNZ = m_zpTopFlat[zpFlatIndexNZ];

					//Avoid checking the particle against itself
					if (pIndexGlobal != pIndexGlobalNZ)
					{
						Vertex pNext = m_vertices[pIndexGlobalNZ];

						float xDiff = pNext.Position.x - p.Position.x;
						float yDiff = pNext.Position.y - p.Position.y;
						float zDiff = pNext.Position.z - p.Position.z;

						//Square the distances to avoid square root computation
						float dSquared = (xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);

						if (dSquared < haSquared)
						{
							currentFriendsCount++;
						}
					}
				}
			}
			//Loop around all neighbouring zones/particles finished, so size can be updated.
			if(m_simType == simulationType::SteelTest)
				m_pBaseSize[pIndexGlobal] += 0.00001 * (requiredFriendsCount - currentFriendsCount);
			else
				m_pBaseSize[pIndexGlobal] += 0.0001 * (requiredFriendsCount - currentFriendsCount);


			averageSize += m_pBaseSize[pIndexGlobal] / m_nParticles;
		}
		std::cout << "Average base size: " << averageSize << std::endl;
	}

	void TestCylinder::ShakeParticles(int rndSeed)
	{
		float averageSize = 0;
		bool tooClose, outside;
		int attemptsCounter = 0;
		int shakeCounter = 0;
		int tooCloseCounter = 0;
		float rnd;
		float overlapSF = 0.75;

		srand(rndSeed);

		for (int i = 0; i < m_nParticles; i++)
		{
			int currentZone = m_pzTopFlat[i];
			int nNeighZones = m_zzRowLength[currentZone];
			int pIndexGlobal = i;

			Vertex p = m_vertices[i];

			float massSize = m_pMassSize[pIndexGlobal];

			float sf = 0.2 * massSize;

			Vec3 rndVec = { GenRandom(-1, 1), GenRandom(-1,1), GenRandom(-1,1) };
			rndVec = UnitizeVector(rndVec);

			float newX = p.Position.x + sf * rndVec.x;
			float newY = p.Position.y + sf * rndVec.y;
			float newZ = p.Position.z + sf * rndVec.z;

			double r;

			if (m_simType == simulationType::Cylinder || m_simType == simulationType::DirectTension || m_simType == simulationType::SteelTest)
				r = sqrt(newX * newX + newY * newY);
			else
				r = sqrt(newY * newY + newZ * newZ);

			tooClose = false;
			outside = false;

			//Loop over neighbouring zones
			for (int neighZoneRowIndex = 0; neighZoneRowIndex < nNeighZones; neighZoneRowIndex++)
			{
				int zzFlatIndex = m_zzStartIndex[currentZone] + neighZoneRowIndex;
				int zIndexGlobal = m_zzTopFlat[zzFlatIndex];

				int nParticlesInNeighZone = m_zpRowLength[zIndexGlobal];

				//Loop over the particles in the neighbouring zone
				for (int pRowIndexInNeighZone = 0; pRowIndexInNeighZone < nParticlesInNeighZone; pRowIndexInNeighZone++)
				{
					int zpFlatIndexNZ = m_zpStartIndex[zIndexGlobal] + pRowIndexInNeighZone;
					int pIndexGlobalNZ = m_zpTopFlat[zpFlatIndexNZ];

					//Avoid checking the particle against itself
					if (pIndexGlobal != pIndexGlobalNZ)
					{
						Vertex pNext = m_vertices[pIndexGlobalNZ];
						float massSizeNext = m_pMassSize[pIndexGlobalNZ];

						float xDiff = newX - pNext.Position.x;
						float yDiff = newY - pNext.Position.y;
						float zDiff = newZ - pNext.Position.z;

						float d = std::sqrt(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);

						float overlap = (massSize + massSizeNext) - d;

						float overlapLimit = overlapSF * massSize;

						if (overlap > overlapLimit) //Allowing a little overlap.
						{
							tooClose = true;
							break;
						}
					}
				}//End neigh particle loop

				if (tooClose)
				{
					tooCloseCounter++;
					break;
				}

			}//End zone loop

			if (!tooClose)
			{
				if (m_simType == simulationType::Cylinder)
				{
					if (newZ < (-m_ms.bbSize.z / 2.0f) || newZ >(m_ms.bbSize.z / 2.0f) || r > m_ms.radius)
						outside = true;
				}
				else if (m_simType == simulationType::SplitCylinder)
				{
					if (newX < (-m_ms.bbSize.x / 2.0f) || newX >(m_ms.bbSize.x / 2.0f) || r > m_ms.radius)
						outside = true;
				}
				else if (m_simType == simulationType::ModulusOfRupture)
				{
					if (newX < (-m_ms.bbSize.x / 2.0f) || newX >(m_ms.bbSize.x / 2.0f) || newY < (-m_ms.bbSize.y / 2.0f) || newY >(m_ms.bbSize.y / 2.0f) || newZ < (-m_ms.bbSize.z / 2.0f) || newZ >(m_ms.bbSize.z / 2.0f))
						outside = true;
				}
				else if (m_simType == simulationType::DirectTension)
				{ 
					if (!InsideDirectTension(r, newZ, m_ms.radius) || newZ < (-m_ms.bbSize.z / 2.0f) || newZ > (m_ms.bbSize.z / 2.0f))
						outside = true;
				}
				else if (m_simType == simulationType::SteelTest)
				{
					if (!InsideSteelTest2(r, newZ) || newZ < (-m_ms.bbSize.z / 2.0f) || newZ >(m_ms.bbSize.z / 2.0f))
						outside = true;
				}
			}

			if (!tooClose && !outside)
			{
				m_vertices[i].Position.x = newX;
				m_vertices[i].Position.y = newY;
				m_vertices[i].Position.z = newZ;
				shakeCounter++;
			}

			attemptsCounter++;

		}//End particle loop

		//std::cout<< "shake/attempts: " << ((float)shakeCounter)/((float)attemptsCounter) << std::endl;
	}

	void TestCylinder::ShakeParticlesIteration(int n)
	{
		std::cout<< "Random seed: " << m_ms.rndSeed << std::endl;
		int counter = 0;
		int tenCounter = 0;
		for (int i = 0; i < n; i++)
		{
			ShakeParticles(i+ m_ms.rndSeed);
			counter++;

			if (counter % 10 == 0)
			{
				tenCounter++;
				counter = 0;
				std::cout << "Completed " << tenCounter * 10 << " nr iterations" << endl;
			}
		}
	}

	void TestCylinder::CreateArms()
	{
		Vertex v, vNext;
		float d = 0;
		int vIndex, vNextIndex;
		int pCount = 0;
		int addCount = 0;
		float xDiff, yDiff, zDiff;
		float bondExists;
		int existingNextParticleIndex;
		int existingArmFlatIndex;
		m_armIndexCounter = 0;
		m_armCounter = 0;
		m_ppCounter = 0;
		m_paCounter = 0;
		float horizonSquared = 0;//m_Horizon * m_Horizon;

		for (int i = 0; i < m_nParticles; i++)
		{
			vector<int> ppi;
			m_ppTop.push_back(ppi);
			vector<int> pai;
			m_paTop.push_back(pai);
		}

		//Loop over particles
		for (int i = 0; i < m_nParticles; i++)
		{
			int currentZone = m_pzTopFlat[i];
			vIndex = i;
			v = m_vertices[vIndex];

			if (i == 10963)
			{
				int a = 10;
				float mS = m_pMassSize[i];
			}

			float horizon = m_pMassSize[i] * m_alpha;

			//Loop over neighbouring zones
			for (int j = 0; j < m_zzTop[currentZone].size(); j++)
			{
				int neighbZone = m_zzTop[currentZone][j];

				//Loop over particles in neighbouring zone
				for (int k = 0; k < m_zpTop[neighbZone].size(); k++)
				{
					vNextIndex = m_zpTop[neighbZone][k];
					vNext = m_vertices[vNextIndex];

					xDiff = vNext.Position.x - v.Position.x;
					yDiff = vNext.Position.y - v.Position.y;
					zDiff = vNext.Position.z - v.Position.z;

					//Square the distances to avoid square root computation
					float d = sqrt(xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);

					//Create a line
					if (d < horizon && vIndex != vNextIndex)
					{
					    existingNextParticleIndex = FindNextParticleIndex(vIndex, vNextIndex);
						existingArmFlatIndex = FindExistingArmFlatIndex(vIndex, vNextIndex);

						if(existingArmFlatIndex == -1) //No existing arm found
						{
							Vertex v1Copy, v2Copy;
							v1Copy.Position = v.Position;
							v1Copy.Color = v.Color;
							v2Copy.Color = v.Color;
							v2Copy.Position = vNext.Position;

							ArmIndices ai = { vIndex, vNextIndex, m_armCounter};
							m_paTop[vIndex].push_back(m_armCounter);
							m_armi[m_armCounter] = ai;

							m_ppTop[vIndex].push_back(vNextIndex);

							m_arms[m_armIndexCounter] = vIndex;
							m_arms2[m_armIndexCounter] = m_armIndexCounter;
							m_vertices2[m_armIndexCounter] = v1Copy;
							m_armIndexCounter++;

							m_arms[m_armIndexCounter] = vNextIndex;
							m_arms2[m_armIndexCounter] = m_armIndexCounter;
							m_vertices2[m_armIndexCounter] = v2Copy;
							m_armIndexCounter++;

							m_armCounter++;
							m_ppCounter++;
							m_paCounter++;
						}
						else
						{
							m_paTop[vIndex].push_back(existingArmFlatIndex);
							m_ppTop[vIndex].push_back(existingNextParticleIndex);
							m_ppCounter++;
							m_paCounter++;
						}
					}
				}
			}
		}


		std::cout << "Create bonds completed" << std::endl;
	}

	bool TestCylinder::DoesBondExist(int vIndex, int vNextIndex)
	{
		for (int i = 0; i < m_ppTop[vNextIndex].size(); i++)
		{
			if (m_ppTop[vNextIndex][i] == vIndex)
				return true;
		}
		return false;
	}

	int TestCylinder::FindNextParticleIndex(int vIndex, int vNextIndex)
	{
		for (int i = 0; i < m_ppTop[vNextIndex].size(); i++)
		{
			if (m_ppTop[vNextIndex][i] == vIndex)
				return m_ppTop[vNextIndex][i];
		}
		return -1;
	}

	int TestCylinder::FindExistingArmFlatIndex(int vIndex, int vNextIndex)
	{
		//Loop through arms connected to the first index
		for (int i = 0; i < m_paTop[vIndex].size(); i++)
		{
			int armIndex = m_paTop[vIndex][i];

			if (m_armi[armIndex].p1 == vIndex && m_armi[armIndex].p2 == vNextIndex || m_armi[armIndex].p1 == vNextIndex && m_armi[armIndex].p2 == vIndex)
				return armIndex;
		}
		//Loop through arms connected to the second index
		for (int i = 0; i < m_paTop[vNextIndex].size(); i++)
		{
			int armIndex = m_paTop[vNextIndex][i];

			if (m_armi[armIndex].p1 == vIndex && m_armi[armIndex].p2 == vNextIndex || m_armi[armIndex].p1 == vNextIndex && m_armi[armIndex].p2 == vIndex)
				return armIndex;
		}

		//No existing arm found
		return -1;
	}

	void TestCylinder::GenFlatTopology1()
	{
		//Cuda initialisation functions 
		m_zzTopFlat = new int[m_zzCounter];
		m_zzStartIndex = new int[m_nZones];
		m_zzRowLength = new int[m_nZones];

		m_zpTopFlat = new int[m_nParticles];
		m_zpStartIndex = new int[m_nZones];
		m_zpRowLength = new int[m_nZones];


		int counter = 0;
		int rowLength;

		for (int i = 0; i < m_zpTop.size(); i++)
		{
			rowLength = 0;
			for (int j = 0; j < m_zpTop[i].size(); j++)
			{
				if (j == 0)
				{
					m_zpStartIndex[i] = counter;
				}
				m_zpTopFlat[counter] = m_zpTop[i][j];
				m_zpTopFlatV.push_back(m_zpTop[i][j]);
				counter++;
				rowLength++;
			}
			m_zpRowLength[i] = rowLength;
		}

		int counter2 = 0;
		int rowLength2;

		for (int i = 0; i < m_zzTop.size(); i++)
		{
			rowLength2 = 0;
			for (int j = 0; j < m_zzTop[i].size(); j++)
			{
				if (j == 0)
				{
					m_zzStartIndex[i] = counter2;
				}
				m_zzTopFlat[counter2] = m_zzTop[i][j];
				m_zzTopFlatV.push_back(m_zzTop[i][j]);
				counter2++;
				rowLength2++;
			}
			m_zzRowLength[i] = rowLength2;
		}

		std::cout << "Flat topology 1 completed" << std::endl;
	}

	void TestCylinder::GenFlatTopology2()
	{
		m_ppTopFlat = new int[m_ppCounter];
		m_ppStartIndex = new int[m_nParticles];
		m_ppRowLength = new int[m_nParticles];

		int counter = 0;
		int rowLength;

		for (int i = 0; i < m_ppTop.size(); i++)
		{
			rowLength = 0;
			for (int j = 0; j < m_ppTop[i].size(); j++)
			{
				if (j == 0)
				{
					m_ppStartIndex[i] = counter;
				}
				m_ppTopFlat[counter] = m_ppTop[i][j];
				counter++;
				rowLength++;
			}
			m_ppRowLength[i] = rowLength;
		}


		m_paTopFlat = new int[m_paCounter];
		m_paStartIndex = new int[m_nParticles];
		m_paRowLength = new int[m_nParticles];

		counter = 0;
		rowLength = 0;

		for (int i = 0; i < m_paTop.size(); i++)
		{
			rowLength = 0;
			for (int j = 0; j < m_paTop[i].size(); j++)
			{
				if (j == 0)
				{
					m_paStartIndex[i] = counter;
				}
				m_paTopFlat[counter] = m_paTop[i][j];
				counter++;
				rowLength++;
			}
			m_paRowLength[i] = rowLength;
		}


		std::cout << "Flat topology 2 completed" << std::endl;
	}

	void TestCylinder::LaplacianCPU()
	{
		for (int i = 0; i < m_nParticles; i++)
		{
			int startIndex = m_ppStartIndex[i];
			int rowLength = m_ppRowLength[i];
			int n = rowLength;

			//Get position from current vertex
			float xavrg = m_vertices[i].Position.x;
			float yavrg = m_vertices[i].Position.y;
			float zavrg = m_vertices[i].Position.z;

			float sf = 0.001;

			for (int j = 0; j < rowLength; j++)
			{
				int flatIndex = startIndex + j;
				int pGlobalIndex = m_ppTopFlat[flatIndex];
				xavrg += sf * (m_vertices[pGlobalIndex].Position.x / n);
				yavrg += sf * (m_vertices[pGlobalIndex].Position.y / n);
				zavrg += sf * (m_vertices[pGlobalIndex].Position.z / n);
			}

			//Update position for the copy of the current vertex
			m_vertices[i].Position.x = xavrg;
			m_vertices[i].Position.y = yavrg;
			m_vertices[i].Position.z = zavrg;
		}
	}

	void TestCylinder::LaplacianGPU()
	{
		int n = 10;

		m_ffCuda.CopyFromHostToDevice_Particles(m_vertices);

		for (int i = 0; i < n; i++)
		{
			m_ffCuda.ExecuteLaplacian();
			m_ffCuda.SwapArrays_Vertices();
		}

		m_ffCuda.CopyFromDeviceToHost_Particles(m_vertices);
	}

	void TestCylinder::ComputeStats()
	{
		int pCount = 0;

		for (int i = 0; i < m_nZones; i++)
		{
			if (m_zpTop[i].size() > 0)
			{
				m_Stats.nUsedZones++;
				pCount += m_zpTop[i].size();
			}
		}

		m_Stats.nParticles = m_nParticles;
		m_Stats.nParticlesPerZone = pCount / m_Stats.nUsedZones;
		m_Stats.nZones = m_nZones;

		pCount = 0;

		for (int i = 0; i < m_ppTop.size(); i++)
		{
			pCount += m_ppTop[i].size();
		}

		float largestHa = 0;
		for (int i = 0; i < m_nParticles; i++)
		{
			if (m_pHaSize[i] > largestHa)
				largestHa = m_pHaSize[i];
		}

		m_Stats.nBonds = pCount;
		m_Stats.nBondsPerParticle = pCount / m_ppTop.size();

		float smallestZoneDim = 1e10;
		if (m_ms.zoneStep.x < smallestZoneDim) smallestZoneDim = m_ms.zoneStep.x;
		if (m_ms.zoneStep.y < smallestZoneDim) smallestZoneDim = m_ms.zoneStep.y;
		if (m_ms.zoneStep.z < smallestZoneDim) smallestZoneDim = m_ms.zoneStep.z;

		m_Stats.zoneSizeParticleSizeRatio = smallestZoneDim / largestHa;

		cout << "particle count:" << m_Stats.nParticles << endl;
		cout << "bond count:" << m_Stats.nBonds << endl;
		cout << "zone count:" << m_Stats.nZones << endl;
		cout << "used zone count:" << m_Stats.nUsedZones << endl;
		cout << "average number of neighbours per particle:" << m_Stats.nBondsPerParticle << endl;
		cout << "average number of particles per zone:" << m_Stats.nParticlesPerZone << endl;
		cout << "zone size (x,y,z):" << m_ms.zoneStep.x << ", " << m_ms.zoneStep.y << ", " << m_ms.zoneStep.z << endl;
		cout << "zone over horizon (should be greater than 1!): " << m_Stats.zoneSizeParticleSizeRatio << endl;

	}

	void TestCylinder::CalcBaseSize(int nIterations, int nNeighbours)
	{
		int counter = 0;

		while (counter < nIterations)
		{
			IncrementParticleSize1D(nNeighbours);
			counter++;
		}

		UpdateDiscSize();
		UpdateIcosaSize();

		std::cout << nIterations << " size iterations completed" << std::endl;
	}

	void TestCylinder::CalcMassSize()
	{
		float volTot = 0;
		float pi = 3.1415;
		float volTarget = m_ms.radius * m_ms.radius * pi * m_ms.height;
		float* pVol = new float[m_nParticles];

		if (m_simType == simulationType::SteelTest)
			volTarget = 3.0430e-06;


		for (int i = 0; i < m_nParticles; i++)
		{
			pVol[i] = (4.0f / 3.0f) * pi * m_pBaseSize[i] * m_pBaseSize[i] * m_pBaseSize[i];
			volTot += pVol[i];
		}

		float volumeSf = volTarget / volTot;
		std::cout << "Target Volume: " << volTarget << std::endl;
		std::cout << "Based Volume: " << volTot << std::endl;
		volTot = 0;

		for (int i = 0; i < m_nParticles; i++)
		{
			float radiusSf = pow(volumeSf, (1.0f / 3.0f));
			m_pMassSize[i] = radiusSf * m_pBaseSize[i];
			float volP = (4.0f / 3.0f) * pi * m_pMassSize[i] * m_pMassSize[i] * m_pMassSize[i];
			volTot += volP;
		}

		std::cout << "Mass Volume: " << volTot << std::endl;
		delete[] pVol;
	}

	void TestCylinder::CalcHaSize(float alpha)
	{
		for (int i = 0; i < m_nParticles; i++)
		{
			m_pHaSize[i] = alpha * m_pMassSize[i];
		}
	}

	void TestCylinder::IncrementSizeGPU()
	{
		//GPU execusion
		if (m_IncrementSizeGPU)
			m_IncrementSizeCounterGPU = 0;

		if (m_IncrementSizeCounterGPU < 1)
		{
			int n = 10;
			m_ffCuda.CopyFromHostToDevice_Ha(m_pBaseSize);
			m_ffCuda.CopyFromHostToDevice_FriendsCount(m_pCurrentFriendsCount);

			for (int i = 0; i < n; i++)
			{
				m_ffCuda.IncrementSize();
				m_ffCuda.SwapArrays_Ha();
				m_ffCuda.SwapArrays_Friends();
				m_ffCuda.CopyFromDeviceToHost_Ha(m_pBaseSize);
				m_ffCuda.CopyFromDeviceToHost_FriendsCount(m_pCurrentFriendsCount);

				float averageSize = 0;
				int allFriends = 0;

				for (int j = 0; j < m_nParticles; j++)
				{
					averageSize += m_pBaseSize[j] / m_nParticles;
					allFriends += m_pCurrentFriendsCount[i];
				}

				std::cout << "Average size GPU: " << averageSize << std::endl;
				std::cout << "All particle friends GPU: " << allFriends << std::endl;
				std::cout << "Average particle friends GPU: " << allFriends / m_nParticles << std::endl;
			}

			UpdateDiscSize();
			UpdateIcosaSize();
			m_IncrementSizeCounterGPU = 1;
		}
	}

	void TestCylinder::CopyPositions()
	{
		float ix, iy, iz, dx, dy, dz;
		float sf = m_scaleFactor;

		if (m_drawParticles || m_drawBonds)
		{
			for (int i = 0; i < m_nParticles; i++)
			{
				ix = m_structure.m_particles[i].iPosition.x;
				iy = m_structure.m_particles[i].iPosition.y;
				iz = m_structure.m_particles[i].iPosition.z;

				dx = m_structure.m_particles[i].cPosition.x - ix;
				dy = m_structure.m_particles[i].cPosition.y - iy;
				dz = m_structure.m_particles[i].cPosition.z - iz;

				//Scaling the position
				m_vertices[i].Position.x = ix + sf * dx;
				m_vertices[i].Position.y = iy + sf * dy;
				m_vertices[i].Position.z = iz + sf * dz;
			}
		}

		if (m_drawBonds2)
		{
			int armIndexCounter = 0;

			for (int i = 0; i < m_structure.m_arms.size(); i++)
			{
				int i1 = m_structure.m_arms[i].p1;
				int i2 = m_structure.m_arms[i].p2;

				ix = m_structure.m_particles[i1].iPosition.x;
				iy = m_structure.m_particles[i1].iPosition.y;
				iz = m_structure.m_particles[i1].iPosition.z;

				dx = m_structure.m_particles[i1].cPosition.x - ix;
				dy = m_structure.m_particles[i1].cPosition.y - iy;
				dz = m_structure.m_particles[i1].cPosition.z - iz;

				//Scaling the position
				m_vertices2[armIndexCounter].Position.x = ix + sf * dx;
				m_vertices2[armIndexCounter].Position.y = iy + sf * dy;
				m_vertices2[armIndexCounter].Position.z = iz + sf * dz;

				armIndexCounter++;

				ix = m_structure.m_particles[i2].iPosition.x;
				iy = m_structure.m_particles[i2].iPosition.y;
				iz = m_structure.m_particles[i2].iPosition.z;

				dx = m_structure.m_particles[i2].cPosition.x - ix;
				dy = m_structure.m_particles[i2].cPosition.y - iy;
				dz = m_structure.m_particles[i2].cPosition.z - iz;

				//Scaling the position
				m_vertices2[armIndexCounter].Position.x = ix + sf * dx;
				m_vertices2[armIndexCounter].Position.y = iy + sf * dy;
				m_vertices2[armIndexCounter].Position.z = iz + sf * dz;

				armIndexCounter++;
			}
		}
	}

	void TestCylinder::ColorParticles()
	{
		float min = 1e20, max = -1e20;
		float* values = new float[m_nParticles];

		for (int i = 0; i < m_nParticles; i++)
		{
			float value = 0.0f;

			if (m_colorParticleBy == colorParticleBy::velocity)
				value = GetVectorLength(m_structure.m_particles[i].Veloc);
			else if (m_colorParticleBy == colorParticleBy::acceleration)
				value = GetVectorLength(m_structure.m_particles[i].Accel);
			else if (m_colorParticleBy == colorParticleBy::forceArm)
				value = GetVectorLength(m_structure.m_particles[i].ArmForce);
			else if (m_colorParticleBy == colorParticleBy::forceExt)
				value = GetVectorLength(m_structure.m_particles[i].ExtForce);
			else if (m_colorParticleBy == colorParticleBy::meanStrain)
				value = m_structure.m_particles[i].meanStrain;
			else if (m_colorParticleBy == colorParticleBy::forceTot)
				value = GetAddedVectorLength(m_structure.m_particles[i].ArmForce, m_structure.m_particles[i].ExtForce);
			else if (m_colorParticleBy == colorParticleBy::displacement)
				value = Distance(m_structure.m_particles[i].cPosition, m_structure.m_particles[i].iPosition);
			else if (m_colorParticleBy == colorParticleBy::damage)
				value = m_structure.m_particles[i].damage;
			else if (m_colorParticleBy == colorParticleBy::isBC)
				value = (float)m_structure.m_particles[i].isBC;

			if (value < min)
				min = value;

			if (value > max)
				max = value;

			if (m_colorParticleBy == colorParticleBy::damage) 
			{
				min = 0.0f;
				max = 0.5f;
			}

			values[i] = value;
		}

		float perc;

		for (int i = 0; i < m_nParticles; i++)
		{
			Vec4 color = GetBlendedColor(min, max, values[i]);
			m_vertices[i].Color = color;
		}

		delete[] values;
	}

	void TestCylinder::ColorArms()
	{
		float min = 1e20, max = -1e20, colorMax;
		float value = 0.0f;
		float* values = new float[m_structure.m_arms.size()];

		for (int i = 0; i < m_structure.m_arms.size(); i++)
		{
			if (m_colorArmBy == colorArmBy::force || m_colorArmBy == colorArmBy::forceComTen)
				value = m_structure.m_arms[i].fMag;
			else if (m_colorArmBy == colorArmBy::strain)
				value = m_structure.m_arms[i].strain;
			else if (m_colorArmBy == colorArmBy::plasticStrain)
				value = m_structure.m_arms[i].plasticStrain;
			else if (m_colorArmBy == colorArmBy::broken)
				value = (float)m_structure.m_arms[i].broken;
			else if (m_colorArmBy == colorArmBy::elongationToFracture)
				value = (float)m_structure.m_arms[i].elongation;
			else if (m_colorArmBy == colorArmBy::nonFailureZone)
				value = (float)m_structure.m_arms[i].insideNonFailZone;


			if (value < min)
				min = value;

			if (value > max)
				max = value;

			values[i] = value;
		}

		//Limits for strain
		if (m_colorArmBy == colorArmBy::plasticStrain || m_colorArmBy == colorArmBy::strain)
		{
			min = -m_structure.m_m.yieldStrain;
			max = m_structure.m_m.yieldStrain;
		}
		else if (m_colorArmBy == colorArmBy::elongationToFracture)
		{
			min = 0;
			max = m_structure.m_m.elongationLimit;
		}

		int armIndexCounter = 0;

		for (int i = 0; i < m_structure.m_arms.size(); i++)
		{
			float num = values[i];
			Vec4 color;
			if (m_colorArmBy == colorArmBy::forceComTen)
				color = GetBlendedColorBlueRed(min, max, num);
			else if (m_colorArmBy == colorArmBy::strain)
				color = GetBlendedColorGreenZero(min, max, num);
			else if (m_colorArmBy == colorArmBy::broken)
			{
				if (m_structure.m_arms[i].broken)
					color = { 1.0f, 1.0f, 1.0f, 1.0f };
				else
					color = { 1.0f, 1.0f, 1.0f, 0.0f };
			}
			else if (m_colorArmBy == colorArmBy::nonFailureZone)
			{
				if (m_structure.m_arms[i].insideNonFailZone)
					color = { 0.5f, 1.0f, 0.5f, 1.0f };
				else
					color = { 1.0f, 0.5f, 0.5f, 1.0f };
			}
			else if (m_colorArmBy == colorArmBy::elongationToFracture)
			{
				if (num < 0.0f)
					num = 0.0f;		//Ignore compressed elements

				color = GetBlendedColor(min, max, num);
			}
			else
				color = GetBlendedColor(min, max, num);

			m_vertices2[armIndexCounter].Color = color;
			armIndexCounter++;
			m_vertices2[armIndexCounter].Color = color;
			armIndexCounter++;
		}

		delete[] values;
	}

	void TestCylinder::RunAnalysis()
	{
		if (m_runAnalysis)
		{
			m_structure.RunAnalysis();
			CopyPositions();

			if (m_drawParticles)
				ColorParticles();

			if (m_drawBonds2)
				ColorArms();

			m_runCounter++;
		}
	}

	void TestCylinder::RunShaking()
	{
		if (m_runShaking)
		{
			ShakeParticles(m_shakeCounter);

			m_shakeCounter++;
			std::cout << "Shaking iteration:" << m_shakeCounter << " done" << std::endl;

			if (m_shakeCounter == 100)
			{
				m_runShaking = false;
				m_shakeCounter = 0;
			}
		}
	}

	void TestCylinder::RunOutput()
	{
		if (m_btnRunOutput || m_runCounter == 2000)
		{
			m_structure.OutputResults();
		}
	}

	void TestCylinder::RunExport()
	{
		if (m_btnRunExport)
		{
			ExportModel();
		}
	}

	void TestCylinder::RunXray()
	{
		if (m_XrayYZ)
		{
			float step = m_ms.radius / 100;

			if (m_clipDistance1 < -m_ms.radius)
				m_XrayDir = 1;
			else if (m_clipDistance1 > m_ms.radius)
				m_XrayDir = -1;

			m_clipDistance1 += m_XrayDir * step;
		}

		if (m_XrayXY)
		{
			float step = m_ms.height / 250;

			if (m_clipDistance3 < -m_ms.height / 2.0f)
				m_XrayDir2 = 1;
			else if (m_clipDistance3 > m_ms.height / 2.0f)
				m_XrayDir2 = -1;

			m_clipDistance3 += m_XrayDir2 * step;
		}
	}

	void TestCylinder::RunDefAni()
	{
		if (m_AniDeformation)
		{
			float step = 100.0f / 75.0f;

			if (m_scaleFactor < 0.0f)
				m_AniDefDir = 1;
			else if (m_scaleFactor > 10.0f)
				m_AniDefDir = -1;

			m_scaleFactor += m_AniDefDir * step;
		}
	}


	//----------------------------Drawing-----------------------------//
	void TestCylinder::OnUpdate()
	{
		if (m_btnRunAnalysis)
		{
			m_startTimeAnalysis = std::chrono::high_resolution_clock::now();
			m_runAnalysis = !m_runAnalysis;
		}

		if (m_btnRunShaking)
			m_runShaking = !m_runShaking;

		if (m_btnRunOutput || m_runCounter == 2000)
			RunOutput();

		if (m_btnRunExport)
			RunExport();

		if (m_btnXrayYZ)
			m_XrayYZ = !m_XrayYZ;

		if (m_btnXrayXY)
			m_XrayXY = !m_XrayXY;

		if (m_btnAniDeformation)
			m_AniDeformation = !m_AniDeformation;

		RunDefAni();

		RunXray();

		RunShaking();

		RunAnalysis();

		m_nowTimeAnalysis = std::chrono::high_resolution_clock::now();
		m_durationAnalysis = m_nowTimeAnalysis - m_startTimeAnalysis;

		//OpenGL 
		{
			if (m_drawParticles)
			{
				GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_pIndexVB));
				GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles, m_vertices));
			}
			if (m_drawZones)
			{
				GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_zLineIndexVB));
				GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_zVertexCounter, m_zoneVertices1D));
			}
			if (m_drawBonds)
			{
				GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB));
				GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles, m_vertices));
			}
			if (m_drawBonds2)
			{
				GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB2));
				GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_armIndexCounter, m_vertices2));
			}
			if (m_drawDisc)
			{
				UpdateDiscPosition();
				GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_discIndexVB));
				GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles * (m_nDiscSides + 1), m_discVertices));
			}
			if (m_drawIcosa)
			{
				UpdateIcosaPosition();
				GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_icosaIndexVB));
				GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles * 12, m_icosaVertices));
			}

			if (m_drawCylinders && m_simType == simulationType::ModulusOfRupture)
			{	
				GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_cylinderIndexVB));
				GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nCylinderTriangles * 2 * 4, m_cylinderVertices));
			}
		}

		GLCall(glClear(GL_COLOR_BUFFER_BIT));
		GLCall(glClear(GL_DEPTH_BUFFER_BIT));

		m_Shader->Bind();

		float oW = (16.0f / 9.0f) * m_orthoSF;
		float oH = 1.0f * m_orthoSF;

		if(m_viewType == viewType::perspective)
			m_Proj = glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f);
		else
			m_Proj = glm::ortho(-1.0f * oW, 1.0f * oW, -1.0f * oH, 1.0f * oH, 0.01f, 100.0f);
		
		m_View = glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up);
		m_Trans = glm::mat4(1.0f);
		m_Rot = glm::mat4(1.0f);
		m_Model = m_Trans * m_Rot;
		glm::mat4 mvp = m_Proj * m_View * m_Model;

		m_Shader->SetUniformMat4f("u_Model", m_Model);
		m_Shader->SetUniformMat4f("u_View", m_View);
		m_Shader->SetUniformMat4f("u_Proj", m_Proj);
		m_Shader->SetUniform1f("u_clipDistance1", m_clipDistance1);
		m_Shader->SetUniform1f("u_clipDistance2", m_clipDistance2);
		m_Shader->SetUniform1f("u_clipDistance3", m_clipDistance3);
		m_Shader->SetUniform1f("u_clipDistance4", m_clipDistance4);

		//Enable and disable clipping plane
		if (m_enableClipping)
		{
			glEnable(GL_CLIP_DISTANCE0);
			glEnable(GL_CLIP_DISTANCE1);
			glEnable(GL_CLIP_DISTANCE2);
			glEnable(GL_CLIP_DISTANCE3);
		}
		else
		{
			glDisable(GL_CLIP_DISTANCE0);
			glDisable(GL_CLIP_DISTANCE1);
			glDisable(GL_CLIP_DISTANCE2);
			glDisable(GL_CLIP_DISTANCE3);
		}

	}

	void TestCylinder::OnRender()
	{
		//Draw lines
		if (m_drawZones)
		{
			GLCall(glBindVertexArray(m_zLineIndexVA));
			GLCall(glDrawElements(GL_LINES, m_zLineIndexCounter, GL_UNSIGNED_INT, nullptr));
		}

		//Draw lines
		if (m_drawBonds)
		{
			GLCall(glBindVertexArray(m_bLineIndexVA));
			GLCall(glDrawElements(GL_LINES, m_armIndexCounter, GL_UNSIGNED_INT, nullptr));
		}

		//Draw lines 2
		if (m_drawBonds2)
		{
			GLCall(glBindVertexArray(m_bLineIndexVA2));
			GLCall(glDrawElements(GL_LINES, m_armIndexCounter, GL_UNSIGNED_INT, nullptr));
		}

		//Draw disc
		if (m_drawDisc)
		{
			GLCall(glBindVertexArray(m_discIndexVA));
			GLCall(glDrawElements(GL_TRIANGLES, m_nParticles * m_nDiscSides * 3, GL_UNSIGNED_INT, nullptr));
		}

		//Draw points
		if (m_drawParticles)
		{
			GLCall(glBindVertexArray(m_pIndexVA));
			GLCall(glDrawElements(GL_POINTS, m_nParticles, GL_UNSIGNED_INT, nullptr));
		}

		//Draw icosahedron
		if (m_drawIcosa)
		{
			GLCall(glBindVertexArray(m_icosaIndexVA));
			GLCall(glDrawElements(GL_TRIANGLES, m_nParticles * 20 * 3, GL_UNSIGNED_INT, nullptr));
		}

		//Draw cylinders
		if (m_drawCylinders && m_simType == simulationType::ModulusOfRupture)
		{
			GLCall(glBindVertexArray(m_cylinderIndexVA));
			GLCall(glDrawElements(GL_TRIANGLES, (m_nCylinderTriangles -1) * 2 * 3 * 4, GL_UNSIGNED_INT, nullptr));
		}

	}

	//----------------------------------------------------------------//

	//UI and dynamic moving
	void TestCylinder::OnImGuiRenderer()
	{
		float min =  floor(m_durationAnalysis.count() / 60.0f);
		float sec = (float)(((int)m_durationAnalysis.count()) % 60);

		ImGui::TextColored(ImVec4(1, 1, 0, 1), "CLIPPING PLANES:");
		ImGui::Checkbox("Enable clipping", &m_enableClipping);
		m_btnXrayYZ = ImGui::Button("X-Ray Loop YZ-plane");
		m_btnXrayXY = ImGui::Button("X-Ray Loop XY-plane");
		ImGui::SliderFloat("YZ front", &m_clipDistance1, -0.100, 0.100);
		ImGui::SliderFloat("YZ back", &m_clipDistance2, -0.100, 0.100);
		ImGui::SliderFloat("XY top", &m_clipDistance3, -0.250, 0.250);
		ImGui::SliderFloat("XY bot", &m_clipDistance4, -0.250, 0.250);

		ImGui::TextColored(ImVec4(1, 1, 0, 1), "PERFORMANCE:");
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::Text("Iteration counter %.1f", (float)m_structure.m_disp.stepCounter);
		ImGui::Text("Time elapsed: %.0f:%.0f", min, sec);

		ImGui::TextColored(ImVec4(1, 1, 0, 1), "DRAWING OPTIONS:");
		ImGui::Checkbox("Particles", &m_drawParticles);
		ImGui::Checkbox("Zones", &m_drawZones);
		ImGui::Checkbox("Arms P", &m_drawBonds);
		ImGui::Checkbox("Arms A", &m_drawBonds2);
		ImGui::Checkbox("Disc", &m_drawDisc);
		ImGui::Checkbox("Solid", &m_drawIcosa);
		ImGui::Checkbox("Cylinders", &m_drawCylinders);

		ImGui::TextColored(ImVec4(1, 1, 0, 1), "DEFORMATION SCALE:");
		m_btnAniDeformation = ImGui::Button("Animate Deformation");
		ImGui::SliderFloat("", &m_scaleFactor, 1.0f, 40.0f);

		ImGui::TextColored(ImVec4(1, 1, 0, 1), "COLOR PARTICLES BY:");
		const char* itemsP[] = {  "displacement", "velocity", "acceleration", "meanStrain", "forceArm", "forceExt", "forceTot", "damage", "isBC" };
		ImGui::Combo(" ", &m_selectedItemParticleColorBy, itemsP, IM_ARRAYSIZE(itemsP));
		m_colorParticleBy = (colorParticleBy)m_selectedItemParticleColorBy;

		ImGui::TextColored(ImVec4(1, 1, 0, 1), "COLOR ARMS BY:");
		const char* itemsA[] = { "elongationToFracture", "strain","plasticStrain","force","forceComTen","broken", "nonFailureZone" };
		ImGui::Combo("  ", &m_selectedItemArmColorBy, itemsA, IM_ARRAYSIZE(itemsA));
		m_colorArmBy = (colorArmBy)m_selectedItemArmColorBy;

		ImGui::TextColored(ImVec4(1, 1, 0, 1), "Action:");
		m_btnRunShaking = ImGui::Button("Run Shaking");
		m_btnRunAnalysis = ImGui::Button("Run Analysis");
		m_btnRunOutput = ImGui::Button("Output results");
		m_btnRunExport = ImGui::Button("Export Model");
	}

	void TestCylinder::UpdatePositions()
	{
		int counter = 0;
		int quadCounter = 0;
		float pSpeed = 0.001;

		for (int i = 0; i < m_nParticles * 4; i += 4)
		{
			float xRnd = pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);
			float yRnd = pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);
			float zRnd = 0; //m_pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);

			m_vertices[quadCounter].Position.x += xRnd;
			m_vertices[quadCounter].Position.y += yRnd;
			m_vertices[quadCounter].Position.z += zRnd;

			quadCounter++;
		}
	}

	void TestCylinder::UpdateDiscPosition()
	{
		int pIndex = 0;

		for (int i = 0; i < m_nParticles; i++)
		{
			float diffX = m_vertices[i].Position.x - m_discVertices[pIndex].Position.x;
			float diffY = m_vertices[i].Position.y - m_discVertices[pIndex].Position.y;
			float diffZ = m_vertices[i].Position.z - m_discVertices[pIndex].Position.z;

			for (int j = 0; j <= m_nDiscSides; j++)
			{
				m_discVertices[pIndex + j].Position.x += diffX;
				m_discVertices[pIndex + j].Position.y += diffY;
				m_discVertices[pIndex + j].Position.z += diffZ;
			}

			pIndex += m_nDiscSides + 1;
		}
	}

	void TestCylinder::UpdateDiscSize()
	{
		int pIndex = 0;
		float smallestHa = 1e10;
		float largestHa = -1e10;

		for (int i = 0; i < m_nParticles; i++)
		{
			if (m_pMassSize[i] > largestHa)
				largestHa = m_pMassSize[i];

			if (m_pMassSize[i] < smallestHa)
				smallestHa = m_pMassSize[i];
		}

		float haMaxForColor = largestHa - smallestHa;

		for (int i = 0; i < m_nParticles; i++)
		{
			float haForColor = m_pMassSize[i] - smallestHa; //Make sure the size goes from 0 -> upwards for percentage calc.
			float perc = 100 * (haForColor / haMaxForColor);
			Vec4 color = GetBlendedColor(perc);

			float newR = m_pMassSize[i];
			m_discVertices[pIndex].Position.x = m_vertices[i].Position.x;
			m_discVertices[pIndex].Position.y = m_vertices[i].Position.y;
			m_discVertices[pIndex].Position.z = m_vertices[i].Position.z;

			float pX = m_discVertices[pIndex].Position.x;
			float pY = m_discVertices[pIndex].Position.y;

			float oldR = Distance(m_discVertices[pIndex], m_discVertices[pIndex + 1]);

			float sf = newR / oldR;

			for (int j = 1; j <= m_nDiscSides; j++)
			{
				m_discVertices[pIndex + j].Position.x = pX + sf * (m_discVertices[pIndex + j].Position.x - pX);
				m_discVertices[pIndex + j].Position.y = pY + sf * (m_discVertices[pIndex + j].Position.y - pY);
				m_discVertices[pIndex + j].Color = color;
			}

			pIndex += m_nDiscSides + 1;
		}
	}

	void TestCylinder::UpdateIcosaPosition()
	{
		int pIndex = 0;

		for (int i = 0; i < m_nParticles; i++)
		{
			float diffX = m_vertices[i].Position.x - m_icosaCenter[i].Position.x;
			float diffY = m_vertices[i].Position.y - m_icosaCenter[i].Position.y;
			float diffZ = m_vertices[i].Position.z - m_icosaCenter[i].Position.z;

			for (int j = 0; j < 12; j++)
			{
				m_icosaVertices[pIndex + j].Position.x += diffX;
				m_icosaVertices[pIndex + j].Position.y += diffY;
				m_icosaVertices[pIndex + j].Position.z += diffZ;
			}

			m_icosaCenter[i].Position.x = m_vertices[i].Position.x;
			m_icosaCenter[i].Position.y = m_vertices[i].Position.y;
			m_icosaCenter[i].Position.z = m_vertices[i].Position.z;

			pIndex += 12;
		}
	}

	void TestCylinder::UpdateIcosaSize()
	{
		int pIndex = 0;

		for (int i = 0; i < m_nParticles; i++)
		{
			float newR = m_pMassSize[i];
			float pX = m_vertices[i].Position.x;
			float pY = m_vertices[i].Position.y;
			float pZ = m_vertices[i].Position.z;

			float oldR = Distance(m_vertices[i], m_icosaVertices[pIndex]);

			float sf = newR / oldR;

			for (int j = 0; j < 12; j++)
			{
				m_icosaVertices[pIndex + j].Position.x = pX + sf * (m_icosaVertices[pIndex + j].Position.x - pX);
				m_icosaVertices[pIndex + j].Position.y = pY + sf * (m_icosaVertices[pIndex + j].Position.y - pY);
				m_icosaVertices[pIndex + j].Position.z = pZ + sf * (m_icosaVertices[pIndex + j].Position.z - pZ);
			}

			pIndex += 12;
		}
	}

	float TestCylinder::GenRandom(float min, float max)
	{
		float rnd = (float)rand() / (RAND_MAX);
		float diff = max - min;
		float val = min + rnd * diff;
		return val;
	}

	void TestCylinder::ExportModel()
	{
		std::ofstream fileP("output/coord.txt");

		for (int i = 0; i < m_nParticles; i++)
		{
			float x = m_vertices[i].Position.x;
			float y = m_vertices[i].Position.y;
			float z = m_vertices[i].Position.z;
			float r = m_pMassSize[i];

			fileP << x << ',' << y << ',' << z << ',' << r << '\n';
		}
		fileP.close();

		std::ofstream fileC("output/color.txt");

		for (int i = 0; i < m_nParticles; i++)
		{
			float x = m_vertices[i].Color.x;
			float y = m_vertices[i].Color.y;
			float z = m_vertices[i].Color.z;
			float r = m_vertices[i].Color.w;

			fileC << x << ',' << y << ',' << z << ',' << r << '\n';
		}
		fileC.close();

	}

	//Supporting mesh functions
	vector<Vertex> TestCylinder::CreateDisc(float x, float y, float z, float radius, int n, Vec4 color)
	{
		vector<Vertex> vs;

		float a, aStep, cX, cY, cZ;
		float pi = 3.1415;

		a = 0;
		aStep = (pi * 2) / (n - 1);

		Vertex v;
		v.Position = { x, y, z };
		v.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		vs.push_back(v);

		for (int i = 0; i < n; i++)
		{
			cX = x + radius * std::cos(a);
			cY = y + radius * std::sin(a);
			cZ = z;

			Vertex vNext;
			vNext.Position = { cX, cY, cZ };
			vNext.Color = color; //{ 0.18f, 0.6f, 0.96f, 1.0f };

			vs.push_back(vNext);

			a += aStep;
		}

		return vs;
	}

	vector<Vertex> TestCylinder::CreateCylinder(Vec3 position, float radius, int n, float length)
	{
		vector<Vertex> vs;

		float halfLength = length / 2.0f;
		float a, aStep, cX, cY, cZ, x;
		float pi = 3.1415;

		a = 0;
		aStep = (pi * 2) / (n - 1);

		Vertex v;
		Vec4 color = { 0.18f, 0.6f, 0.96f, 1.0f };

		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				cX = position.x + radius * std::sin(a);
				cY = -halfLength + j * length;
				cZ = position.z + radius * std::cos(a);

				Vertex vNext;
				vNext.Position = { cX, cY, cZ };
				vNext.Color = color;
				vs.push_back(vNext);
			}

			a += aStep;
		}

		return vs;
	}

	vector<int> TestCylinder::GetDiscIndices(int startIndex, int n)
	{
		vector<int> indices;

		for (int i = 1; i <= n; i++)
		{
			indices.push_back(startIndex);
			indices.push_back(startIndex + i);

			if (i == n)
				indices.push_back(startIndex + 1);
			else
				indices.push_back(startIndex + 1 + i);
		}

		return indices;
	}

	vector<Vertex> TestCylinder::CreateIcoSphere(float x, float y, float z, float radius, Vec4 color)
	{
		//Predefined coordinates for the 12 vertices of an icosahedron with 1 in radius
		float xIco[12] = { 0, 0.894427, 0.276393, -0.723607, -0.723607, 0.276393, 0.723607, -0.276393, -0.894427, -0.276393, 0.723607, 0.0 };
		float yIco[12] = { 0, 0, 0.850651, 0.525731, -0.525731, -0.850651, 0.525731, 0.850651, 0.0, -0.850651, -0.525731, 0.0 };
		float zIco[12] = { 1, 0.447214, 0.447214, 0.447214, 0.447214, 0.447214, -0.447214, -0.447214, -0.447214, -0.447214, -0.447214, -1.0 };

		vector<Vertex> vs;
		float a, aStep, cX, cY, cZ;

		for (int i = 0; i < 12; i++)
		{
			cX = x + radius * xIco[i];
			cY = y + radius * yIco[i];
			cZ = z + radius * zIco[i];

			Vertex vNext;
			vNext.Position = { cX, cY, cZ };
			vNext.Color = color;
			vs.push_back(vNext);
		}
		return vs;
	}

	vector<int> TestCylinder::GetIcoSphereIndices(int startIndex)
	{
		//Indices for 20 triangles for the predifined icosahedron
		vector<int> indices =
		{ 0,1,2,
			0,2,3,
			0,3,4,
			0,4,5,
			0,5,1,
			1,5,10,
			1,6,2,
			1,10,6,
			2,6,7,
			2,7,3,
			3,7,8,
			3,8,4,
			4,8,9,
			4,9,5,
			5,9,10,
			6,10,11,
			6,11,7,
			7,11,8,
			8,11,9,
			9,11,10
		};

		for (int i = 0; i < indices.size(); i++)
			indices[i] += startIndex;

		return indices;
	}

	vector<int> TestCylinder::GetCylinderIndices(int startIndex, int n) 
	{	
		vector<int> indices;

		for (int i = 0; i < n; i++)
		{
			if (i < (n - 1))
			{
				indices.push_back(i * 2 + startIndex);
				indices.push_back(i * 2 + 1 + startIndex);
				indices.push_back(i * 2 + 2 + startIndex);

				indices.push_back(i * 2 + 2 + startIndex);
				indices.push_back(i * 2 + 1 + startIndex);
				indices.push_back(i * 2 + 3 + startIndex);
			}
		}

		return indices;
	}

	//Camera functions
	void TestCylinder::OnUpdateCameraKey(keyAcion ka)
	{
		m_Camera.ProcessKeyInput(ka);
	}

	void TestCylinder::OnUpdateCameraMouseRotate(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputRotate(xPos, yPos);
	}

	void TestCylinder::OnUpdateCameraMousePan(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputPan(xPos, yPos);
	}

	void TestCylinder::OnUpdateCameraScroll(float fov)
	{
		m_Camera.ProcessScrollInput(fov);
	}

	void TestCylinder::OnUpdateCameraReset()
	{
		m_Camera.m_FirstMouseRotate = true;
	}

	void TestCylinder::OnUpdateCameraResetPan()
	{
		m_Camera.m_FirstMousePan = true;
	}





}

