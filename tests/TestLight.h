#pragma once

#include "Test.h"
#include <glew.h>
#include "../Shader.h"
#include "../Camera.h"

#include <memory>

using namespace std;

namespace testNS
{

	class TestLight : public Test
	{
	public:
		TestLight();
		~TestLight();

		void OnUpdate() override;
		void OnRender() override;
		void OnImGuiRenderer() override;
		void OnUpdateCameraKey(keyAcion ka) override;
		void OnUpdateCameraMouseRotate(float xPos, float yPos) override;
		void OnUpdateCameraMousePan(float xPos, float yPos) override;
		void OnUpdateCameraScroll(float fov) override;
		void OnUpdateCameraReset() override;

		vector<VertexN> CreateIcoSphere(float x, float y, float z, float radius, Vec4 color);
		vector<VertexN> CreateCube(float x, float y, float z, float size, Vec4 color);

		vector<int> GetIcoSphereIndices(int startIndex);
		vector<int> GetCubeIndices(int startIndex);

	private:
		unique_ptr<Shader> m_Shader;

		glm::mat4 m_Proj, m_View, m_Model;

		glm::vec3 m_TranslationA, m_TranslationB;

		unsigned int m_IndexVA;
		unsigned int m_IndexVB;
		unsigned int m_IndexIB;

		unsigned int m_icosaIndexVA;
		unsigned int m_icosaIndexVB;
		unsigned int m_icosaIndexIB;

		unsigned int m_lightVA;
		unsigned int m_lightVB;
		unsigned int m_lightIB;

		int m_nQuads;

		int m_IndexCount;
		int m_LineCount;

		unsigned int* m_Indices;
		unsigned int* m_icosaIndices;
		unsigned int* m_cubeIndices;
		unsigned int* m_cylinderIndices;

		VertexN* m_icosaVertices;
		VertexN* m_cubeVertices;

		int vCounter;
		int iCounter;

		glm::vec3 lightPosition;

		float m_ColorScaleValue;
		float m_ColorIncrement;
		float m_ColorStep;

		float m_Time;

		Camera m_Camera;

		float m_Fov;

	};

};