
#include "TestBatch.h" 
#include "imgui/imgui.h"
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include <array>



namespace testNS 
{
	
	static std::array<Vertex, 4> CreateQuad(float x, float y, float z, float size)
	{
		Vertex v0, v1, v2, v3;
		v0.Position = { x-size, y-size, 0 };
		v0.Color = { 0.18f, 0.6f, 0.96f, 1.0f };
		
		v1.Position = { x+size, y-size, 0 };
		v1.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v2.Position = { x+size, y+size, 0 };
		v2.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v3.Position = { x-size, y+size, 0 };
		v3.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		return { v0,v1,v2,v3 };
	}


	TestBatch::TestBatch() 
		:	m_Proj(glm::ortho(-2.0f, 2.0f, -1.5f, 1.5f, -1.0f, 1.0f)),
			m_View(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),			//glm::mat4 m4(1.0f) => constructs an identity matrix
			m_TranslationA(0, 0, 0),
			m_TranslationB(0, 0, 0),
			m_nQuads(500000)
	{
		m_Indices = new unsigned int[m_nQuads * 6];
		m_CornerPos = new Vertex[m_nQuads * 6];

		m_ColorStep = 0.05f;
		m_ColorIncrement = m_ColorStep;
		m_ColorScaleValue = 0.0f;

		//The projection matrix converts the modell into the normalised device coordinate space which spans from -1 -> 1 in x,y,z.
		//minX, maxX, minY, maxY, minZ, maxZ

		m_Shader = make_unique<Shader>("resources/shaders/BasicBatch.shader");

		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));

		GLCall(glGenVertexArrays(1, &m_IndexVA));
		GLCall(glBindVertexArray(m_IndexVA));

		GLCall(glGenBuffers(1, &m_IndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_IndexVB));
		//Allocating memory for 1000 vertex objects, but the memory is currently set to null and will be filled in later.
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nQuads * 10, nullptr, GL_DYNAMIC_DRAW)); 

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		//Automatic index generation based on number of predefinded quads.
		GenIndicies();
		
		GenPositions();
		
		glGenBuffers(1, &m_IndexIB);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndexIB);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nQuads * 6, m_Indices, GL_STATIC_DRAW);

	}

	TestBatch::~TestBatch()
	{
	
	}

	void TestBatch::GenIndicies()
	{
		int quadCounter = 0;
		int index = 0;

		for (int i = 0; i < m_nQuads; i++)
		{
			m_Indices[i * 6]     = index;
			m_Indices[i * 6 + 1] = index + 1;
			m_Indices[i * 6 + 2] = index + 2;
			m_Indices[i * 6 + 3] = index + 2;
			m_Indices[i * 6 + 4] = index + 3;
			m_Indices[i * 6 + 5] = index;

			index += 4;
		}
	}

	void TestBatch::GenPositions() 
	{	
		int counter = 0;
		float width = 1.8;
		float height = 1.3;

		float maxDist = std::sqrt(width * width + height * height);
		float minDist = 0;

		for (int i = 0; i < m_nQuads; i++)
		{
			float x = (width * 2) * (((float)rand() / (RAND_MAX)) - 0.5);
			float y = (height * 2) * (((float)rand() / (RAND_MAX)) - 0.5);
			float z = 0;

			float size = 0.001; //= 0.5 * (((float)rand() / (RAND_MAX)));

			float dist = std::sqrt(x * x + y * y);

			float perc = 100 *(dist / maxDist);

			auto quad = CreateQuad(x, y, z, size);

			for (int j = 0; j < 4; j++)
			{
				quad[j].Color = GetBlendedColor(perc);
				m_CornerPos[counter] = quad[j];
				counter++;
			}
		}
	}

	void TestBatch::UpdatePositions()
	{
		int counter = 0;

		for (int i = 0; i < m_nQuads * 4; i+=4)
		{
			float xRnd = 0.01 * (((float)rand() / (RAND_MAX)) - 0.5);
			float yRnd = 0.01 * (((float)rand() / (RAND_MAX)) - 0.5);
			
			m_CornerPos[i].Position.x += xRnd;
			m_CornerPos[i].Position.y += yRnd;
			
			m_CornerPos[i+1].Position.x += xRnd;
			m_CornerPos[i+1].Position.y += yRnd;

			m_CornerPos[i+2].Position.x += xRnd;
			m_CornerPos[i+2].Position.y += yRnd;

			m_CornerPos[i+3].Position.x += xRnd;
			m_CornerPos[i+3].Position.y += yRnd;

		}
	}

	void TestBatch::UpdateColor()
	{
		int counter = 0;

		for (int i = 0; i < m_nQuads * 4; i += 4)
		{
			m_CornerPos[i].Color.x = m_ColorScaleValue;
			m_CornerPos[i + 1].Color.x = m_ColorScaleValue;
			m_CornerPos[i + 1].Color.y = 1 - m_ColorScaleValue;
			m_CornerPos[i + 2].Color.x = m_ColorScaleValue;
			m_CornerPos[i + 2].Color.y = 1 - m_ColorScaleValue;
			m_CornerPos[i + 3].Color.x = m_ColorScaleValue;
			m_CornerPos[i + 3].Color.y = 1 - m_ColorScaleValue;
		}
	}


	void TestBatch::OnUpdate()
	{
		//Set dynamic vertex buffer
		GLCall(glGenBuffers(1, &m_IndexVB));

		//auto q0 = CreateQuad(-0.5f, -0.5f, 0.0f, 0.5f);
		//auto q1 = CreateQuad(0.5f, 0.5f, 0.0f, 0.5f);
		//auto q2 = CreateQuad(-0.5f, 0.5f, 0.0f, 0.3f);

		//Vertex vertices[12];
		//memcpy(vertices, q0.data(), q0.size() * sizeof(Vertex));		//Copying data from q0 into vertices[8] at the postion vertices points to
		//memcpy(vertices + q0.size(), q1.data(), q1.size() * sizeof(Vertex));
		//memcpy(vertices + (q0.size()+q1.size()), q2.data(), q2.size() * sizeof(Vertex));

		UpdatePositions();

		if (m_ColorScaleValue >= 1.0f)
			m_ColorIncrement = -1.0 * m_ColorStep;
		else if (m_ColorScaleValue <= 0.0f)
			m_ColorIncrement = m_ColorStep;

		m_ColorScaleValue += m_ColorIncrement;

		UpdateColor();

		GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex)* m_nQuads * 4, m_CornerPos));

		GLCall(glClear(GL_COLOR_BUFFER_BIT));


		glm::mat4 model = glm::translate(glm::mat4(1.0f), m_TranslationA);
		glm::mat4 mvp = m_Proj * m_View * model;

		m_Shader->Bind(); //Select the shader

		m_Shader->SetUniformMat4f("u_MVP", mvp);

	}

	void TestBatch::OnRender()
	{
		GLCall(glBindVertexArray(m_IndexVA));

		GLCall(glDrawElements(GL_TRIANGLES, m_nQuads * 6, GL_UNSIGNED_INT, nullptr));
	}

	void TestBatch::OnImGuiRenderer()
	{
		ImGui::SliderFloat3("Translation A", &m_TranslationA.x, -2.0f, 2.0f);  //Passing the memory adress of x and y z will then follow           // Edit 1 float using a slider from 0.0f to 1.0f    
		
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}
}

