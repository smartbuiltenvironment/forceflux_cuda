
#include "TestInstancing.h" 

//#include "../Renderer.h"
#include "imgui/imgui.h"
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include <array>


namespace testNS 
{
	TestInstancing::TestInstancing()
		:   m_Proj(glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f)),
			m_View(glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up)),
			m_Model(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),			//glm::mat4 m4(1.0f) => constructs an identity matrix
			m_Trans(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),			
			m_Rot(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),
			m_TranslationA(0, 0, 0),
			m_TranslationB(0, 0, 0),
			m_nParticles(5000000),			//5000000
			m_Time(0.0f),	
			m_Fov(45.0f),
			m_pSize(0.0007f),				//0.0007	
			m_MaxParticles(10000000)
	{
		m_Shader = make_unique<Shader>("resources/shaders/TestInstance.shader");

		m_Camera = Camera();
		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));
			
		m_ParticlePoints = new Point[m_nParticles];					//4 vertices per quad
		m_ParticleColors = new Color[m_nParticles];					//4 vertices per quad
		
		m_ColorStep = 0.05f;
		m_ColorIncrement = m_ColorStep;
		m_ColorScaleValue = 0.0f;

		GenPositions();

		static const GLfloat g_vertex_buffer_data[] = {
			-1.0f * m_pSize, -1.0f * m_pSize, 0.0f,
			 1.0f * m_pSize, -1.0f * m_pSize, 0.0f,
			-1.0f * m_pSize,  1.0f * m_pSize, 0.0f,
			 1.0f * m_pSize,  1.0f * m_pSize, 0.0f,
		};

		/*
		GLfloat g[12];
		QuadFromVector(&g[0], glm::vec3(0, 0, 1));

		static const GLfloat g_vertex_buffer_data[12] = {
			g[0], g[1], g[2],
			g[3], g[4], g[5],
			g[6], g[7], g[8],
			g[9], g[10], g[11]
		};*/

		//Billboard initialised with the vertex data
		glGenBuffers(1, &m_billboard_vertex_buffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_billboard_vertex_buffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(g_vertex_buffer_data), g_vertex_buffer_data, GL_STATIC_DRAW);

		// The VBO containing the positions and sizes of the particles
		glGenBuffers(1, &m_particles_position_buffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_particles_position_buffer);
		// Initialize with empty (NULL) buffer : it will be updated later, each frame.
		glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * 3 * sizeof(float), NULL, GL_STREAM_DRAW);

		// The VBO containing the colors of the particles
		glGenBuffers(1, &m_particles_color_buffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_particles_color_buffer);
		// Initialize with empty (NULL) buffer : it will be updated later, each frame.
		glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * 4 * sizeof(float), NULL, GL_STREAM_DRAW);

	}

	TestInstancing::~TestInstancing()
	{
	
	}

	void TestInstancing::QuadFromVector(GLfloat* g_vertex_buffer_data, glm::vec3 camDir)
	{
		camDir = glm::normalize(camDir);
		
		glm::vec3 up = vec3(0.0f, 0.0f, 1.0f);
		glm::vec3 right = cross(camDir, up);	

		up = cross(camDir, right);
	
		GLfloat quadCenter[] = {0,0,0};

		g_vertex_buffer_data[0]  = m_pSize * (quadCenter[0] - right.x - up.x);
		g_vertex_buffer_data[1]  = m_pSize * (quadCenter[1] - right.y - up.y);
		g_vertex_buffer_data[2]  = m_pSize * (quadCenter[2] - right.z - up.z);

		g_vertex_buffer_data[3]  = m_pSize * (quadCenter[0] + right.x - up.x);
		g_vertex_buffer_data[4]  = m_pSize * (quadCenter[1] + right.y - up.y);
		g_vertex_buffer_data[5]  = m_pSize * (quadCenter[2] + right.z - up.z);

		g_vertex_buffer_data[6]  = m_pSize * (quadCenter[0] - right.x + up.x);
		g_vertex_buffer_data[7]  = m_pSize * (quadCenter[1] - right.y + up.y);
		g_vertex_buffer_data[8]  = m_pSize * (quadCenter[2] - right.z + up.z);
		
		g_vertex_buffer_data[9]  = m_pSize * (quadCenter[0] + right.x + up.x);
		g_vertex_buffer_data[10] = m_pSize * (quadCenter[1] + right.y + up.y);
		g_vertex_buffer_data[11] = m_pSize * (quadCenter[2] + right.z + up.z);
	}

	void TestInstancing::GenPositions()
	{
		int counter = 0;
		float width = 1.8;
		float height = 1.3;
		float maxDist = height;
		float minDist = 0;

		Vec3 xVec = { m_pSize * m_Camera.m_Right.x , m_pSize * m_Camera.m_Right.y, m_pSize * m_Camera.m_Right.z };
		Vec3 yVec = { m_pSize * m_Camera.m_Up.x , m_pSize * m_Camera.m_Up.y, m_pSize * m_Camera.m_Up.z };

		for (int i = 0; i < m_nParticles; i++)
		{
			float r = height * (float)rand() / (RAND_MAX);
			float theta = 3.14f * (float)rand() / (RAND_MAX);
			float phi = (2.0f * 3.14f) * (float)rand() / (RAND_MAX);

			float x = r * std::sin(theta) * std::cos(phi);
			float y = r * std::sin(theta) * std::sin(phi);
			float z = r * std::cos(theta);

			float perc = 100 * (r / maxDist);

			m_ParticlePoints[i].Position.x = x;
			m_ParticlePoints[i].Position.y = y;
			m_ParticlePoints[i].Position.z = z;

			m_ParticleColors[i].Color = GetBlendedColor(perc);
		}
	}



	void TestInstancing::OnUpdateCameraKey(keyAcion ka)
	{
		m_Camera.ProcessKeyInput(ka);
	}

	void TestInstancing::OnUpdateCameraMouseRotate(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputRotate(xPos, yPos);
	}

	void TestInstancing::OnUpdateCameraMousePan(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputRotate(xPos, yPos);
	}
	
	void TestInstancing::OnUpdateCameraScroll(float fov)
	{
		m_Camera.ProcessScrollInput(fov);
	}

	void TestInstancing::OnUpdateCameraReset()
	{
		m_Camera.m_FirstMouseRotate = true;
	}

	void TestInstancing::OnUpdateCameraResetPan()
	{
		m_Camera.m_FirstMousePan = true;
	}

	float TestInstancing::AngleBetween(glm::vec3 a, glm::vec3 b) {
		glm::vec3 origin = glm::vec3(0, 0, 0);
		glm::vec3 da = glm::normalize(a - origin);
		glm::vec3 db = glm::normalize(b - origin);
		float dot = glm::dot(da, db);
		return glm::acos(dot);
		//return dot;
	}

	void TestInstancing::OnUpdate()
	{
		//Positions Vec3 -> 3 * float
		glBindBuffer(GL_ARRAY_BUFFER, m_particles_position_buffer);
		glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * 3 * sizeof(float), NULL, GL_STREAM_DRAW); // Buffer orphaning, a common way to improve streaming perf. See above link for details.
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_nParticles * 3 * sizeof(float), m_ParticlePoints);

		//Colors Vec4 -> 4 * float
		glBindBuffer(GL_ARRAY_BUFFER, m_particles_color_buffer);
		glBufferData(GL_ARRAY_BUFFER, m_MaxParticles * 4 * sizeof(float), NULL, GL_STREAM_DRAW); // Buffer orphaning, a common way to improve streaming perf. See above link for details.
		glBufferSubData(GL_ARRAY_BUFFER, 0, m_nParticles * 4 * sizeof(float), m_ParticleColors);

		//cout << m_Camera.m_Right.x << ", " << m_Camera.m_Right.y << ", " << m_Camera.m_Right.z << endl;
		//GLCall(glGenBuffers(1, &m_IndexVB_Particles));
		//UpdateQuad();
		//GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles * 4, m_ParticleVertices));

		GLCall(glClear(GL_COLOR_BUFFER_BIT));

		m_Shader->Bind();
		
		m_Proj = glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f/9.0f, 0.01f, 100.0f);
		m_View = glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up);
		m_Trans = glm::mat4(1.0f);
		m_Rot = glm::mat4(1.0f);
		m_Model = m_Trans * m_Rot;

		glm::mat4 mvp = m_Proj * m_View * m_Model;

		m_Shader->SetUniformMat4f("u_MVP", mvp);

		//m_Shader->SetUniformMat4f("model", m_Model);
		//m_Shader->SetUniformMat4f("view", m_View);
		//m_Shader->SetUniformMat4f("projection", m_Proj);
	}

	void TestInstancing::OnRender()
	{
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, m_billboard_vertex_buffer);
		glVertexAttribPointer(
			0, // attribute. No particular reason for 0, but must match the layout in the shader.
			3, // size
			GL_FLOAT, // type
			GL_FALSE, // normalized?
			0, // stride
			(void*)0 // array buffer offset
		);

		// 2nd attribute buffer : positions of particles' centers
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, m_particles_position_buffer);
		glVertexAttribPointer(
			1, // attribute. No particular reason for 1, but must match the layout in the shader.
			3, // size : x + y + z => 3
			GL_FLOAT, // type
			GL_FALSE, // normalized?
			0, // stride
			(void*)0 // array buffer offset
		);

		// 3rd attribute buffer : particles' colors
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, m_particles_color_buffer);
		glVertexAttribPointer(
			2, // attribute. No particular reason for 2, but must match the layout in the shader.
			4, // size : r + g + b + a => 4
			GL_FLOAT, // type
			GL_TRUE, // normalized? *** YES, this means that the unsigned char[4] will be accessible with a vec4 (floats) in the shader ***
			0, // stride
			(void*)0 // array buffer offset
		);



		// These functions are specific to glDrawArrays*Instanced*.
		// The first parameter is the attribute buffer we're talking about.
		// The second parameter is the "rate at which generic vertex attributes advance when rendering multiple instances"
		glVertexAttribDivisorARB(0, 0); // particles vertices : always reuse the same 4 vertices -> 0
		glVertexAttribDivisorARB(1, 1); // positions : one per quad (its center) -> 1
		glVertexAttribDivisorARB(2, 1); // color : one per quad -> 1

		// Draw the particules !
		// This draws many times a small triangle_strip (which looks like a quad).
		// This is equivalent to :
		// for(i in ParticlesCount) : glDrawArrays(GL_TRIANGLE_STRIP, 0, 4),
		// but faster.
		glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, m_nParticles);

	}

	void TestInstancing::OnImGuiRenderer()
	{
		//Passing the memory adress of x and y z will then follow automatically.    
		ImGui::SliderFloat3("Translation A", &m_TranslationA.x, -3.14f, 3.14f);  
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}
}

