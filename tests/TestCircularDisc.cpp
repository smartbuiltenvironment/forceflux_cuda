
#include "TestCircularDisc.h" 

#include "imgui/imgui.h"
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include <array>


namespace testNS 
{
	
	static std::array<Vertex, 4> CreateQuad(float x, float y, float z, float size)
	{
		Vertex v0, v1, v2, v3;
		v0.Position = { x-size, y-size, 0 };
		v0.Color = { 0.18f, 0.6f, 0.96f, 1.0f };
		
		v1.Position = { x+size, y-size, 0 };
		v1.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v2.Position = { x+size, y+size, 0 };
		v2.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		v3.Position = { x-size, y+size, 0 };
		v3.Color = { 0.18f, 0.6f, 0.96f, 1.0f };

		return { v0,v1,v2,v3 };
	}


	TestCircularDisc::TestCircularDisc()
		:	m_Proj(glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f)),
			m_View(glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up)),
			m_Model(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),			//glm::mat4 m4(1.0f) => constructs an identity matrix
			m_Trans(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),
			m_Rot(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),
			m_TranslationA(0, 0, 0),
			m_TranslationB(0, 0, 0),
			m_nParticles(100000)
	{
		m_Shader = make_unique<Shader>("resources/shaders/BasicBatch.shader");

		m_drawZones = true;
		m_drawBonds = true;
		m_drawParticles = true;

		m_nR = 20;
		m_nPhi = 50;
		m_nParticlesPerZone = (int)std::floor(m_nParticles / (m_nR * m_nPhi));
		
		m_Camera = Camera();
		m_Indices = new unsigned int[m_nParticles];
		m_zoneLineIndices = new unsigned int[m_nR * m_nPhi * 8]; //Allocating a little more space than nessecary.
		m_bondLineIndices = new unsigned int[m_nParticles * 100]; //Allocating a little more space than nessecary.

		m_ParticlePos = new Vertex[m_nParticles];
		m_ParticleZoneIndex = new int[m_nParticles];
		m_zzCount = new int[m_nR * m_nPhi];
		m_zoneVertices = new Vertex[m_nR * m_nPhi + m_nPhi];

		m_zLineIndexCounter = 0;
		m_zLineCounter = 0;

		m_bLineIndexCounter = 0;
		m_bLineCounter = 0;

		m_ColorStep = 0.05f;
		m_ColorIncrement = m_ColorStep;
		m_ColorScaleValue = 0.0f;

		m_pSpeed = 0.001;
		m_bindCounter = 0;
		
		
		GenParticleIndicies();
		GenParticlePositions();

		CreateBonds();

		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));

		GLCall(glGenVertexArrays(1, &m_pIndexVA));
		GLCall(glBindVertexArray(m_pIndexVA));

		GLCall(glGenBuffers(1, &m_pIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_pIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles * 10, nullptr, GL_DYNAMIC_DRAW)); //Allocating more then needed 

		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));
		
		glGenBuffers(1, &m_pIndexIB);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pIndexIB);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_nParticles, m_Indices, GL_STATIC_DRAW);

		
		//Zone lines
		GLCall(glGenVertexArrays(1, &m_zLineIndexVA));
		GLCall(glBindVertexArray(m_zLineIndexVA));
		
		GLCall(glGenBuffers(1, &m_zLineIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_zLineIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_zVertexCounter, nullptr, GL_STATIC_DRAW));
		
		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		GLCall(glGenBuffers(1, &m_zLineIndexIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_zLineIndexIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_zLineIndexCounter, m_zoneLineIndices, GL_STATIC_DRAW));
		
		//Bond lines
		GLCall(glGenVertexArrays(1, &m_bLineIndexVA));
		GLCall(glBindVertexArray(m_bLineIndexVA));

		
		GLCall(glGenBuffers(1, &m_bLineIndexVB));
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB));
		GLCall(glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * m_nParticles, nullptr, GL_DYNAMIC_DRAW));
		
		GLCall(glEnableVertexAttribArray(0)); //Attribute 0 is position
		GLCall(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Position)));

		GLCall(glEnableVertexAttribArray(1)); //Attribute 1 is color
		GLCall(glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const void*)offsetof(Vertex, Color)));

		GLCall(glGenBuffers(1, &m_bLineIndexIB));
		GLCall(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_bLineIndexIB));
		GLCall(glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_bLineIndexCounter, m_bondLineIndices, GL_STATIC_DRAW));
		

	}


	TestCircularDisc::~TestCircularDisc()
	{
		GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));
		GLCall(glBindVertexArray(0));
		m_Shader->Unbind();

		delete[] m_Indices;
		delete[] m_bondLineIndices;
		delete[] m_zoneLineIndices;
		delete[] m_ParticleZoneIndex;
		delete[] m_ParticlePos;
		delete[] m_zoneVertices;			//Flat list of zone corners
		delete[] m_zzCount;
	}

	void TestCircularDisc::OnUpdateCameraKey(keyAcion ka)
	{
		m_Camera.ProcessKeyInput(ka);
	}

	void TestCircularDisc::OnUpdateCameraMouseRotate(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputRotate(xPos, yPos);
	}

	void TestCircularDisc::OnUpdateCameraMousePan(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputPan(xPos, yPos);
	}

	void TestCircularDisc::OnUpdateCameraScroll(float fov)
	{
		m_Camera.ProcessScrollInput(fov);
	}

	void TestCircularDisc::OnUpdateCameraReset()
	{
		m_Camera.m_FirstMouseRotate = true;
	}

	void TestCircularDisc::OnUpdateCameraResetPan()
	{
		m_Camera.m_FirstMousePan = true;
	}

	void TestCircularDisc::GenParticleIndicies()
	{
		int quadCounter = 0;
		int index = 0;

		for (int i = 0; i < m_nParticles; i++)
		{
			m_Indices[i] = i;
		}
	}


	float TestCircularDisc::GenRandom(float min, float max) 
	{
		float rnd = (float)rand() / (RAND_MAX);
		float diff = max - min;
		float val = min + rnd * diff;

		return val;
	}


	void TestCircularDisc::GenParticlePositions()
	{	
		int counter = 0;
		float width = 1.8;
		float height = 1.3;

		float maxDist = std::sqrt(width * width + height * height);
		float minDist = 0;

		float rMin = 0;
		float rMax = height;
		float rStep = rMax / m_nR;
		
		float phiMin = 0;
		float phiMax = 2.0f * 3.1415f;
		float phiStep = phiMax / m_nPhi;

		float rVal = 0.1;
		float phiVal = 0;

		int zoneCounter = 0;

		int npCount = 10;

		for (int i = 0; i < m_nR; i++)
		{
			for (int j = 0; j < m_nPhi; j++)
			{
				vector<Vertex> vertVec;
				vector<int> flatIndexVec;

				for (int k = 0; k < m_nParticlesPerZone; k++)
				{
					float r = GenRandom(rVal, rVal+rStep);
					float phi = GenRandom(phiVal, phiVal + phiStep);

					float x = r * std::cos(phi);
					float y = r * std::sin(phi);
					float z = 0;

					float dist = std::sqrt(x * x + y * y);
					float perc = 100 * (dist / maxDist);

					Vertex v0;
					v0.Position = { x, y, z };
					v0.Color = GetBlendedColor(perc);

					m_ParticlePos[counter] = v0;
					
					flatIndexVec.push_back(counter);
					vertVec.push_back(v0);

					counter++;

				}

				m_zFlatParticleIndex.push_back(flatIndexVec);
				m_zParticlesPos.push_back(vertVec);

				SetZoneVertices(rVal, phiVal, zoneCounter);
				SetZoneLineIndices(zoneCounter, i, j);
				SetZoneZoneTopology(i, j, zoneCounter);

				phiVal += phiStep;
				zoneCounter++;
				
			}
			phiVal = 0;
			rVal += rStep;
		}

		//Add the last row of zone vertices
		for (int j = 0; j < m_nPhi; j++)
		{
			SetZoneVertices(rVal, phiVal, zoneCounter);
			phiVal += phiStep;
			zoneCounter++;
		}
	}

	void TestCircularDisc::CreateBonds() 
	{
		int zoneCount = m_nR * m_nPhi;
		Vertex v, vNext;
		float d = 0;
		m_Horizon = 0.015;
		int vFlatIndex, vNextFlatIndex;
		int pCount = 0;
		int addCount = 0;

		for (int i = 0; i < zoneCount; i++)
		{
			//int i = 999;
			int currentZone = m_zzTop[i][0];

			if (addCount == m_nPhi) 
			{
				addCount = 0;

				m_Horizon += m_Horizon * 0.05;
			}

			//Looping over all particles in current zone
			for(int k = 0; k < m_nParticlesPerZone; k++)
			{
				//int k = 10;
				vFlatIndex = m_zFlatParticleIndex[currentZone][k];
				v = m_ParticlePos[vFlatIndex];

				//Loop over neighbouring zone indices
				for(int j = 0; j < m_zzCount[i]; j++)
				{
					int nextZone = m_zzTop[i][j];

					//Looping over all particles in the neighbouring zone
					for(int m = 0; m < m_nParticlesPerZone; m++)
					{	
						//vNext = m_zParticlesPos[nextZone][m];
						vNextFlatIndex = m_zFlatParticleIndex[nextZone][m];

						vNext = m_ParticlePos[vNextFlatIndex];

						d = Distance(v, vNext);

						pCount++;

						//Create a line
						if (d < m_Horizon) 
						{
							m_bondLineIndices[m_bLineIndexCounter] = vFlatIndex;
							m_bLineIndexCounter++;
							
							m_bondLineIndices[m_bLineIndexCounter] = vNextFlatIndex;
							m_bLineIndexCounter++;

							m_bLineCounter++;	
						}
					}
				}
			}

			addCount++;
		}

		std::cout << "Bond lines count: " << m_bLineCounter << std::endl;

	}

	void TestCircularDisc::SetZoneZoneTopology(int i, int j, int zoneCounter) 
	{
		vector<int> zTop;

		if (i > 0 && i < m_nR - 1 && j > 0 && j < m_nPhi - 1) 
		{	
			zTop.push_back(zoneCounter);
			zTop.push_back(zoneCounter - 1);
			zTop.push_back(zoneCounter - 1 - m_nPhi);
			zTop.push_back(zoneCounter - m_nPhi);
			zTop.push_back(zoneCounter - m_nPhi +1);
			zTop.push_back(zoneCounter + 1);
			zTop.push_back(zoneCounter + 1 + m_nPhi);
			zTop.push_back(zoneCounter + m_nPhi);
			zTop.push_back(zoneCounter + m_nPhi - 1);
			m_zzCount[zoneCounter] = 9;
		}	
		else if (i == 0 && j > 0 && j < m_nPhi - 1)
		{
			zTop.push_back(zoneCounter);
			zTop.push_back(zoneCounter - 1);
			zTop.push_back(zoneCounter + 1);
			zTop.push_back(zoneCounter + 1 + m_nPhi);
			zTop.push_back(zoneCounter + m_nPhi);
			zTop.push_back(zoneCounter + m_nPhi - 1);
			m_zzCount[zoneCounter] = 6;
		}
		else if (i == 0 && j == 0)
		{
			zTop.push_back(zoneCounter);
			zTop.push_back(zoneCounter + 1);
			zTop.push_back(zoneCounter + m_nPhi - 1);
			zTop.push_back(zoneCounter + m_nPhi - 1 + m_nPhi);
			zTop.push_back(zoneCounter + m_nPhi);
			zTop.push_back(zoneCounter + m_nPhi + 1);
			m_zzCount[zoneCounter] = 6;
		}
		else if (i == 0 && j == m_nPhi-1)
		{
			zTop.push_back(zoneCounter);
			zTop.push_back(zoneCounter + 1);
			zTop.push_back(zoneCounter - 1);
			zTop.push_back(zoneCounter + m_nPhi - 1);
			zTop.push_back(zoneCounter + m_nPhi);
			zTop.push_back(0);
			m_zzCount[zoneCounter] = 6;
		}
		else if (i > 0 && i < m_nR-1 && j == m_nPhi - 1)
		{
			zTop.push_back(zoneCounter);
			zTop.push_back(zoneCounter - 1);
			zTop.push_back(zoneCounter - 1 - m_nPhi);
			zTop.push_back(zoneCounter + m_nPhi);
			zTop.push_back(zoneCounter - m_nPhi + 1);
			zTop.push_back(zoneCounter + 1);
			zTop.push_back(zoneCounter - m_nPhi);
			zTop.push_back(zoneCounter - 2 * m_nPhi +1);
			zTop.push_back(zoneCounter + m_nPhi - 1);
			m_zzCount[zoneCounter] = 9;
		}

		else if (i > 0 && i < m_nR - 1 && j == 0)
		{
			zTop.push_back(zoneCounter);
			zTop.push_back(zoneCounter - 1);
			zTop.push_back(zoneCounter + 2 * m_nPhi - 1);
			zTop.push_back(zoneCounter + m_nPhi);
			zTop.push_back(zoneCounter - m_nPhi + 1);
			zTop.push_back(zoneCounter + 1);
			zTop.push_back(zoneCounter - m_nPhi);
			zTop.push_back(zoneCounter + m_nPhi + 1);
			zTop.push_back(zoneCounter + m_nPhi - 1);
			m_zzCount[zoneCounter] = 9;
		}

		else if (i == (m_nR-1) && j < m_nPhi-1)
		{
			zTop.push_back(zoneCounter);
			zTop.push_back(zoneCounter - 1);
			zTop.push_back(zoneCounter - 1 - m_nPhi);
			zTop.push_back(zoneCounter - m_nPhi);
			zTop.push_back(zoneCounter - m_nPhi + 1);
			zTop.push_back(zoneCounter + 1);
			m_zzCount[zoneCounter] = 6;
		}

		else if (i == (m_nR - 1) && j == m_nPhi - 1)
		{
			zTop.push_back(zoneCounter);
			zTop.push_back(zoneCounter - 1);
			zTop.push_back(zoneCounter - 1 - m_nPhi);
			zTop.push_back(zoneCounter - m_nPhi);
			zTop.push_back(zoneCounter - m_nPhi + 1);
			zTop.push_back(zoneCounter - 2 * m_nPhi + 1);
			m_zzCount[zoneCounter] = 6;
		}

		m_zzTop.push_back(zTop);
	}

	void TestCircularDisc::SetZoneVertices(float r, float phi, int zoneCouter) 
	{
		float x1 = r * std::cos(phi);
		float y1 = r * std::sin(phi);
		float z1 = 0;

		Vec4 color = { 0.5, 0.5, 0.5, 1};
		Vertex v1;
		v1.Position = { x1, y1, z1 };
		v1.Color = color;

		m_zoneVertices[zoneCouter] = v1;
		m_zVertexCounter++;
	}

	void TestCircularDisc::SetZoneLineIndices(int zoneCornerCouter, int i, int j)
	{
		int zoneCornerIndex = zoneCornerCouter;
		int zoneCornerIndexNextPhi = zoneCornerCouter + 1;
		int zoneCornerIndexNextR = zoneCornerCouter + m_nPhi;
		int zoneCornerIndexNextRandPhi = zoneCornerCouter + m_nPhi + 1;

		if (j == (m_nPhi - 1))
		{
			zoneCornerIndexNextPhi = zoneCornerCouter + 1 - m_nPhi; //Get the index from the first corner on this loop.
			zoneCornerIndexNextRandPhi = zoneCornerIndexNextR - m_nPhi +1;
		}

		//Line in r-direction
		m_zoneLineIndices[m_zLineIndexCounter] = zoneCornerIndex;
		m_zLineIndexCounter++;
		m_zoneLineIndices[m_zLineIndexCounter] = zoneCornerIndexNextR;
		m_zLineIndexCounter++;

		//Line in phi-direction
		m_zoneLineIndices[m_zLineIndexCounter] = zoneCornerIndex;
		m_zLineIndexCounter++;
		m_zoneLineIndices[m_zLineIndexCounter] = zoneCornerIndexNextPhi;
		m_zLineIndexCounter++;

		m_zLineCounter += 2;
		
		
		if (i == (m_nR - 1))
		{
			//Line in phi-direction for the next r value
			m_zoneLineIndices[m_zLineIndexCounter] = zoneCornerIndexNextR;
			m_zLineIndexCounter++;
			m_zoneLineIndices[m_zLineIndexCounter] = zoneCornerIndexNextRandPhi;
			m_zLineIndexCounter++;

			m_zLineCounter++;
		}
		
	}


	void TestCircularDisc::UpdatePositions()
	{
		int counter = 0;
		int quadCounter = 0;

		for (int i = 0; i < m_nParticles * 4; i+=4)
		{
			float xRnd = m_pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);
			float yRnd = m_pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);
			float zRnd = 0; //m_pSpeed * (((float)rand() / (RAND_MAX)) - 0.5);

			m_ParticlePos[quadCounter].Position.x += xRnd;
			m_ParticlePos[quadCounter].Position.y += yRnd;
			m_ParticlePos[quadCounter].Position.z += zRnd;

			quadCounter++;
		}
	}

	void TestCircularDisc::UpdateColor()
	{
		int counter = 0;

		int quadCounter = 0;

		for (int i = 0; i < m_nParticles * 4; i += 4)
		{
			m_ParticlePos[quadCounter].Color.x = m_ColorScaleValue;
			m_ParticlePos[quadCounter].Color.y = 1 - m_ColorScaleValue;

			quadCounter++;
		}
	}


	void TestCircularDisc::OnUpdate()
	{
		if (m_ColorScaleValue >= 1.0f)
			m_ColorIncrement = -1.0 * m_ColorStep;
		else if (m_ColorScaleValue <= 0.0f)
			m_ColorIncrement = m_ColorStep;

		m_ColorScaleValue += m_ColorIncrement;
		
		
		//UpdatePositions();

		//UpdateColor();
		
		if (m_drawParticles) 
		{
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_pIndexVB));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles, m_ParticlePos));
		}

		if (m_drawZones) 
		{
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_zLineIndexVB));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_zVertexCounter, m_zoneVertices));
		}

		if (m_drawBonds)
		{
			GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_bLineIndexVB));
			GLCall(glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(Vertex) * m_nParticles, m_ParticlePos));
		}


		GLCall(glClear(GL_COLOR_BUFFER_BIT));
		
		m_Shader->Bind();
		m_bindCounter++;

		m_Proj = glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f);
		m_View = glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up);
		m_Trans = glm::mat4(1.0f);
		m_Rot = glm::mat4(1.0f);
		m_Model = m_Trans * m_Rot;

		glm::mat4 mvp = m_Proj * m_View * m_Model;

		m_Shader->SetUniformMat4f("u_MVP", mvp);
	}

	void TestCircularDisc::OnRender()
	{
		//Draw points
		if (m_drawParticles) 
		{
			GLCall(glBindVertexArray(m_pIndexVA));
			GLCall(glDrawElements(GL_POINTS, m_nParticles, GL_UNSIGNED_INT, nullptr));
		}
		
		//Draw lines
		if (m_drawZones) 
		{
			GLCall(glBindVertexArray(m_zLineIndexVA));
			GLCall(glDrawElements(GL_LINES, m_zLineIndexCounter + 1, GL_UNSIGNED_INT, nullptr));
		}

		//Draw lines
		if (m_drawBonds)
		{
			GLCall(glBindVertexArray(m_bLineIndexVA));
			GLCall(glDrawElements(GL_LINES, m_bLineIndexCounter +1, GL_UNSIGNED_INT, nullptr));
		}

	}

	void TestCircularDisc::OnImGuiRenderer()
	{
		//ImGui::SliderFloat3("Translation A", &m_TranslationA.x, -2.0f, 2.0f);  //Passing the memory adress of x and y z will then follow           // Edit 1 float using a slider from 0.0f to 1.0f    
		
		ImGui::SliderFloat("Speed", &m_pSpeed, 0.00010, 0.010);

		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

		ImGui::Checkbox("Draw particles", &m_drawParticles);

		ImGui::Checkbox("Draw zones", &m_drawZones);

		ImGui::Checkbox("Draw bonds", &m_drawBonds);

	}
}

