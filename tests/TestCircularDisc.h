#pragma once

#include "Test.h"
#include <glew.h>
#include <vector>
#include "../Shader.h"
#include "../Camera.h"




#include <memory>

using namespace std;

namespace testNS
{
	class TestCircularDisc : public Test
	{
	public:
		TestCircularDisc();
		~TestCircularDisc();

		void OnUpdate() override;
		void OnRender() override;
		void OnImGuiRenderer() override;

		void GenParticlePositions();
		void GenParticleIndicies();
		void UpdatePositions();
		void UpdateColor();
		void OnUpdateCameraKey(keyAcion ka) override;
		void OnUpdateCameraMouseRotate(float xPos, float yPos) override;
		void OnUpdateCameraMousePan(float xPos, float yPos) override;
		void OnUpdateCameraScroll(float fov) override;
		void OnUpdateCameraReset() override;
		void OnUpdateCameraResetPan() override;

		void CreateBonds();

		void SetZoneVertices(float r, float phi, int zoneCounter);
		void SetZoneLineIndices(int zoneCornerCouter, int i, int j);
		void SetZoneZoneTopology(int i, int j, int zoneCounter);

		void Initialise2DArrays();

		float GenRandom(float min, float max);

	private:

		unique_ptr<Shader> m_Shader;						//Smart pointers, deleted automatically etc..

		glm::mat4 m_Proj, m_View, m_Model, m_Trans, m_Rot;


		glm::vec3 m_TranslationA, m_TranslationB;

		unsigned int m_pIndexVA;
		unsigned int m_pIndexVB;
		unsigned int m_pIndexIB;

		unsigned int m_zLineIndexVA;
		unsigned int m_zLineIndexVB;
		unsigned int m_zLineIndexIB;

		unsigned int m_bLineIndexVA;
		unsigned int m_bLineIndexVB;
		unsigned int m_bLineIndexIB;


		int m_nParticles;

		int m_bLineIndexCounter;
		int m_bLineCounter;
		int m_bVertexCounter;

		int m_zLineIndexCounter;
		int m_zLineCounter;
		int m_zVertexCounter;
		
		unsigned int* m_Indices;
		unsigned int* m_bondLineIndices;
		unsigned int* m_zoneLineIndices;
		int* m_ParticleZoneIndex;
		Vertex* m_ParticlePos;
		Vertex* m_zoneVertices;			//Flat list of zone corners
		int* m_zzCount;

		vector<vector<Vertex>> m_zParticlesPos;
		vector<vector<int>> m_zFlatParticleIndex;
		vector<vector<int>> m_zzTop;
		
		int m_nParticlesPerZone;
		int m_nR;
		int m_nPhi;

		float m_Horizon;

		Camera m_Camera;

		float m_Fov;

		float m_pSize;

		float m_ColorScaleValue;
		float m_ColorIncrement;
		float m_ColorStep;

		bool m_drawZones;
		bool m_drawBonds;
		bool m_drawParticles;

		float m_pSpeed;

		int m_bindCounter;

	};

};