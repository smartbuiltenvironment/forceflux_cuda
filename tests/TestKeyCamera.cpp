
#include "TestKeyCamera.h" 
#include "imgui/imgui.h"
#include "glm/glm.hpp" 
#include "glm/gtc/matrix_transform.hpp"
#include <array>


namespace testNS 
{

	TestKeyCamera::TestKeyCamera()
		:   m_Proj(glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f / 9.0f, 0.01f, 100.0f)),
			m_View(glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up)),
			m_Model(glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0))),			//glm::mat4 m4(1.0f) => constructs an identity matrix
			m_TranslationA(0, 0, 0),
			m_TranslationB(0, 0, 0),
			m_nQuads(100),
			m_Time(0.0f),
			m_Fov(45.0f)
	{

		m_Camera = Camera();

		m_Shader = make_unique<Shader>("resources/shaders/BasicLine.shader");

		GLCall(glClearColor(0.0f, 0.0f, 0.0f, 1.0f));

		GLfloat vertices[] =
		{
		  -1.0f, -1.0f,  -1.0f, 
		   1.0f, -1.0f,  -1.0f,
		   1.0f,  1.0f,  -1.0f,
		  -1.0f,  1.0f,  -1.0f,

		  -1.0f, -1.0f,  1.0f,
		   1.0f, -1.0f,  1.0f,
		   1.0f,  1.0f,  1.0f,
		  -1.0f,  1.0f,  1.0f,

		   0.0f,  0.0f,  0.0f,
		   3.0f,  0.0f,  0.0f,
		   0.0f,  3.0f,  0.0f,
		   0.0f,  0.0f,  3.0f,
		 };

		
		glGenVertexArrays(1, &m_IndexVA);
		glBindVertexArray(m_IndexVA);

		glGenBuffers(1, &m_IndexVB);
		glBindBuffer(GL_ARRAY_BUFFER, m_IndexVB);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(GLfloat), (void*)0);
		
		m_IndexCount = 24+6;

		m_Indices = new unsigned int[m_IndexCount]
		{ 
			0,1,
			1,2,
			2,3,
			3,0,

			4,5,
			5,6,
			6,7,
			7,4,
			
			0, 4,
			1, 5,
			2, 6,
			3, 7,

			8,9,
			8,10,
			8,11
		};

		glGenBuffers(1, &m_IndexIB);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_IndexIB);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * m_IndexCount, m_Indices, GL_STATIC_DRAW);
	}

	TestKeyCamera::~TestKeyCamera()
	{
	
	}

	void TestKeyCamera::OnUpdateCameraKey(keyAcion ka)
	{
		m_Camera.ProcessKeyInput(ka);
	}

	void TestKeyCamera::OnUpdateCameraMouseRotate(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputRotate(xPos, yPos);
	}

	void TestKeyCamera::OnUpdateCameraMousePan(float xPos, float yPos)
	{
		m_Camera.ProcessMouseInputPan(xPos, yPos);
	}

	void TestKeyCamera::OnUpdateCameraScroll(float fov)
	{
		m_Camera.ProcessScrollInput(fov);
	}

	void TestKeyCamera::OnUpdateCameraReset()
	{
		m_Camera.m_FirstMouseRotate = true;
	}

	void TestKeyCamera::OnUpdate()
	{
		GLCall(glClear(GL_COLOR_BUFFER_BIT));

		m_Shader->Bind();
		
		m_Proj = glm::perspective(glm::radians(m_Camera.m_Fov), 16.0f/9.0f, 0.01f, 100.0f);

		m_View = glm::lookAt(m_Camera.m_Position, m_Camera.m_Target, m_Camera.m_Up);

		m_Model = glm::translate(glm::mat4(1.0f), m_TranslationA);

		m_Shader->SetUniformMat4f("model", m_Model);
		m_Shader->SetUniformMat4f("view", m_View);
		m_Shader->SetUniformMat4f("projection", m_Proj);
	}

	void TestKeyCamera::OnRender()
	{
		//Renderer renderer;
		GLCall(glBindVertexArray(m_IndexVA));
		GLCall(glDrawElements(GL_LINES, m_IndexCount, GL_UNSIGNED_INT, nullptr));
	}

	void TestKeyCamera::OnImGuiRenderer()
	{
		//Passing the memory adress of x and y z will then follow automatically.    
		ImGui::SliderFloat3("Translation A", &m_TranslationA.x, -10.0f, 10.0f);  
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
	}
}

