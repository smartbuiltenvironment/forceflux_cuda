#pragma once

#include <iostream>

#include <functional>
#include <vector>
#include <string>
#include "../Common.h"
#include "../ErrorCheck.h"


namespace testNS
{
	class Test
	{
	public:
		Test() {}
		virtual ~Test() {}

		virtual void OnUpdate() {}	//Virtual functions implemented with a "blank body"
		virtual void OnRender() {}					//Runs every frame
		virtual void OnImGuiRenderer() {}
		virtual void OnUpdateCameraKey(keyAcion ka) {}
		virtual void OnUpdateCameraMouseRotate(float xPos, float yPos) {}
		virtual void OnUpdateCameraMousePan(float xPos, float yPos) {}
		virtual void OnUpdateCameraScroll(float fov) {}
		virtual void OnUpdateCameraReset() {}
		virtual void OnUpdateCameraResetPan() {}
	};


	class TestMenu : public Test
	{
	public:
		TestMenu(Test*& currentTestPointer);

		void OnImGuiRenderer() override;

		template<typename T>
		void RegisterTest(const std::string& name)
		{
			std::cout << "Registering test " << name << std::endl;

			m_Tests.push_back(std::make_pair(name, []() {return new T(); }));
		}

	private:
		Test*& m_CurrentTest;
		std::vector<std::pair<std::string, std::function< Test* () >>> m_Tests;

	};



}