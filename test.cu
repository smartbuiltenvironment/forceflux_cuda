﻿#include <cuda_runtime.h>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>
#include "device_launch_parameters.h"


class TestClass
{
public:

    double x, y;

    TestClass()
    {
        x = 10.0;
        y = 20.0;
    }

    TestClass(double X, double Y)
    {
        x = X;
        y = Y;
    }

    void Print() 
    {
        std::cout << x << ", " << y << std::endl;
    }


};


// CUDA kernel for vector addition
// __global__ means this is called from the CPU, and runs on the GPU
__global__ void vectorAdd(const int* __restrict a, const int* __restrict b,
    int* __restrict c, int N) {
    // Calculate global thread ID
    int tid = (blockIdx.x * blockDim.x) + threadIdx.x;

    // Boundary check
    if (tid < N) c[tid] = a[tid] + b[tid];
}

// Check vector add result
void verify_result(std::vector<int> &a, std::vector<int> &b, std::vector<int> &c) {
    for (int i = 0; i < a.size(); i++) {
        assert(c[i] == a[i] + b[i]);
    }
}

int notMain() {
    // Array size of 2^16 (65536 elements)
    constexpr int N = 1 << 16;
    //constexpr int N = 12;
    constexpr size_t bytes = sizeof(int) * N;

    TestClass tc;


    // Vectors for holding the host-side (CPU-side) data
    std::vector<int> a(N);
    std::vector<int> b(N);
    std::vector<int> c(N);

    // Initialize random numbers in each array
    for (int i = 0; i < N; i++) {
        a[i] = (rand() % 100);
        b[i] = (rand() % 100);
    }

    // Allocate memory on the device
    int* d_a, * d_b, * d_c;
    cudaMalloc(&d_a, bytes);
    cudaMalloc(&d_b, bytes);
    cudaMalloc(&d_c, bytes);

    // Copy data from the host to the device (CPU -> GPU)

    //a.data() returns a pointer to the a vector (which is stored in the cpu memory)
    //cudaMemcpyHostToDevice - specifying the direction of the transfere.

    cudaMemcpy(d_a, a.data(), bytes, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, b.data(), bytes, cudaMemcpyHostToDevice);

    // Threads per CTA (1024), i.e. number of threads per thread-block.
    int NUM_THREADS = 1 << 10;

    // CTAs per Grid
    // We need to launch at LEAST as many threads as we have elements
    // This equation pads an extra CTA to the grid if N cannot evenly be divided
    // by NUM_THREADS (e.g. N = 1025, NUM_THREADS = 1024)
    // Could have been:
    //int NUM_BLOCKS = (N) /NUM_THREADS; 
    //if we new that 
    int NUM_BLOCKS = (N + NUM_THREADS - 1) / NUM_THREADS;

    // Launch the kernel on the GPU
    // Kernel calls are asynchronous (the CPU program continues execution after
    // call, but no necessarily before the kernel finishes)
    vectorAdd << <NUM_BLOCKS, NUM_THREADS >> > (d_a, d_b, d_c, N);

    // Copy sum vector from device to host
    // cudaMemcpy is a synchronous operation, and waits for the prior kernel
    // launch to complete (both go to the default stream in this case).
    // Therefore, this cudaMemcpy acts as both a memcpy and synchronization
    // barrier.
    cudaMemcpy(c.data(), d_c, bytes, cudaMemcpyDeviceToHost);

    // Check result for errors
    verify_result(a, b, c);

    // Free memory on device
    cudaFree(d_a);
    cudaFree(d_b);
    cudaFree(d_c);

    std::cout << "COMPLETED SUCCESSFULLY\n";

    return 0;
}