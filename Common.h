#pragma once

#include <cmath>
#include <chrono>
#include <thread>
#include <math.h>

//#define float PI = 3.14159265358979323846;

#define RANDMAX = 1;


enum class drawType
{
	drawStatic, 
    drawDynamic
};

enum class keyAcion
{
    non, 
    leftArrow, 
    rightArrow, 
    upArrow, 
    downArrow, 
    mouse, 
    scroll
};

enum class colorParticleBy 
{
    displacement,
    velocity, 
    acceleration, 
    meanStrain, 
    forceArm,
    forceExt,
    forceTot,
    damage,
    isBC,
};

enum class colorArmBy
{
    elongationToFracture,
    strain,
    plasticStrain,
    force,
    forceComTen,
    broken,
    nonFailureZone,
};

enum class pArrangement
{
    regular,
    irregular,
    random
};

enum class simulationType
{
    Cylinder,
    SplitCylinder,
    ModulusOfRupture,
    DirectTension,
    SteelTest,
};


enum class BC
{
    prescribeAll,
    prescribeNon,
    prescribeZ,
    springLoad,
    withFriction,
    loadCylinders
};

enum class BCtype
{
    support,
    load
};

enum class viewType
{
    perspective,
    orthographic
};





struct Vec2
{
	float x, y;
};

struct Vec3
{
	float x, y, z;
};

struct Vec4
{
	float x, y, z, w;
};

struct Arr3
{
    int x, y, z;
};


struct Vertex
{
	Vec3 Position;
	Vec4 Color;
};


struct VertexD
{
    Vec3 cPosition;
    Vec4 Color;
    Vec3 iPosition;
};

struct VertexN
{
    Vec3 Position;
    Vec3 Normal;
    Vec4 Color;
};

struct LoadingCylinder 
{
    Vec3 cPosition;
    Vec3 Normal;
    float Radius;
    BCtype type;
    Vec3 iPosition;
};

struct Particle 
{
    Vec3 cPosition;         //Current position
    Vec3 iPosition;         //Initial position
    Vec3 ArmForce;
    Vec3 ExtForce;
    Vec3 TotForce;
    Vec3 Veloc;
    Vec3 Accel;
    Vec4 Color;
    float h;                //Radius of the particle
    float rMass;            //Real mass 
    float fMass;            //Fictious mass
    float meanStrain;
    float displacement;
    float rSize;
    float damage;           //Number of broken bonds over number of total bonds.
    bool isBC;
};

struct Arm
{
    int p1;
    int p2;
    float iLength;
    float cLength;
    float elongation;
    float strain;
    float plasticStrain;
    float fMag;
    Vec4 color;
    bool broken;
    bool insideNonFailZone;
    float elongationLimit;
};

struct ArmIndices 
{
    int p1;
    int p2;
    int flatIndex;
};

struct Point
{
    Vec3 Position;
};

struct Color
{
    Vec4 Color;
};

struct Line
{
    Vec3 StartPosition;
    Vec3 EndPosition;
    Vec4 Color;
};

struct Stats 
{
    int nBondsPerParticle;
    int nParticlesPerZone;
    int nBonds;
    int nParticles;
    int nZones;
    int nUsedZones;
    float zoneSizeParticleSizeRatio;
};

struct ModelSettings 
{
    Arr3 zoneCount;
    Vec3 zoneStep;
    Vec3 bbSize;
    Vec3 zonePadding;
    float height;
    float radius;
    float AreaSmall;
    float AreaLarge;
    float bcAngle;
    float bcThickness;
    float edgePadding;
    float waistSF;
    float rndSeed;
};


struct PoissonsRatioSettings
{
    float hLimit;
    float waistHeight;
    float rLimit;
};


struct SolverSettings 
{
    float dt;       //Timestep
    float co;       //Carry over factor
    float t;        //Time
    float t1;       //Time at t + 0.5* dt
    float t2;       //Time at t + dt
};

struct LoadSettings
{
    float magnitude;      //Total load in [N]
    int nLoadSteps;
    int stepCounter;
};

struct DisplacementSettings
{
    float magnitude;      //Total load in [N]
    int nDispSteps;
    int stepCounter;
    float startMag;       //Start magnitude when using a varying load step  
    float endMag;         //End magnitude for varying load step
    int loadIncreaseSteps;
    int loadIncreaseStepSize;
};


struct SpringSettings
{
    float k;      //Total load in [N]
    float iLength;
};



struct Material 
{
    float rho;              //Density    
    float E;                //Youngs modulus
    float K;                //Bulk modulus    
    float G;                //Shear modulus
    float v;                //Poissions ratio
    float yieldStrain;      //Yield strain in compression
    float elongationLimit;  //Plastic elongation limit to failure
    float nonFailElongationLimit;  //Plastic elongation limit to failure
};

struct KernelSettings 
{
    float alpha;
    float alphaSquared;
    float alphaPowThree;
    float alphaPowSeven;
    float C;
    float k;

};


static Vec4 GetBlendedColor(float percentage)
{
    float frac;

    if (percentage >= 0.0f && percentage <= 25.0f)
    {
        //Blue fading to Cyan [0,x,255], where x is increasing from 0 to 255
        frac = percentage / 25.0f;
        return { 0.0f, (frac * 1.0f), 1.0f , 1.0f};
    }
    else if (percentage <= 50.0f)
    {
        //Cyan fading to Green [0,255,x], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 25.0f) / 25.0f;
        return { 0.0f, 1.0f, (frac * 1.0f), 1.0f};
    }
    else if (percentage <= 75.0f)
    {
        //Green fading to Yellow [x,255,0], where x is increasing from 0 to 255
        frac = std::abs(percentage - 50.0f) / 25.0f;
        return {(frac * 1.0f), 1.0f, 0.0f, 1.0f };
    }
    else if (percentage <= 100.0f)
    {
        //Yellow fading to red [255,x,0], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 75.0f) / 25.0f;
        return { 1.0f, (frac * 1.0f), 0.0f, 1.0f };
    }
    else if (percentage > 100.0f)
    {
        return { 1.0f, 0.0f, 0.0f, 1.0f};
    }

    return { 0.5, 0.5, 0.5, 1.0};
}


static Vec4 GetBlendedColor(float min, float max, float value)
{
    float diff = max - min;

    //float newMin = 0;
    float newMax = diff;
    float newValue = value - min;
    float percentage = 100.0f * (newValue / newMax);

    float frac;

    if (percentage >= 0.0f && percentage <= 25.0f)
    {
        //Blue fading to Cyan [0,x,255], where x is increasing from 0 to 255
        frac = percentage / 25.0f;
        return { 0.0f, (frac * 1.0f), 1.0f , 1.0f };
    }
    else if (percentage <= 50.0f)
    {
        //Cyan fading to Green [0,255,x], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 25.0f) / 25.0f;
        return { 0.0f, 1.0f, (frac * 1.0f), 1.0f };
    }
    else if (percentage <= 75.0f)
    {
        //Green fading to Yellow [x,255,0], where x is increasing from 0 to 255
        frac = std::abs(percentage - 50.0f) / 25.0f;
        return { (frac * 1.0f), 1.0f, 0.0f, 1.0f };
    }
    else if (percentage <= 100.0f)
    {
        //Yellow fading to red [255,x,0], where x is decreasing from 255 to 0
        frac = 1.0f - std::abs(percentage - 75.0f) / 25.0f;
        return { 1.0f, (frac * 1.0f), 0.0f, 1.0f };
    }
    else if (percentage > 100.0f)
    {   
        //Returning red if the value overshoot the limit.
        return { 1.0f, 0.0f, 0.0f, 1.0f };
    }

    return { 0.5, 0.5, 0.5, 1.0 };
}


static Vec4 GetBlendedColorBlueRed(float min, float max, float value)
{
    float negRange, posRange, percentage, frac, sf;

    //Compression, thus blue, we assume that min is also a negative value
    if (value < 0) 
    {
        negRange = min;
        frac = value / negRange;   

        //Blue fading to white [x,x,255], where x is increasing from 0 to 255
        sf = frac;
        return { (sf * 1.0f), (sf * 1.0f), 1.0f , 1.0f };
    }
    else //We got tension, this red
    {
        posRange = max;
        frac = value / posRange;

        //White fading to Red [255,x,x], where x is decreasing from 255 to 0
        sf = 1.0f - frac;
        return { 1.0f, (sf * 1.0f), (sf * 1.0f), 1.0f };
    }
    
    return { 0.5, 0.5, 0.5, 1.0 };
}



static Vec4 GetBlendedColorGreenZero(float min, float max, float value)
{
    float negRange, posRange, percentage, frac, sf;

    //Negative strain colored from green to blue.
    if (value < 0)
    {
        negRange = std::abs(min);
        frac = std::abs(value / negRange);
        percentage = 100 * frac;

        if (percentage >= 0.0f && percentage <= 50.0f)
        {
            //Green fading to Cyan [0,x,255], where x is increasing from 0 to 255
            sf = percentage / 50.0f;
            return { 0.0f, 1.0f, (sf * 1.0f) , 1.0f };
        }
        else if (percentage <= 100.0f)
        {
            //Cyan fading to blue [0,x,255], where x is decreasing from 255 to 0
            sf = 1.0f - std::abs(percentage - 50.0f) / 50.0f;
            return { 0.0f, (sf * 1.0f), 1.0f, 1.0f };
        }
        else if (percentage > 100.0f)
        {
            //Blue
            return { 0.0f, 0.0f, 1.0f, 1.0f };
        }
    }
    else if (value == 0)
    {
        return { 0.0f, 1.0f, 0.0f, 1.0f };
    }
    else //Positive strain, colored from greed to red.
    {
        posRange = std::abs(max);
        frac = std::abs(value / posRange);
        percentage = 100 * frac;

        if (percentage >= 0.0f && percentage <= 50.0f)
        {
            sf = percentage / 50.0f;
            
            //Green fading to Yellow [x,255,0], where x is increasing from 0 to 255
            frac = std::abs(percentage - 50.0f) / 50.0f;
            return { (frac * 1.0f), 1.0f, 0.0f, 1.0f };
        }
        else if (percentage <= 100.0f)
        {
            //Yellow fading to red [255,x,0], where x is decreasing from 255 to 0
            frac = 1.0f - std::abs(percentage - 50.0f) / 50.0f;
            return { 1.0f, (frac * 1.0f), 0.0f, 1.0f };
        }
        else if (percentage > 100)
        {   
            //Red
            return { 1.0f, 0.0f, 0.0f, 1.0f };
        }
    }
    return { 0.5, 0.5, 0.5, 1.0 };
}




static float Distance(Vertex &v1, Vertex &v2) 
{
    float x1, y1, z1;
    float x2, y2, z2;

    x1 = v1.Position.x;
    y1 = v1.Position.y;
    z1 = v1.Position.z;

    x2 = v2.Position.x;
    y2 = v2.Position.y;
    z2 = v2.Position.z;

    return std::sqrt(std::pow(x2 - x1,2) + std::pow(y2 - y1, 2) + std::pow(z2 - z1, 2));
}

static float Distance(Particle& p1, Particle& p2)
{
    float x1, y1, z1;
    float x2, y2, z2;

    x1 = p1.cPosition.x;
    y1 = p1.cPosition.y;
    z1 = p1.cPosition.z;

    x2 = p2.cPosition.x;
    y2 = p2.cPosition.y;
    z2 = p2.cPosition.z;

    return std::sqrt(std::pow(x2 - x1, 2) + std::pow(y2 - y1, 2) + std::pow(z2 - z1, 2));
}

static float Distance(Vec3& v1, Vec3& v2)
{
    float x1, y1, z1;
    float x2, y2, z2;

    x1 = v1.x;
    y1 = v1.y;
    z1 = v1.z;

    x2 = v2.x;
    y2 = v2.y;
    z2 = v2.z;

    return std::sqrt(std::pow(x2 - x1, 2) + std::pow(y2 - y1, 2) + std::pow(z2 - z1, 2));
}

static Vec3 GetVector(Vec3& from, Vec3& to)
{
    float x, y, z;

    x = to.x - from.x;
    y = to.y - from.y;
    z = to.z - from.z;

    return {x, y, z};
}


static Vec3 AddVectors(Vec3& v1, Vec3& v2)
{
    float x, y, z;

    x = v1.x + v2.x;
    y = v1.y + v2.y;
    z = v1.z + v2.z;

    return { x, y, z };
}



static int Factorial(int f)
{
    if (f == 0)
        return 1;
    else
        return f * Factorial(f - 1);
}

static float CalcForceFlux110(float strain_ab, float strain_ab_pl, Material bondMaterial, float meanStrain)
{
    float S = 0.0f;
    float N = 3.0f;                    
    float G = bondMaterial.G;
    float v = bondMaterial.v;
    float K = bondMaterial.K;

    S = G * (N + 2) * (strain_ab - strain_ab_pl) + (N * K - G * (N + 2)) * meanStrain;

    //float H = N * G * (4.0f * v - 1.0f) / (1.0f - 2.0f * v);
    //S = N * (G * (N + 2.0f) * (strain_ab - strain_ab_pl) + H * meanStrain);

    return S;
}

static float CalcForceFlux77(float strain_ab, Material bondMaterial, float meanStrain)
{
    float S = 0.0f;
    float N = 3.0f;
    float G = bondMaterial.G;
    float v = bondMaterial.v;
    float K = bondMaterial.K;

    S = G * (N + 2) * strain_ab + (N * K - G * (N + 2)) * meanStrain;

    return S;
}

static float EvaluateDerivative(float r, float h, KernelSettings ks)
{
    float u = r / h;
    float Wprime = 0;
    float fprime = 0;
    float pi = 3.14159265358979323846f;

    if (u <= ks.alpha)
    {
        //Eq.(113) simplified and the derivative taken 
        fprime = (105.0f * u * (u * u - ks.alphaSquared)) / (8.0f * pi * ks.alphaPowSeven);

        //Eq.(57) 
        Wprime = (1.0f / (h * h * h * h)) * fprime;
    }
    else
        Wprime = 0;

    return Wprime;
}

static double Evaluate(double r, double h, KernelSettings ks)
{
    double u = r / h;
    double W = 0;
    double f = 0;
    float pi = 3.14159265358979323846f;

    if (u <= ks.alpha)
    {
        //Eq.(113) simplified
        f = (105.0f / (32.0f * pi * ks.alphaPowThree)) * pow((1 - (u*u / ks.alphaSquared)), 2);

        //Eq.(53)
        W = (1.0 / (h * h * h)) * f;
    }
    else
        W = 0;

    return W;
}

static Vec3 GetUnitVector(Particle a, Particle b, float length) 
{
    return 
    {
        (b.cPosition.x - a.cPosition.x) / length,
        (b.cPosition.y - a.cPosition.y) / length,
        (b.cPosition.z - a.cPosition.z) / length
    };
}

static Vec3 UnitizeVector(Vec3 v)
{
    float length = std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);

    return
    {
        v.x / length,
        v.y / length,
        v.z / length
    };
}


static float GetSmallest(float v1, float v2, float v3) 
{
    if (v1 < v2 && v1 < v3)
        return v1;
    else if (v2 < v1 && v2 < v3)
        return v2;

    return v3;
}

static float GetVectorLength(Vec3 v) 
{
    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

static float Rad2Deg(float rad) 
{
    return rad * 180 / 3.14159265359f;
}

static float Deg2Rad(float deg)
{
    return deg * 3.14159265359 / 180.0f;
}

static float GetAddedVectorLength(Vec3 v1, Vec3 v2)
{
    Vec3 v = { v1.x + v2.x, v1.y + v2.y, v1.z + v2.z };

    return std::sqrt(v.x * v.x + v.y * v.y + v.z * v.z);
}

static bool InsideSliceBC(float rLimit, float aLimit, Vec3 vBase, Vec3 vParticle) 
{  
    //Project to the yz plane
    vBase.x = 0;
    vParticle.x = 0;

    float r = GetVectorLength(vParticle);

    Vec3 vBaseU = UnitizeVector(vBase);
    Vec3 vParticleU = UnitizeVector(vParticle);

    float scalarProd = vBaseU.y * vParticleU.y + vBaseU.z * vParticleU.z; //vBaseU.x * vParticleU.x;

    float aRad = std::acos(scalarProd);
    float aDeg = Rad2Deg(aRad);


    if ((aDeg < aLimit || aDeg >(180 - aLimit)) && r > rLimit) 
    {
        return true;
    }
    return false;
}


static bool InsideHeightBC(float hLimit, Vec3 vParticle) 
{
    if (std::abs(vParticle.z) > hLimit)
        return true;

    return false;
}



static bool InsideLengthBC(float lLimit, Vec3 vParticle)
{
    if (std::abs(vParticle.x) < lLimit)
        return true;

    return false;
}


static bool IsParticleInsideCylinder(Vec3 pPos, LoadingCylinder LC) 
{
    Vec3 projPos = { pPos.x, 0.0f, pPos.z };

    Vec3 LCprojPos = { LC.cPosition.x, 0.0f, LC.cPosition.z };

    float d = Distance(projPos, LCprojPos);

    if (d <= LC.Radius)
        return true;

    return false;
}


static Vec3 GetAverageVector(Vec3 v1, Vec3 v2) 
{
    return { (v1.x + v2.x) / 2.0f, (v1.y + v2.y) / 2.0f, (v1.z + v2.z) / 2.0f };
}


static Vec3 CalcParticleProjectionOnCylinder(Vec3 pPos, LoadingCylinder LC)
{
    Vec3 vec = GetVector(LC.cPosition, pPos);
    vec.y = 0.0f;
    Vec3 vecScaled = GetVector(LC.cPosition, pPos);
    vecScaled.y = 0.0f;
    
    Vec3 vecDiff = { 0, 0, 0};

    float vL = GetVectorLength(vec);

    //Unitize vector
    vecScaled.x = vecScaled.x / vL;
    //vecScaled.y = vecScaled.y / vL;
    vecScaled.z = vecScaled.z / vL;

    //Scale vector
    vecScaled.x = vecScaled.x * LC.Radius;
    //vecScaled.y = vecScaled.y * LC.Radius;
    vecScaled.z = vecScaled.z * LC.Radius;

    vecDiff.x = vecScaled.x - vec.x;
    //vecDiff.y = vecScaled.y - vec.y;
    vecDiff.z = vecScaled.z - vec.z;

    return vecDiff;
}




