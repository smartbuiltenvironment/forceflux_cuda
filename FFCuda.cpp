﻿
#include "FFCuda.h"


FFCuda::FFCuda()
{

}


FFCuda::~FFCuda()
{
	//delete[] m_dVertices;
	//delete[] m_dCounter;
}


void FFCuda::Initialise(int nParticles, int nZones, int zzCounter, int ppCounter)
{
	m_nParticles = nParticles;
	m_nZones = nZones;

	m_dVertices = new Vertex[nParticles];
	m_dVerticesCopy = new Vertex[nParticles];

	m_dparticleHa = new float[nParticles];
	m_dparticleHaCopy = new float[nParticles];

	m_dpzTop = new int[nParticles];
	m_dpFriendsCount = new int[nParticles];
	m_dpFriendsCountCopy = new int[nParticles];

	m_dzpTop = new int[nParticles];
	m_dzpStartIndex = new int[nZones];
	m_dzpRowLength = new int[nZones];

	m_dzzTop = new int[zzCounter];
	m_dzzStartIndex = new int[nZones];
	m_dzzRowLength = new int[nZones];

	m_NUM_THREADS = 1024;										//Should be dividable by 32
	m_NUM_BLOCKS = (nParticles + m_NUM_THREADS - 1) / m_NUM_THREADS;
		
	size_t bytesVP = sizeof(Vertex) * m_nParticles;
	size_t bytesIP = sizeof(int) * m_nParticles;
	size_t bytesIZ = sizeof(int) * m_nZones;
	size_t bytesIZZ = sizeof(int) * zzCounter;
	size_t bytesIZP = sizeof(int) * m_nParticles;
	size_t bytesFP = sizeof(float) * m_nParticles;
	size_t bytesIPP = sizeof(int) * ppCounter;

	cudaMalloc(&m_dVertices, bytesVP);
	cudaMalloc(&m_dVerticesCopy, bytesVP);
	cudaMalloc(&m_dpzTop, bytesIP);
	cudaMalloc(&m_dpFriendsCount, bytesIP);
	cudaMalloc(&m_dpFriendsCountCopy, bytesIP);

	//Particle size data
	cudaMalloc(&m_dparticleHa, bytesFP);
	cudaMalloc(&m_dparticleHaCopy, bytesFP);

	//Allocate memory for zone-particle topology
	cudaMalloc(&m_dzpTop, bytesIZP);
	cudaMalloc(&m_dzpStartIndex, bytesIZ);
	cudaMalloc(&m_dzpRowLength, bytesIZ);

	//Allocate memory for zone-zone topology
	cudaMalloc(&m_dzzTop, bytesIZZ);
	cudaMalloc(&m_dzzStartIndex, bytesIZ);
	cudaMalloc(&m_dzzRowLength, bytesIZ);

	//Allocate memory for particle-particle topology
	cudaMalloc(&m_dppTop, bytesIPP);
	cudaMalloc(&m_dppStartIndex, bytesIP);
	cudaMalloc(&m_dppRowLength, bytesIP);

	m_TestVec = { 0 , 0, 0.0001};
}


void FFCuda::SwapArrays_Vertices()
{
	std::swap(m_dVertices, m_dVerticesCopy);
}

void FFCuda::SwapArrays_Ha()
{
	std::swap(m_dparticleHa, m_dparticleHaCopy);
}

void FFCuda::SwapArrays_Friends()
{
	std::swap(m_dpFriendsCount, m_dpFriendsCountCopy);
}


//Copy initialisation
void FFCuda::CopyFromHostToDevice1(int* hpzTop, float* hParticleHa, int* hpFriendsCount)
{
	size_t bytesIP = sizeof(int) * m_nParticles;
	size_t bytesFP = sizeof(float) * m_nParticles;

	cudaMemcpy(m_dpzTop, hpzTop, bytesIP, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dparticleHa, hParticleHa, bytesFP, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dpFriendsCount, hpFriendsCount, bytesIP, cudaMemcpyHostToDevice);
}

void FFCuda::CopyFromHostToDevice2(int* hzzTop, int zzCounter, int* hzzMinIndex, int* hzzMaxIndex)
{
	size_t bytesIZZ = sizeof(int) * zzCounter;
	size_t bytesIZ = sizeof(int) * m_nZones;

	cudaMemcpy(m_dzzTop, hzzTop, bytesIZZ, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dzzRowLength, hzzMaxIndex, bytesIZ, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dzzStartIndex, hzzMinIndex, bytesIZ, cudaMemcpyHostToDevice);
}

void FFCuda::CopyFromHostToDevice3(int* hzpTop, int zpCounter, int* hzpMinIndex, int* hzpMaxIndex)
{
	size_t bytesIZP = sizeof(int) * zpCounter;
	size_t bytesIZ = sizeof(int) * m_nZones;

	cudaMemcpy(m_dzpTop, hzpTop, bytesIZP, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dzpRowLength, hzpMaxIndex, bytesIZ, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dzpStartIndex, hzpMinIndex, bytesIZ, cudaMemcpyHostToDevice);
}

void FFCuda::CopyFromHostToDevice4(int* hppTop, int ppCounter, int* hppStartIndex, int* hppRowLength)
{
	size_t bytesIPP = sizeof(int) * ppCounter;
	size_t bytesIP = sizeof(int) * m_nParticles;

	cudaMemcpy(m_dppTop, hppTop, bytesIPP, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dppRowLength, hppRowLength, bytesIP, cudaMemcpyHostToDevice);
	cudaMemcpy(m_dppStartIndex, hppStartIndex, bytesIP, cudaMemcpyHostToDevice);
}


//Copy singular variables
void FFCuda::CopyFromHostToDevice_Particles(Vertex* hVertices) 
{
	size_t bytesV = sizeof(Vertex) * m_nParticles;
	cudaMemcpy(m_dVertices, hVertices, bytesV, cudaMemcpyHostToDevice);
}

void FFCuda::CopyFromDeviceToHost_Particles(Vertex* hVertices)
{
	size_t bytesV = sizeof(Vertex) * m_nParticles;
	cudaMemcpy(hVertices, m_dVertices, bytesV, cudaMemcpyDeviceToHost);
}

void FFCuda::CopyFromHostToDevice_Ha(float* particleHa)
{
	size_t bytesF = sizeof(float) * m_nParticles;
	cudaMemcpy(m_dparticleHa, particleHa, bytesF, cudaMemcpyHostToDevice);
}

void FFCuda::CopyFromDeviceToHost_Ha(float* particleHa)
{
	size_t bytesF = sizeof(float) * m_nParticles;
	cudaMemcpy(particleHa, m_dparticleHa, bytesF, cudaMemcpyDeviceToHost);
}

void FFCuda::CopyFromHostToDevice_HaCopy(float* particleHaCopy)
{
	size_t bytesF = sizeof(float) * m_nParticles;
	cudaMemcpy(m_dparticleHaCopy, particleHaCopy, bytesF, cudaMemcpyHostToDevice);
}

void FFCuda::CopyFromDeviceToHost_HaCopy(float* particleHaCopy)
{
	size_t bytesF = sizeof(float) * m_nParticles;
	cudaMemcpy(particleHaCopy, m_dparticleHaCopy, bytesF, cudaMemcpyDeviceToHost);
}

void FFCuda::CopyFromHostToDevice_FriendsCount(int* pFriendsCount)
{
	size_t bytesI = sizeof(int) * m_nParticles;
	cudaMemcpy(m_dpFriendsCount, pFriendsCount , bytesI, cudaMemcpyHostToDevice);
}

void FFCuda::CopyFromDeviceToHost_FriendsCount(int* pFriendsCount)
{
	size_t bytesI = sizeof(int) * m_nParticles;
	cudaMemcpy(pFriendsCount, m_dpFriendsCount, bytesI, cudaMemcpyDeviceToHost);
}



void FFCuda::ExecuteLaplacian() 
{
	Laplacian<<<m_NUM_BLOCKS, m_NUM_THREADS>>>(m_dVertices, m_dVerticesCopy, m_dppTop, m_dppStartIndex, m_dppRowLength, m_nParticles);

	cudaDeviceSynchronize();
}


void FFCuda::IncrementSize() 
{	
	
	IncrementParticleSize <<< m_NUM_BLOCKS, m_NUM_THREADS >>> (
		m_dVertices,
		m_dparticleHa, 
		m_dparticleHaCopy, 
		m_nParticles, 
		m_dpzTop, 
		m_dzzTop, 
		m_dzzStartIndex, 
		m_dzzRowLength, 
		m_dzpTop, 
		m_dzpStartIndex, 
		m_dzpRowLength,
		m_dpFriendsCount);

	cudaDeviceSynchronize();
}


void FFCuda::IncrementSize2()
{	
	int xThreads = 1024;
	int yThreads = 1024;
	int zThreads = 64;

	int numBlocksX = (m_nParticles + xThreads - 1) / xThreads;		
	int numBlocksY = (300 + yThreads - 1) / yThreads;				//Maximum numbers of neighbours to a particle
	int numBlocksZ = (27 + zThreads - 1) / zThreads;				//27 is the maximum numbers of neighbouring zones.

	dim3 blocks(xThreads, yThreads, zThreads);

	dim3 grid(numBlocksX, numBlocksY, numBlocksZ);

	IncrementParticleSize2 <<< grid, blocks >>> (
		m_dVertices,
		m_dparticleHa,
		m_dparticleHaCopy,
		m_nParticles,
		m_dpzTop,
		m_dzzTop,
		m_dzzStartIndex,
		m_dzzRowLength,
		m_dzpTop,
		m_dzpStartIndex,
		m_dzpRowLength,
		m_dpFriendsCount,
		m_dpFriendsCountCopy);

	cudaDeviceSynchronize();
}


