﻿#include <cuda_runtime.h>
#include "device_launch_parameters.h"
#include "Common.h"


__global__ void CudaTest(Vertex* dVertices, int n, Vec3 testVec);

__global__ void CudaTest2(Vertex* dVertices, Vertex* dVerticesCopy, int n, Vec3 testVec);

__global__ void Laplacian(Vertex* dVertices, Vertex* dVerticesCopy, int* dppTop, int* dStartIndex, int* dRowLength, int nParticles);

__global__ void IncrementParticleSize(Vertex* particlePos, float* particleHa, float* particleHaCopy, int nParticles, int* pzTop, int* zzTop, int* zzStartIndex, int* zzRowLength, int* zpTop, int* zpStartIndex, int* zpRowLength, int* pFriendsCount);

__global__ void IncrementParticleSize2(Vertex* particlePos, float* particleHa, float* particleHaCopy, int nParticles, int* pzTop, int* zzTop, int* zzStartIndex, int* zzRowLength, int* zpTop, int* zpStartIndex, int* zpRowLength, int* pFriendsCount, int* pFriendsCountCopy);




