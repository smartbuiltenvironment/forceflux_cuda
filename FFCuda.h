﻿#pragma once

#include <cuda_runtime.h>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "Common.h"
//#include <helper_cuda.h>

#include "Cuda.cuh"




struct DeviceData
{
    float* dPos;  // mapped host pointers
    float* dVel;
    
};


class FFCuda 
{
public:
    FFCuda();
    ~FFCuda();
    
    void Initialise(int nParticles, int nZones, int zzCounter,int ppCounter);

    void ExecuteLaplacian();

    void IncrementSize();

    void IncrementSize2();

    void SwapArrays_Vertices();

    void SwapArrays_Ha();

    void SwapArrays_Friends();

    void CopyFromHostToDevice1(int* hpzTop, float* hparticleHa, int* hpFriendsCount);

    void CopyFromHostToDevice2(int* hzzTop, int zzCounter, int* hzzMinIndex, int* hzzMaxIndex);

    void CopyFromHostToDevice3(int* hzpTop, int zpCounter, int* hzpMinIndex, int* hzpMaxIndex);

    void CopyFromHostToDevice4(int* hppTop, int ppCounter, int* hppMinIndex, int* hppMaxIndex);

    void CopyFromHostToDevice_Particles(Vertex* particles);

    void CopyFromDeviceToHost_Particles(Vertex* particles);

    void CopyFromHostToDevice_Ha(float* particleHa);

    void CopyFromDeviceToHost_Ha(float* particleHa);

    void CopyFromHostToDevice_HaCopy(float* particleHaCopy);

    void CopyFromDeviceToHost_HaCopy(float* particleHaCopy);

    void CopyFromHostToDevice_FriendsCount(int* pFriendsCount);

    void CopyFromDeviceToHost_FriendsCount(int* pFriendsCount);


private:
    Vertex* m_dVertices;
    Vertex* m_dVerticesCopy;

    int m_NUM_THREADS;
    int m_NUM_BLOCKS;

    Vec3 m_TestVec;

    int m_nParticles;
    int m_nZones;
        
    float* m_dparticleHa;
    float* m_dparticleHaCopy;

    int* m_dpFriendsCount; //Particle friends count
    int* m_dpFriendsCountCopy;

    //Particle zone structure   
    int* m_dpzTop;

    //Zone-particle topology
    int* m_dzpTop;
    int* m_dzpStartIndex;
    int* m_dzpRowLength;

    //Zone-zone topology
    int* m_dzzTop;
    int* m_dzzStartIndex;
    int* m_dzzRowLength;

    //Particle-particle topology
    int* m_dppTop;
    int* m_dppStartIndex;
    int* m_dppRowLength;

};











