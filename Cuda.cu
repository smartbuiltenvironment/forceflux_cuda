﻿#include "Cuda.cuh"


__global__ void CudaTest(Vertex* dVertices, int n, Vec3 testVec)
{
	int index = (blockIdx.x * blockDim.x) + threadIdx.x;

	//Boundary check
	if (index < n)
	{
		dVertices[index].Position.x += testVec.x;
		dVertices[index].Position.y += testVec.y;
		dVertices[index].Position.y += testVec.z;
	}
}


__global__ void CudaTest2(Vertex* dVertices, Vertex* dVerticesCopy, int n, Vec3 testVec)
{
	int index = (blockIdx.x * blockDim.x) + threadIdx.x;

	//Boundary check
	if (index < n)
	{
		dVerticesCopy[index].Position.x = dVertices[index].Position.x + testVec.x;
		dVerticesCopy[index].Position.y = dVertices[index].Position.y + testVec.y;
		dVerticesCopy[index].Position.z = dVertices[index].Position.z + testVec.z;
	}
}


__global__ void Laplacian(Vertex* dVertices, Vertex* dVerticesCopy, int* dppTop, int* dStartIndex, int* dRowLength, int nParticles)
{
	int index = (blockIdx.x * blockDim.x) + threadIdx.x;

	//Boundary check
	if (index < nParticles)
	{
		int startIndex = dStartIndex[index];
		int rowLength = dRowLength[index];
		float n = rowLength;

		//Get position from current vertex
		float xavrg = dVertices[index].Position.x;
		float yavrg = dVertices[index].Position.y;
		float zavrg = dVertices[index].Position.z;

		float sf = 0.001;

		for (int i = 0; i < rowLength; i++)
		{	
			int flatIndex = startIndex + i;
			int pGlobalIndex = dppTop[flatIndex];
			xavrg += sf * (dVertices[pGlobalIndex].Position.x / n);
			yavrg += sf * (dVertices[pGlobalIndex].Position.y / n);
			zavrg += sf * (dVertices[pGlobalIndex].Position.z / n);
		}

		//Update position for the copy of the current vertex
		dVerticesCopy[index].Position.x = xavrg;
		dVerticesCopy[index].Position.y = yavrg;
		dVerticesCopy[index].Position.z = zavrg;
	}
}


__global__ void IncrementParticleSize
(	Vertex* particlePos,
	float* particleHa, 
	float* particleHaCopy,
	int nParticles, 
	int* pzTop, 
	int* zzTop, 
	int* zzStartIndex, 
	int* zzRowLength, 
	int* zpTop, 
	int* zpStartIndex, 
	int* zpRowLength,
	int* pFriendsCount)
{
	int index = (blockIdx.x * blockDim.x) + threadIdx.x;
	Vertex v, vNext;
	float d = 0;
	int vNextIndex;
	int requiredFriendsCount = 5;
	int friendsCount = 0;
	
	if(index < nParticles)
	{
		int currentZone = pzTop[index];
		int nNeighZones = zzRowLength[currentZone];
		Vertex p = particlePos[index];
		
		float haSquared = particleHa[index] * particleHa[index];

		pFriendsCount[index] = 0;
		friendsCount = 0;

		for (int neighZoneRowIndex = 0; neighZoneRowIndex < nNeighZones; neighZoneRowIndex++)
		{
			int zzFlatIndex = zzStartIndex[currentZone] + neighZoneRowIndex;
			int zIndexGlobal = zzTop[zzFlatIndex];

			int nParticlesInNeighZone = zpRowLength[zIndexGlobal];
			
			//Loop over the particles in the neighbouring zone
			for (int pRowIndexInNeighZone = 0; pRowIndexInNeighZone < nParticlesInNeighZone; pRowIndexInNeighZone++)
			{

				int zpFlatIndexNZ = zpStartIndex[zIndexGlobal] + pRowIndexInNeighZone;
				int pIndexGlobalNZ = zpTop[zpFlatIndexNZ];


				//Avoid checking the particle against itself
				if (index != pIndexGlobalNZ)
				{							
					Vertex pNext = particlePos[pIndexGlobalNZ];

					float xDiff = pNext.Position.x - p.Position.x;
					float yDiff = pNext.Position.y - p.Position.y;
					float zDiff = pNext.Position.z - p.Position.z;

					//Square the distances to avoid square root computation
					float dSquared = (xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);

					if (dSquared < haSquared)
					{	
						//pFriendsCount[index]++;
						friendsCount++;


						pFriendsCount[index]++;
					}
				}
			}
		}
		//Loop around all neighbouring zones/particles finished, so size can be updated.
		particleHaCopy[index] += 0.0001 * (requiredFriendsCount - pFriendsCount[index]);//pFriendsCount[index]);

	}


}



__global__ void IncrementParticleSize2
(Vertex* particlePos,
	float* particleHa,
	float* particleHaCopy,
	int nParticles,
	int* pzTop,
	int* zzTop,
	int* zzStartIndex,
	int* zzRowLength,
	int* zpTop,
	int* zpStartIndex,
	int* zpRowLength,
	int* pFriendsCount,
	int* pFriendsCountCopy)
{
	int indexX = (blockIdx.x * blockDim.x) + threadIdx.x;		//Looping over the particles
	int indexY = (blockIdx.y * blockDim.y) + threadIdx.y;		//Looping over the neighbouring particles
	int indexZ = (blockIdx.z * blockDim.z) + threadIdx.z;		//Looping over the neighbouring zones

	Vertex v, vNext;
	float d = 0;
	int vNextIndex;
	int requiredFriendsCount = 5;
	int friendsCount = 0;

	if (indexX < nParticles)
	{
		int currentZone = pzTop[indexX];
		int nNeighZones = zzRowLength[currentZone];
		Vertex p = particlePos[indexX];

		float haSquared = particleHa[indexX] * particleHa[indexX];

		pFriendsCountCopy[indexX] = 0;

		friendsCount = 0;

		//Loop over the neighbouring zones
		int neighZoneRowIndex = indexZ;

		if(neighZoneRowIndex < nNeighZones)
		{
			int zzFlatIndex = zzStartIndex[currentZone] + neighZoneRowIndex;
			int zIndexGlobal = zzTop[zzFlatIndex];

			int nParticlesInNeighZone = zpRowLength[zIndexGlobal];

			//Loop over the particles in the neighbouring zone
			int pRowIndexInNeighZone = indexY;

			if(pRowIndexInNeighZone < nParticlesInNeighZone)
			{
				int zpFlatIndexNZ = zpStartIndex[zIndexGlobal] + pRowIndexInNeighZone;
				int pIndexGlobalNZ = zpTop[zpFlatIndexNZ];

				//Avoid checking the particle against itself
				if (indexX != pIndexGlobalNZ)
				{
					Vertex pNext = particlePos[pIndexGlobalNZ];

					float xDiff = pNext.Position.x - p.Position.x;
					float yDiff = pNext.Position.y - p.Position.y;
					float zDiff = pNext.Position.z - p.Position.z;

					pFriendsCountCopy[indexX]++;

					//Square the distances to avoid square root computation
					float dSquared = (xDiff * xDiff + yDiff * yDiff + zDiff * zDiff);

					if (dSquared < haSquared)
					{
						//pFriendsCount[indexX]++;
						friendsCount++;
					}
				}
			}
		}
		//Loop around all neighbouring zones/particles finished, so size can be updated.
		particleHaCopy[indexX] += 0.0001 * (requiredFriendsCount - friendsCount);//pFriendsCount[index]);

	}


}

