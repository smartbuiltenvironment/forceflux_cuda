#include "Camera.h"


Camera::Camera()    
{	
	Initialise(5.0f, {0,0,0});
}

Camera::Camera(float dist, Vec3 target)
{
	Initialise(dist, target);
}

Camera::~Camera()
{


}

void Camera::Initialise(float dist, Vec3 target) 
{
	m_Distance = dist;
	
	m_Target = vec3(target.x, target.y, target.z);

	m_MoveX = vec3(1.0f, 0.0f, 0.0f);
	m_MoveY = vec3(0.0f, 1.0f, 0.0f);
	m_MoveZ = vec3(0.0f, 0.0f, 1.0f);
	m_Speed = 0.05f;

	m_ScreenWidth = 1600;
	m_ScreenHeight = 900;

	m_FirstMouseRotate = true;
	m_MouseLastX = m_ScreenWidth / 2;
	m_MouseLastY = m_ScreenHeight / 2;

	m_MouseLastX_Pan = m_ScreenWidth / 2;
	m_MouseLastY_Pan = m_ScreenHeight / 2;

	m_Pitch = 35.0f;
	m_Yaw = -115.0f;
	m_Fov = 100; //45.0f;

	m_Up = vec3(0.0f, 0.0f, 1.0f);

	//Run once to initialise the camera vectors.
	ProcessMouseInputRotate(m_MouseLastX, m_MouseLastY);
}


void Camera::ProcessKeyInput(keyAcion ka)
{
	glm::vec3 moveVec;
	if (ka == keyAcion::leftArrow)
		moveVec = -1.0f * m_Right;
	else if (ka == keyAcion::rightArrow)
		moveVec = m_Right;
	else if (ka == keyAcion::upArrow)
		moveVec = m_Direction;
	else if (ka == keyAcion::downArrow)
		moveVec = -1.0f * m_Direction;

	m_Target += m_Speed * moveVec;
	m_Position += m_Speed * moveVec;
}


void Camera::UpdateCameraCoord() 
{
	m_Direction = normalize(m_Target - m_Position);			//New camera direction vecor
	m_Right = cross(m_Direction, vec3(0.0f, 0.0f, 1.0f));	//
	m_Up = cross(m_Right, m_Direction);						//
}

void Camera::ProcessMouseInputRotate(double xPos, double yPos)
{
	if (m_FirstMouseRotate)
	{
		m_MouseLastX = xPos;
		m_MouseLastY = yPos;
		m_FirstMouseRotate = false;
	}

	float xoffset = xPos - m_MouseLastX;
	float yoffset = m_MouseLastY - yPos;	//Resversed since y-coordinates go from bottom to top.
	m_MouseLastX = xPos;
	m_MouseLastY = yPos;

	float sensitivity = -0.25f;
	xoffset *= sensitivity;
	yoffset *= sensitivity;
	m_Yaw += xoffset;
	m_Pitch += yoffset;

	if (m_Pitch > 89.0f)
		m_Pitch = 89.0f;
	if (m_Pitch < -89.0f)
		m_Pitch = -89.0f;

	glm::vec3 newDirection;
	newDirection.x = cos(glm::radians(m_Yaw)) * cos(glm::radians(m_Pitch));
	newDirection.z = sin(glm::radians(m_Pitch));
	newDirection.y = sin(glm::radians(m_Yaw)) * cos(glm::radians(m_Pitch));
	
	m_Position = m_Distance * glm::normalize(newDirection);

	UpdateCameraCoord();
}


void Camera::ProcessMouseInputPan(double xPos, double yPos)
{
	if (m_FirstMousePan)
	{
		m_MouseLastX_Pan = xPos;
		m_MouseLastY_Pan = yPos;
		m_FirstMousePan = false;
	}

	//float sensitivity = -0.001f;
	float xoffset = xPos - m_MouseLastX_Pan;
	float yoffset = m_MouseLastY_Pan - yPos;	//Resversed since y-coordinates go from bottom to top.

	m_Target.x = xoffset; //* sensitivity;
	m_Target.y = yoffset; // *sensitivity;

	//m_Position.x = xoffset; // *sensitivity;
	//m_Position.y = yoffset; // *sensitivity;

	UpdateCameraCoord();
}



void Camera::ProcessScrollInput(float fov)
{
	m_Fov = fov;
	
	/* Limits if needed.
	if (m_Fov < 1.0f)
		m_Fov = 1.0f;
	if (m_Fov > 45.0f)
		m_Fov = 45.0f;*/
}


Vec2 Camera::ScreenToWorldTransformation(float xPosScreen, float yPosScreen) 
{
	float xMinWorld = -2;
	float xMaxWorld = 2;

	float yMinWorld = -1.125;
	float yMaxWorld = 1.125;

	Vec2 world;

	if (xPosScreen >= (m_ScreenWidth / 2))
	{
		float diff = xPosScreen - (m_ScreenWidth / 2);
		float diffPercent = diff / (m_ScreenWidth / 2);
		world.x = diffPercent * xMaxWorld;
	}
	else
	{
		float diff = xPosScreen - (m_ScreenWidth / 2);
		float diffPercent = diff / (m_ScreenWidth / 2);
		world.x = diffPercent * xMaxWorld;
	}


	if (yPosScreen >= (m_ScreenHeight / 2))
	{
		float diff = xPosScreen - (m_ScreenHeight / 2);
		float diffPercent = diff / (m_ScreenHeight / 2);
		world.y = diffPercent * yMaxWorld;
	}
	else
	{
		float diff = xPosScreen - (m_ScreenHeight / 2);
		float diffPercent = diff / (m_ScreenHeight / 2);
		world.y = diffPercent * yMaxWorld;
	}

	return world;
}


