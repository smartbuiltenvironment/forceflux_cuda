#pragma once

#include <GL/glew.h>
#include <iostream>

#define ASSERT(x)if(!(x)) __debugbreak();

//Macro which will call CLClearError and GLLogCall for error handeling
#define GLCall(x) GLClearError();\
	x;\
	ASSERT(GLLogCall(#x, __FILE__, __LINE__))


//Clear errors so we know there are no other errors from other functions
void GLClearError();


//Check error that may have occured in this function call
bool GLLogCall(const char* function, const char* file, int line);




